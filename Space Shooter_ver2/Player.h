#pragma once
#include "Engine/GameObject.h"

class BoxCollider;
class Durability;
class Pointer;

//プレイヤーモデルを管理するクラス
class Player : public GameObject
{

    int hModel_;                    //モデル番号
    XMVECTOR keyMove_;              //キー入力の移動量
    XMVECTOR calcMove_;             //(キー入力)移動後の位置

    XMVECTOR mouseMove_;            //マウス入力の移動量
    XMVECTOR calcMouseMove_;        //(マウス入力)移動後の位置

    BoxCollider* pCollision_;       //インスタンス(衝突判定)
    Durability* pDurability_;       //インスタンス(耐久度)
    Pointer* pPointer_;             //インスタンス(ポインター)
    bool clashFlag_;                //衝突フラグ(障害物との)
    int flashCnt_;                  //点滅回数(プレイヤーの)
    int flashTime_;                 //点滅間隔※ミリ秒

    XMVECTOR move_;                 //移動量
    int camType_;                   //選択中のカメラ

public:
    //コンストラクタ
    Player(GameObject* parent);

    //デストラクタ
    ~Player();

    //初期化
    void Initialize() override;

    //更新
    void Update() override;

    //描画
    void Draw() override;

    //開放
    void Release() override;

    //何かに当たった
    //引数：当たった相手
    void OnCollision(GameObject* pTarget) override;

    //TPSカメラ時の移動処理
    void MoveTPS();

    //FPSカメラ時の移動処理
    void MoveFPS();

    //移動量を渡す処理
    XMVECTOR GetMove() { return move_; };

    //選択中のカメラを調べる関数
    void CheckCam(int camType);

    //移動
    void Move();

    //位置を渡す
    XMVECTOR GetPos() { return transform_.position_; };

    //傾きを直す関数
    void FixRotate();
};