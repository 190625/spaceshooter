#include "Obstacle.h"
#include "Durability.h"
#include "Engine/Model.h"

//コンストラクタ
Obstacle::Obstacle(GameObject* parent)
    :GameObject(parent, "Obstacle"), hModel_(-1), randam_(0), pCollision_(nullptr)
{
}

//デストラクタ
Obstacle::~Obstacle()
{
}

//初期化
void Obstacle::Initialize()
{
    //モデルデータのロード
    hModel_ = Model::Load("Assets/EnemyAttack.fbx");
    assert(hModel_ >= 0);

    srand((unsigned int)time(NULL));

    randam_ = (rand() % 9) + 1;

    switch (randam_)
    {
    case 1:
        transform_.position_ = pattern_One; break;
    case 2:
        transform_.position_ = pattern_Two; break;
    case 3:
        transform_.position_ = pattern_Three; break;
    case 4:
        transform_.position_ = pattern_Four; break;
    case 5:
        transform_.position_ = pattern_Five; break;
    case 6:
        transform_.position_ = pattern_Six; break;
    case 7:
        transform_.position_ = pattern_Seven; break;
    case 8:
        transform_.position_ = pattern_Eight; break;
    case 9:
        transform_.position_ = pattern_Nine; break;
    default:
        break;
    }

    transform_.position_.vecZ = 40.0f;

    pCollision_ = new SphereCollider(XMVectorSet(0, 0, 0, 0), 3.4f);
    AddCollider(pCollision_);

    //--------------(仮)--------------
    transform_.scale_.vecX *= 2.4f;
    transform_.scale_.vecY *= 2.4f;
    
}

//更新
void Obstacle::Update()
{
    transform_.position_.vecZ -= 0.5f;
    transform_.rotate_.vecZ += 0.5f;

    if (transform_.position_.vecZ < 0.0f)
        KillMe();
}

//描画
void Obstacle::Draw()
{
    Model::SetTransform(hModel_, transform_);
    Model::Draw(hModel_,SHADER_3D);
    //pCollision_->Draw(transform_.position_);
}

//開放
void Obstacle::Release()
{
}

//何かに当たった
void Obstacle::OnCollision(GameObject* pTarget)
{
    if (pTarget->GetObjectName() == "Player")
    {
        KillMe();
    }
}
