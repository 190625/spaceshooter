#pragma once
#include "Engine/GameObject.h"
#include "Engine/Camera.h"

class ResultSceneCamera : public GameObject
{
	XMVECTOR camPos_;       //カメラの位置
	XMVECTOR camTar_;       //カメラの焦点

public:
	ResultSceneCamera(GameObject* parent);
	~ResultSceneCamera();
	void Initialize() override;
	void Update() override;
	void Draw() override;
	void Release() override;
};