#include "ResultText.h"
#include "Engine/Model.h"
#include "Engine/SceneTimer.h"

ResultText::ResultText(GameObject* parent)
	:GameObject(parent, "ResultText"),changeFlag_(true),changeTime_(800)
{
	for (int i = 0; i < textEng_ + textJpn_; i++)
	{
		hModel_[i] = -1;
	}
}

ResultText::~ResultText()
{
}

void ResultText::Initialize()
{
	for (int i = 0; i < textEng_ + textJpn_; i++)
	{
		hModel_[i] = Model::Load("Assets/Result_" + std::to_string(i) + ".fbx");
		assert(hModel_[i] >= 0);

		trans_[i].rotate_.vecY = XMConvertToRadians(180.f);
	}

	trans_[0].position_ = XMVectorSet(0, 2.5f, 4.f, 0);
	trans_[1].position_ = XMVectorSet(-1.4f, 1.3f, 4.f, 0);
	trans_[2].position_ = XMVectorSet(-1.4f, 0.1f, 4.f, 0);
	trans_[3].position_ = XMVectorSet(-1.4f, -1.3f, 4.f, 0);

	trans_[4].scale_ *= 0.8f;
	trans_[4].position_ = XMVectorSet(-2.2f, -3.4f, 4.f, 0);
	trans_[5].scale_ *= 0.8f;
	trans_[5].position_ = XMVectorSet(2.2f, -3.4f, 4.f, 0);
}

void ResultText::Update()
{
	//------------------------------ �_�ŏ��� --------------------------------
	static DWORD startTime_ = timeGetTime();
	DWORD nowTime_ = timeGetTime();

	if (nowTime_ - startTime_ >= changeTime_)
	{
		if (changeFlag_)
			changeFlag_ = false;
		else
			changeFlag_ = true;

		startTime_ = nowTime_;
	}

	SceneTimer::UpdateFrameDelta();
}

void ResultText::Draw()
{
	for (int i = 0; i < textEng_; i++)
	{
		trans_[i].Calclation();
		Model::SetTransform(hModel_[i], trans_[i]);
		Model::Draw(hModel_[i], SHADER_3D);
	}

	for (int i = textEng_; i < textEng_ + textJpn_; i++)
	{
		if (changeFlag_)
		{
			trans_[i].Calclation();
			Model::SetTransform(hModel_[i], trans_[i]);
			Model::Draw(hModel_[i], SHADER_3D);
		}
	}
}

void ResultText::Release()
{
}
