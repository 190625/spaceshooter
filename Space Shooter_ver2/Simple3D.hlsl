//───────────────────────────────────────
 // テクスチャ＆サンプラーデータのグローバル変数定義
//───────────────────────────────────────
Texture2D		g_texture : register(t0);		//テクスチャー
SamplerState	g_sampler : register(s0);		//サンプラー

//───────────────────────────────────────
 // コンスタントバッファ
// DirectX 側から送信されてくる、ポリゴン頂点以外の諸情報の定義
//───────────────────────────────────────
cbuffer global
{
	float4x4	matWVP;			//カメラの情報(ワールド・ビュー・プロジェクションの合成行列)
	float4x4	matW;			//ワールド行列
	float4		diffuseColor;	//ディフューズカラー(マテリアルの色)
	bool		isTexture;		//テクスチャ貼ってあるかどうか
};

//───────────────────────────────────────
// 頂点シェーダー出力＆ピクセルシェーダー入力データ構造体
//───────────────────────────────────────
struct VS_OUT
{
	float4 pos		: SV_POSITION;	//位置
	float4 uv		: TEXCOORD;		//UV座標
	float4 color	: COLOR;		//色(明るさ)
};

//───────────────────────────────────────
// 頂点シェーダ
//───────────────────────────────────────
VS_OUT VS(float4 pos : POSITION, float4 uv : TEXCOORD, float4 normal : NORMAL)
{
	//ピクセルシェーダーへ渡す情報の構造体
	VS_OUT outData;

	//ローカル座標に、ワールド・ビュー・プロジェクション行列をかけて
	//スクリーン座標に変換し、ピクセルシェーダーへ
	outData.pos = mul(pos, matWVP);

	//UV座標
	outData.uv = uv;


	//法線を回転
	normal = mul(normal, matW);


	//ランバートの余弦則を使って明るさを求める
	//求めた明るさとテクスチャの色を掛け算したものが最終的に表示する色になる
	float4 light = float4(0, 0.2, -1, 1);
	light = normalize(light);
	//clamp：引数で与えられた数を、一定の範囲に収める
	outData.color = clamp(dot(normal, light), 0, 1);


	//まとめて出力
	return outData;
}

//───────────────────────────────────────
// ピクセルシェーダ
//───────────────────────────────────────
float4 PS(VS_OUT inData) : SV_Target
{
	//ディフューズ(拡散反射光)
	float4 diffuse;
	//アンビエント(環境光)
	float4 ambient;


	//テクスチャが貼ってあったら
	if (isTexture)
	{
		diffuse = g_texture.Sample(g_sampler, inData.uv) * inData.color;
		ambient = g_texture.Sample(g_sampler, inData.uv) * float4(0.2, 0.2, 0.2, 1);
	}
	
	//テクスチャが貼ってなかったら
	else
	{
		diffuse = diffuseColor * inData.color;
		ambient = diffuseColor * float4(0.2, 0.2, 0.2, 1);
	}

	return diffuse + ambient;
}