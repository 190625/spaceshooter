#pragma once
#include "Engine/GameObject.h"

const int textEng_ = 4;
const int textJpn_ = 2;

class ResultText : public GameObject
{
	int hModel_[textEng_ + textJpn_];
	Transform trans_[textEng_ + textJpn_];
	bool changeFlag_;
	int changeTime_;

public:
	ResultText(GameObject* parent);
	~ResultText();

	void Initialize() override;
	void Update() override;
	void Draw() override;
	void Release() override;
};

