#pragma once
#include "Engine/GameObject.h"

//カメラを管理するクラス
class PlaySceneCamera : public GameObject
{

    XMVECTOR camPos_;       //カメラの位置
    XMVECTOR camTar_;       //カメラの焦点
    XMVECTOR prevPos_;      //TPS時の位置
    XMVECTOR prevTar_;      //TPS時の位置

public:
    //コンストラクタ
    PlaySceneCamera(GameObject* parent);

    //デストラクタ
    ~PlaySceneCamera();

    //初期化
    void Initialize() override;

    //更新
    void Update() override;

    //描画
    void Draw() override;

    //開放
    void Release() override;

    //移動(位置/焦点)
    //引数：移動量
    void Move(XMVECTOR move);

    //位置セット
    void SetCam();

    //カメラ変更関数
    //引数：選択カメラ(1:TPSカメラ 2:FPSカメラ)
    void ChangeCamera(int selectCam);
};