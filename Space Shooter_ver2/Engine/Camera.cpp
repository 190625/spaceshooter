#pragma once

#include "Camera.h"

//変数
XMVECTOR position_ = XMVectorZero();		//カメラの位置（視点）
XMVECTOR target_ = XMVectorZero();			//見る位置（焦点）
XMVECTOR direction_ = XMVectorZero();		//カメラの上方向
XMMATRIX viewMatrix_ = XMMatrixIdentity();	//ビュー行列
XMMATRIX projMatrix_ = XMMatrixIdentity();	//プロジェクション行列

//初期化
void Camera::Initialize()
{
	//初期値設定
	position_ = XMVectorSet(0, 3.0f, 10.0f, 0);				//カメラの位置
	target_ = XMVectorSet(0, 0, 0, 0);						//カメラの焦点
	direction_ = XMVectorSet(0, 1, 0, 0);					//カメラの上方向

	//プロジェクション行列(ズームする場合は、Updateに書く)
	//第一引数：画角(視野角) ※XM_PIが3.14ラジアン＝180度。DIV4は÷4なのでXM_PIDIV4は45度を表す定数
	//第二引数：アスペクト比 ※ウィンドウの縦横の比率　(今数字を直接書いちゃってダサいので、後で何とかしたい)
	//第三引数：ニア(近)	 ※クリッピング面までの距離　カメラからこの値より近いものは映らない
	//第四引数：ファー(遠)	 ※クリッピング面までの距離　カメラからこの値より遠いものは映らない
	projMatrix_ = XMMatrixPerspectiveFovLH(/*XM_PIDIV4*/XMConvertToRadians(90.0f), (FLOAT)800 / (FLOAT)600, 0.1f, 160.0f);
}

//更新
void Camera::Update()
{
	//ビュー行列の作成(視点と焦点はいつ変わるかわからないので、ビュー行列はここで作成//固定の場合は、Initializeに書く)
	//第一引数：カメラの位置(視点)
	//第二引数：見る位置(焦点)
	//第三引数：上方向ベクトル(もしカメラを傾けたいなら、これも変数にする必要あり)
	viewMatrix_ = XMMatrixLookAtLH(position_, target_, direction_);
}

//位置を設定
void Camera::SetPosition(XMVECTOR position)
{
	position_ = position;
}

//焦点を設定
void Camera::SetTarget(XMVECTOR target)
{
	target_ = target;
}

//ビュー行列を取得
XMMATRIX Camera::GetViewMatrix()
{
	return viewMatrix_;
}

//プロジェクション行列を取得
XMMATRIX Camera::GetProjectionMatrix()
{
	return projMatrix_;
}