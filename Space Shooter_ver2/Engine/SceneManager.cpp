#pragma once

#include "SceneManager.h"
#include "../PlayScene.h"
#include "../TitleScene.h"
#include "../ResultScene.h"
#include "Model.h"
#include "Splash.h"


SceneManager::SceneManager(GameObject * parent)
	:GameObject(parent, "SceneManager")
{
	//最初のシーンを設定して初期化
	currentScene_ = SCENE_ID_TITLE;
	nextScene_ = SCENE_ID_TITLE;
}

SceneManager::~SceneManager()
{
}

void SceneManager::Initialize()
{
	Instantiate<TitleScene>(this);

	scoreData_.first_ = 0;
	scoreData_.second_ = 0;
	scoreData_.third_ = 0;
	scoreData_.now_ = 0;

}

void SceneManager::Update()
{
	//シーンを切り替えるとき
	if (currentScene_ != nextScene_)
	{
		//SceneManager以下のオブジェクトを全て削除
		AllChildKill();
		//使用したモデルを全て削除
		Model::AllRelease();
		//使用した画像を全て削除
		Splash::AllRelease();

		//次のシーンを設定する
		switch (nextScene_)
		{
		case SCENE_ID_PLAY:
			Instantiate<PlayScene>(this); break;
		case SCENE_ID_TITLE:
			Instantiate<TitleScene>(this); break;
		case SCENE_ID_RESULT:
			Instantiate<ResultScene>(this); break;
		default:
			break;
		}

		//現在のシーンも更新
		currentScene_ = nextScene_;
	}
}

void SceneManager::Draw()
{
}

void SceneManager::Release()
{
}

void SceneManager::ChangeScene(SCENE_ID nextScene)
{
	nextScene_ = nextScene;
}

void SceneManager::SetScore(int score)
{
	scoreData_.now_ = score;
}

//スコアランキングを更新
void SceneManager::UpdateScore()
{
	if (scoreData_.first_ < scoreData_.now_)
	{
		scoreData_.third_ = scoreData_.second_;
		scoreData_.second_ = scoreData_.first_;
		scoreData_.first_ = scoreData_.now_;
	}
	else
		if (scoreData_.second_ < scoreData_.now_)
		{
			scoreData_.third_ = scoreData_.second_;
			scoreData_.second_ = scoreData_.now_;
		}
		else
			if (scoreData_.third_ < scoreData_.now_)
				scoreData_.third_ = scoreData_.now_;
}

//スコアを取得
int SceneManager::GetScore(int rank)
{
	switch (rank)
	{
	case 1:
		return scoreData_.first_;
	case 2:
		return scoreData_.second_;
	case 3:
		return scoreData_.third_;
	default:
		break;
	}
}
