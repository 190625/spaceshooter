#pragma once

#include "GameObject.h"

//使用するシーン
enum SCENE_ID
{
	SCENE_ID_TITLE = 0,
	SCENE_ID_PLAY,
	SCENE_ID_RESULT,
};

class SceneManager :
	public GameObject
{
public:
	//コンストラクタ
	//引数：parent  親オブジェクト（RootJob）
	SceneManager(GameObject* parent);
	~SceneManager();					//デストラクタ


	//変数宣言
	SCENE_ID currentScene_;	//現在のシーンID
	SCENE_ID nextScene_;	//次の(切り替えたい)シーンID

	struct ScoreData		//スコアを管理する構造体
	{
		int first_;			//一位
		int second_;		//二位
		int third_;			//三位
		int now_;			//現在のスコア
	}scoreData_;

	//仮想関数
	virtual void Initialize() override;
	virtual void Update() override;
	virtual void Draw() override;
	virtual void Release() override;


	//関数

	//次の(切り替えたい)シーンを設定する処理
	void ChangeScene(SCENE_ID nextScene);

	//現在のスコアをセットする
	//引数：スコア
	void SetScore(int score);

	//スコアランキングを更新
	void UpdateScore();

	//スコアを取得
	//引数:順位
	//戻り値：スコア
	int GetScore(int rank);
};

