#pragma once

#include "Input.h"

namespace Input
{
	//実際にデバイスを管理するもの(ポインタ)
	LPDIRECTINPUT8   pDInput = nullptr;


	////////////////////////キーボード関係////////////////////////

	//キーボード入力用
	LPDIRECTINPUTDEVICE8 pKeyDevice = nullptr;
	//現フレームの状態を入れておく配列(一応256個分用意)
	BYTE keyState[256] = { 0 };
	//前フレームの状態を入れておく配列(一応256個分用意)
	BYTE prevKeyState[256];


	////////////////////////マウス関係////////////////////////

	//マウスの位置を入れておく変数
	XMVECTOR mousePosition;
	//マウス入力用
	LPDIRECTINPUTDEVICE8 pMouseDevice = nullptr;
	//現フレームの状態を入れておく配列
	DIMOUSESTATE mouseState;
	//前フレームの状態を入れておく配列
	DIMOUSESTATE prevMouseState;


	////////////////////////コントローラー関係////////////////////////

	//現フレームの状態を入れておく変数
	XINPUT_STATE controllerState_;
	//前フレームの状態を入れておく変数
	XINPUT_STATE prevControllerState_;


	HRESULT Input::Initialize(HWND hWnd)
	{
		/////////////////////////////////IDirectInput8オブジェクトを作成/////////////////////////////////

		if (FAILED(DirectInput8Create(GetModuleHandle(nullptr), DIRECTINPUT_VERSION, IID_IDirectInput8, (VOID**)&pDInput, nullptr)))
		{
			MessageBox(nullptr, "DirectInputの初期化失敗", "エラー", MB_OK);
			return E_FAIL;
		}


		/////////////////////////////////デバイスオブジェクトを作成(キーボード用)/////////////////////////////////

		if (FAILED(pDInput->CreateDevice(GUID_SysKeyboard, &pKeyDevice, nullptr)))
		{
			MessageBox(nullptr, "デバイスオブジェクトの作成失敗", "エラー", MB_OK);
			return E_FAIL;
		}

		//デバイスを指定(キーボード)
		if (FAILED(pKeyDevice->SetDataFormat(&c_dfDIKeyboard)))
		{
			MessageBox(nullptr, "デバイス指定の失敗(キーボード)", "エラー", MB_OK);
			return E_FAIL;
		}

		//強調レベルの設定(他の実行中のアプリに対する優先度)
		if (FAILED(pKeyDevice->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND)))
		{
			MessageBox(nullptr, "強調レベルの設定失敗", "エラー", MB_OK);
			return E_FAIL;
		}


		/////////////////////////////////デバイスオブジェクトを作成(マウス用)/////////////////////////////////

		if (FAILED(pDInput->CreateDevice(GUID_SysMouse, &pMouseDevice, nullptr)))
		{
			MessageBox(nullptr, "デバイスオブジェクトの作成失敗", "エラー", MB_OK);
			return E_FAIL;
		}

		//デバイスを指定(キーボード)
		if (FAILED(pMouseDevice->SetDataFormat(&c_dfDIMouse)))
		{
			MessageBox(nullptr, "デバイス指定の失敗(マウス)", "エラー", MB_OK);
			return E_FAIL;
		}

		//強調レベルの設定(他の実行中のアプリに対する優先度)
		if (FAILED(pMouseDevice->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND)))
		{
			MessageBox(nullptr, "強調レベルの設定失敗", "エラー", MB_OK);
			return E_FAIL;
		}

		return S_OK;
	}

	HRESULT Input::Update()
	{
		/////////////////////////////////前フレームの状態を取得/////////////////////////////////

		//全キーの状態を配列にコピー
		//第一引数：コピー先
		//第二引数：コピー元
		//第三引数：コピーサイズ

		//キーボード用
		memcpy(prevKeyState, keyState, 256);

		//マウス用
		memcpy(&prevMouseState, &mouseState, sizeof(mouseState));

		//コントローラー用
		memcpy(&prevControllerState_, &controllerState_, sizeof(controllerState_));


		/////////////////////////////////各デバイスの状態を取得/////////////////////////////////


		///////////////キーボード用/////////////

		//入力開始を宣言
		if (FAILED(pKeyDevice->Acquire()))
		{
			MessageBox(nullptr, "入力開始宣言の失敗", "エラー", MB_OK);
			return E_FAIL;
		}

		//デバイスからデータを取得
		if (FAILED(pKeyDevice->GetDeviceState(sizeof(keyState), &keyState)))
		{
			MessageBox(nullptr, "デバイスからのデータ取得失敗", "エラー", MB_OK);
			return E_FAIL;
		}


		///////////////マウス用/////////////

		//入力開始を宣言
		if (FAILED(pMouseDevice->Acquire()))
		{
			MessageBox(nullptr, "入力開始宣言の失敗", "エラー", MB_OK);
			return E_FAIL;
		}

		//デバイスからデータを取得
		if (FAILED(pMouseDevice->GetDeviceState(sizeof(mouseState), &mouseState)))
		{
			MessageBox(nullptr, "デバイスからのデータ取得失敗", "エラー", MB_OK);
			return E_FAIL;
		}


		///////////////コントローラー用/////////////

		//デバイスからデータを取得
		XInputGetState(0, &controllerState_);

		return S_OK;
	}

	//キーが押されているかどうか
	bool Input::IsKey(int keyCode)
	{
		//押されていたら(押されている場合は、右から8ビット目が1になっているので、ビット演算で調べる)
		if (keyState[keyCode] & 0x80)
		{
			return true;
		}

		return false;
	}

	//キーを押したかどうか
	bool IsKeyDown(int keyCode)
	{
		//現在の状態と前の状態を比較
		if (keyState[keyCode] & 0x80 && prevKeyState[keyCode] ^ 0x80)
		{
			return true;
		}

		return false;
	}

	//キーを離したかどうか
	bool IsKeyUp(int keyCode)
	{
		//現在の状態と前の状態を比較
		if (keyState[keyCode] ^ 0x80 && prevKeyState[keyCode] & 0x80)
		{
			return true;
		}
		return false;
	}

	//ボタンが押されているかどうか
	bool IsMouseButton(int buttonCode)
	{
		//押されていたら(押されている場合は、右から8ビット目が1になっているので、ビット演算で調べる)
		//※[0]が左、[1]が右、[2]が中央のボタンを表す
		if (mouseState.rgbButtons[buttonCode] & 0x80)
		{
			return true;
		}
		return false;
	}

	//ボタンを押したかどうか
	bool IsMouseButtonDown(int buttonCode)
	{
		//現在の状態と前の状態を比較
		if (IsMouseButton(buttonCode) && prevMouseState.rgbButtons[buttonCode] ^ 0x80)
		{
			return true;
		}
		return false;
	}

	//ボタンを離したかどうか
	bool IsMouseButtonUp(int buttonCode)
	{
		//現在の状態と前の状態を比較
		if (!IsMouseButton(buttonCode) && prevMouseState.rgbButtons[buttonCode] & 0x80)
		{
			return true;
		}
		return false;
	}

	//マウスの移動量を取得する
	//※「lXにX軸の移動量」、「lYにY軸の移動量」、「lZにホイールの移動量」が入っている
	//※この関数が前回呼び出された時点を0としてからの相対値
	XMVECTOR GetMouseMove()
	{
		XMVECTOR result = XMVectorSet((float)mouseState.lX, -(float)mouseState.lY, (float)mouseState.lZ, 0);
		return result;
	}

	//マウスの位置を取得
	XMVECTOR GetMousePosition()
	{
		return mousePosition;
	}

	//マウスの位置を設定
	void SetMousePosition(int x, int y)
	{
		mousePosition = XMVectorSet((float)x, (float)y, 0, 0);
	}

	//ボタンが押されているかどうか
	bool IsPadButton(int buttonCode)
	{
		if (controllerState_.Gamepad.wButtons & buttonCode)
		{
			return true;
		}
		return false;
	}

	//ボタンを押したかどうか
	bool IsPadButtonDown(int buttonCode)
	{
		//現在の状態と前の状態を比較
		if (IsPadButton(buttonCode) && !prevControllerState_.Gamepad.wButtons & buttonCode)
		{
			return true;
		}
		return false;
	}

	//ボタンを離したかどうか
	bool IsPadButtonUp(int buttonCode)
	{
		//現在の状態と前の状態を比較
		if (!IsPadButton(buttonCode) && prevControllerState_.Gamepad.wButtons & buttonCode)
		{
			return true;
		}
		return false;
	}

	void Input::Release()
	{
		//解放処理
		SAFE_RELEASE(pMouseDevice);
		SAFE_RELEASE(pKeyDevice);
		SAFE_RELEASE(pDInput);
	}
}