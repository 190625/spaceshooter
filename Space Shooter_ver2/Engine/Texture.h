#pragma once

#include <d3d11.h>
#include "string"

class Texture
{
	//変数宣言
	ID3D11SamplerState*	pSampler_;				//サンプラー(ポインタ)(テクスチャの貼り方を決める)
	ID3D11ShaderResourceView*	pSRV_;			//シェーダーリソースビュー(ポインタ)(テクスチャをシェーダーに渡すもの)

	UINT imgWidth_;								//画像サイズ(横幅)
	UINT imgHeight_;							//画像サイズ(縦幅)

public:
	Texture();
	~Texture();
	HRESULT Load(std::string fileName);
	void Release();
	ID3D11SamplerState* GetSampler() { return pSampler_; };			//サンプラーを渡す
	ID3D11ShaderResourceView* GetSRV() { return pSRV_; };			//シェーダーリソースビューを渡す
	UINT GetWidth() { return imgWidth_; };							//画像サイズを渡す(横幅)
	UINT GetHeight() { return imgHeight_; };						//画像サイズを渡す(縦幅)
};