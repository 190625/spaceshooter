#pragma once

#include <wincodec.h>
#include "Texture.h"
#include "Direct3D.h"

using namespace Direct3D;

#pragma comment( lib, "WindowsCodecs.lib" )

Texture::Texture()
{
	//作成した変数を初期化
	pSampler_ = nullptr;		//サンプラー
	pSRV_ = nullptr;			//シェーダーリソースビュー

	imgWidth_ = NULL;			//画像サイズ(横幅)
	imgHeight_ = NULL;			//画像サイズ(縦幅)
}

Texture::~Texture()
{
}

HRESULT Texture::Load(std::string fileName)
{
	/////////////////////////////////////////////ファイル名をワイド文字に変換/////////////////////////////////////////////

	wchar_t wtext[FILENAME_MAX];
	size_t ret;
	//マルチバイト→ワイド
	mbstowcs_s(&ret, wtext, fileName.c_str(), fileName.length());


	////////////////////////////////////////////////////COMの初期化処理////////////////////////////////////////////////////
	//COMライブラリを初期化し、同時実行モデルをシングルスレッドアパートメント(STA)として識別する
	//引数はNULLである必要がある
	if (FAILED(CoInitialize(nullptr)))
	{
		MessageBox(nullptr, "COMライブラリはすでに初期化されています", "エラー", MB_OK);
		return E_FAIL;
	}

	//デコーダー、エンコーダー、ピクセル形式コンバーターなど、
	//Windows Imaging Component（WIC）のコンポーネントを作成するために使用されるメソッドを公開する
	IWICImagingFactory *pFactory = nullptr;

	//デコーダーを表すメソッドを公開する
	IWICBitmapDecoder *pDecoder = nullptr;

	//エンコードされたファイルの個々の画像フレームをデコードする方法を定義する
	IWICBitmapFrameDecode* pFrame = nullptr;

	//画像データをあるピクセル形式から別のピクセル形式に変換し、
	//ディザリングとハーフトーンをインデックス形式に処理し、パレット変換とアルファしきい値を処理するIWICBitmapSourceを表す
	IWICFormatConverter* pFormatConverter = nullptr;

	//指定されたCLSIDに関連付けられたクラスの単一のオブジェクトを作成してデフォルトで初期化する
	if (FAILED(CoCreateInstance(CLSID_WICImagingFactory, nullptr, CLSCTX_INPROC_SERVER, IID_IWICImagingFactory, reinterpret_cast<void **>(&pFactory))))
	{
		MessageBox(nullptr, "指定されたCLSIDのオブジェクト作成/初期化失敗", "エラー", MB_OK);
		return E_FAIL;
	}

	//指定されたファイルに基づいて、IWICBitmapDecoderクラスの新しいインスタンスを作成
	HRESULT hr = pFactory->CreateDecoderFromFilename(wtext, NULL, GENERIC_READ, WICDecodeMetadataCacheOnDemand, &pDecoder);
	if (FAILED(hr))
	{
		MessageBox(nullptr, "IWICBitmapDecoderクラスのインスタンス作成失敗", "エラー", MB_OK);
		return E_FAIL;
	}

	//画像の指定されたフレームを取得
	if (FAILED(pDecoder->GetFrame(0, &pFrame)))
	{
		MessageBox(nullptr, "指定された画像フレームの取得失敗", "エラー", MB_OK);
		return E_FAIL;
	}

	//IWICFormatConverterクラスの新しいインスタンスを作成
	if (FAILED(pFactory->CreateFormatConverter(&pFormatConverter)))
	{
		MessageBox(nullptr, "IWICFormatConverterクラスのインスタンス作成失敗", "エラー", MB_OK);
		return E_FAIL;
	}

	//フォーマットコンバーターを初期化
	if (FAILED(pFormatConverter->Initialize(pFrame, GUID_WICPixelFormat32bppRGBA, WICBitmapDitherTypeNone, NULL, 1.0f, WICBitmapPaletteTypeMedianCut)))
	{
		MessageBox(nullptr, "フォーマットコンバーターの初期化失敗", "エラー", MB_OK);
		return E_FAIL;
	}

	////////////////////////////////////////////////////COMの終了処理////////////////////////////////////////////////////
	//現在のスレッドでCOMライブラリを閉じ、
	//スレッドによってロードされたすべてのDLLをアンロードし、
	//スレッドが維持している他のリソースを解放し、スレッド上のすべてのRPC接続を強制的に閉じる
	CoUninitialize();

	////////////////////////////////////////////////////画像サイズを調べる////////////////////////////////////////////////////

	//ビットマップのピクセル幅と高さを取得
	if (FAILED(pFormatConverter->GetSize(&imgWidth_, &imgHeight_)))
	{
		MessageBox(nullptr, "ビットマップのピクセル幅と高さの取得失敗", "エラー", MB_OK);
		return E_FAIL;
	}


	///////////////////////////////////////////////////テクスチャの作成///////////////////////////////////////////////////

	//構造化メモリであるテクセルデータを管理する
	ID3D11Texture2D*	pTexture;

	//2Dテクスチャを作成するために必要な構造体
	D3D11_TEXTURE2D_DESC texdec;
	texdec.Width = imgWidth_;											//テクスチャ幅(テクセル単位)
	texdec.Height = imgHeight_;											//テクスチャ高さ(テクセル単位)
	texdec.MipLevels = 1;												//テクスチャ内のミップマップレベルの最大数
	texdec.ArraySize = 1;												//テクスチャ配列内のテクスチャの数
	texdec.Format = DXGI_FORMAT_R8G8B8A8_UNORM;							//テクスチャフォーマット
	texdec.SampleDesc.Count = 1;										//ピクセルあたりのマルチサンプルの数
	texdec.SampleDesc.Quality = 0;										//画質レベル
	texdec.Usage = D3D11_USAGE_DYNAMIC;									//テクスチャの読み取りおよび書き込み方法を識別する値
	texdec.BindFlags = D3D11_BIND_SHADER_RESOURCE;						//パイプラインステージにバインドするためのフラグ
	texdec.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;						//許可されるCPUアクセスのタイプを指定するフラグ
	texdec.MiscFlags = 0;												//他のあまり一般的ではないリソースオプションを識別するフラグ

	//2Dテクスチャの配列を作成
	if (FAILED(Direct3D::pDevice->CreateTexture2D(&texdec, nullptr, &pTexture)))
	{
		MessageBox(nullptr, "2Dテクスチャの作成失敗", "エラー", MB_OK);
		return E_FAIL;
	}


	///////////////////////////////////////////テクスチャをコンテキストに渡す///////////////////////////////////////////

	//サブリソースデータへのアクセスを提供
	D3D11_MAPPED_SUBRESOURCE hMappedres;

	//Map関数で動きをいったん止めて書き込み可能状態にする
	if (FAILED(Direct3D::pContext->Map(pTexture, 0, D3D11_MAP_WRITE_DISCARD, 0, &hMappedres)))				// GPUからのデータアクセスを止める
	{
		MessageBox(nullptr, "コンテキストへのテクスチャデータの書き込み失敗", "エラー", MB_OK);
		return E_FAIL;
	}

	//ピクセルを生成するようオブジェクトに指示
	//第一引数：コピーする長方形(NULLの値は、全体のビットマップを指定)
	//第二引数：ビットマップのストライド(横一列)
	//第三引数：バッファのサイズ(全体のサイズ)
	//第四引数：バッファへのポインタ
	//(それぞれ1ピクセル4バイトなので4倍している)
	if (FAILED(pFormatConverter->CopyPixels(nullptr, imgWidth_ * 4, imgWidth_ * imgHeight_ * 4, (BYTE*)hMappedres.pData)))
	{
		MessageBox(nullptr, "オブジェクトへのピクセル生成の指示失敗", "エラー", MB_OK);
		return E_FAIL;
	}

	//Unmapで動きを再開させる
	Direct3D::pContext->Unmap(pTexture, 0);																	//再開


	//////////////////////////////////////////サンプラー作成//////////////////////////////////////////

	//サンプラーを作成するための構造体
	D3D11_SAMPLER_DESC  SamDesc;
	ZeroMemory(&SamDesc, sizeof(D3D11_SAMPLER_DESC));				//メモリのブロックをゼロで埋める
	SamDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;				//テクスチャをサンプリングするときに使用するフィルタリング方法

	//テクスチャの境界の外側にあるテクスチャ座標を解決するための手法を特定する
	SamDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;					//0から1の範囲外のauテクスチャ座標を解決するために使用するメソッド
	SamDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;					//0から1の範囲外のavテクスチャ座標を解決するために使用するメソッド
	SamDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;					//0から1の範囲外のawテクスチャ座標を解決するために使用するメソッド

	//サンプラーの作成
	if (FAILED(Direct3D::pDevice->CreateSamplerState(&SamDesc, &pSampler_)))
	{
		MessageBox(nullptr, "サンプラーの作成失敗", "エラー", MB_OK);
		return E_FAIL;
	}


	/////////////////////////////////////シェーダーリソースビューの作成/////////////////////////////////////

	//シェーダーリソースビューを作成するための構造体
	D3D11_SHADER_RESOURCE_VIEW_DESC srv = {};
	srv.Format = DXGI_FORMAT_R8G8B8A8_UNORM;						//DXGI_FORMAT表示形式を指定
	srv.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;				//ビューのリソースタイプ
	srv.Texture2D.MipLevels = 1;									//シェーダーリソースビューからの情報を使用して、リソースを2Dテクスチャとして表示

	//シェーダーリソースビューの作成
	if (FAILED(Direct3D::pDevice->CreateShaderResourceView(pTexture, &srv, &pSRV_)))
	{
		MessageBox(nullptr, "シェーダーリソースビューの作成失敗", "エラー", MB_OK);
		return E_FAIL;
	}


	//関数内で作成したポインタ変数を解放
	SAFE_RELEASE(pTexture);

	return S_OK;
}

void Texture::Release()
{
	//解放処理
	SAFE_RELEASE(pSRV_);			//シェーダーリソースビューのポインタ変数
	SAFE_RELEASE(pSampler_);		//サンプラーのポインタ変数
}