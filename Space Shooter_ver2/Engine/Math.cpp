#pragma once

#include "Math.h"

//行列式(クラメルの公式)
float Math::Det(XMVECTOR a, XMVECTOR b, XMVECTOR c)
{
	return	(a.vecX * b.vecY * c.vecZ) +
			(a.vecY * b.vecZ * c.vecX) +
			(a.vecZ * b.vecX * c.vecY) -
			(a.vecX * b.vecZ * c.vecY) -
			(a.vecY * b.vecX * c.vecZ) -
			(a.vecZ * b.vecY * c.vecX);
}

bool Math::Intersect(XMVECTOR origin, XMVECTOR ray, XMVECTOR v0, XMVECTOR v1, XMVECTOR v2, float * t)
{
	//ポリゴンの線分の長さを記憶する変数
	XMVECTOR edge1 = v1 - v0;
	XMVECTOR edge2 = v2 - v0;

	//計算結果
	XMVECTOR result = origin - v0;

	//各式の・・・
	//X要素
	XMVECTOR a = { edge1.vecX,edge2.vecX,-ray.vecX,0 };
	//Y要素
	XMVECTOR b = { edge1.vecY,edge2.vecY,-ray.vecY,0 };
	//Z要素
	XMVECTOR c = { edge1.vecZ,edge2.vecZ,-ray.vecZ,0 };
	//解要素
	XMVECTOR d = { result.vecX,result.vecY,result.vecZ,0 };

	//分母を求める(基本となる平行四辺形)
	float denom = Det(a, b, c);

	//飛ばしたレイが平行かどうか
	if (denom < 0)
	{
		return false;
	}

	//分子を求める(各ベクトル方向に伸びた平行四辺形)
	float numX = Det(d, b, c);
	float numY = Det(a, d, c);
	float numZ = Det(a, b, d);

	//求めたい値(各ベクトル方向に伸びた平行四辺形を基本となる平行四辺形で割る)
	float x = numX / denom;
	float y = numY / denom;
	*t = numZ / denom;

	//当たっているかどうか
	if (0 <= x && 0 <= y && (x + y) <= 1 && 0 <= *t)
		return true;
	else
		return false;
}