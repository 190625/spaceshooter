#pragma once

#include "Transform.h"

Transform::Transform()
{
	//作成した変数を初期化
	matTranslate_ = XMMatrixIdentity();				//移動行列
	matRotate_ = XMMatrixIdentity();				//回転行列
	matScale_ = XMMatrixIdentity();					//拡大行列

	position_ = XMVectorZero();						//位置
	rotate_ = XMVectorZero();						//向き
	scale_ = XMVectorSet(1.0f, 1.0f, 1.0f, 0.0f);	//拡大率
}

Transform::~Transform()
{
}

//各行列の計算
void Transform::Calclation()
{
	//位置ベクトルの各要素から移動行列を作成
	matTranslate_ = XMMatrixTranslation(position_.vecX, position_.vecY, position_.vecZ);

	//向きベクトルの各要素から回転行列を作成
	XMMATRIX rotateX, rotateY, rotateZ;
	rotateX = XMMatrixRotationX(rotate_.vecX);
	rotateY = XMMatrixRotationY(rotate_.vecY);
	rotateZ = XMMatrixRotationZ(rotate_.vecZ);
	matRotate_ = rotateX * rotateY * rotateZ;

	//拡大率ベクトルの各要素から拡大行列を作成
	matScale_ = XMMatrixScaling(scale_.vecX, scale_.vecY, scale_.vecZ);
}

//ワールド行列を渡す
XMMATRIX Transform::GetWorldMatrix()
{
	//拡大→回転→移動　の順で計算する
	return XMMATRIX(matScale_ * matRotate_ * matTranslate_);
}
