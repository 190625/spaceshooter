#pragma once

#include "SphereCollider.h"
#include "BoxCollider.h"
#include "Model.h"

SphereCollider::SphereCollider(XMVECTOR center, float radius)
{
	transform_.position_ = center;
	transform_.scale_ = XMVectorSet(radius, radius, radius, 0);
	type_ = COLLIDER_CIRCLE;

	//リリース時は判定枠は表示しない
#ifdef _DEBUG
	//テスト表示用判定枠
	hDebugModel_ = Model::Load("Assets/DebugCollision/sphereCollider.fbx");
#endif
}

bool SphereCollider::IsHit(Collider * target)
{
	//指定した相手が箱型コライダーの場合
	if (target->type_ == COLLIDER_BOX)
		return IsHitBoxVsCircle((BoxCollider*)target, this);		//判定結果を返す
	//指定した相手が球体コライダーの場合
	else
		return IsHitCircleVsCircle((SphereCollider*)target, this);	//判定結果を返す
}
