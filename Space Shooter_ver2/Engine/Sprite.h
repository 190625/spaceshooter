#pragma once

#include <DirectXMath.h>
#include "Direct3D.h"
#include "Texture.h"
#include "Transform.h"

using namespace DirectX;


class Sprite
{
	//コンスタントバッファ(渡す情報をまとめた構造体を用意)
	struct CONSTANT_BUFFER
	{
		XMMATRIX	matW;		//ワールド行列
		XMFLOAT4	color;		//色(アルファ値の更新に用いる)
	};

	//頂点情報(頂点シェーダーに渡す情報の構造体)
	struct VERTEX
	{
		XMVECTOR position;			//位置
		XMVECTOR uv;				//UV座標
	};


	//変数宣言
	ID3D11Buffer *pVertexBuffer_;		//頂点バッファ(ポインタ)(頂点情報の置き場所)
	ID3D11Buffer *pIndexBuffer_;		//インデックスバッファ(ポインタ)(インデックス情報の置き場所)
	ID3D11Buffer *pConstantBuffer_;		//コンスタントバッファ(ポインタ)(カメラ・ライト....情報の置き場所)

	Texture* pTexture_;					//Texture型のポインタ


public:
	Sprite();
	~Sprite();
	HRESULT Initialize(std::string fileName);
	HRESULT Draw(Transform& taransform, float alpha);
	void Release();

	//画像サイズを渡す
	XMVECTOR GetTextureSize() { return { (float)pTexture_->GetWidth(),(float)pTexture_->GetHeight(),0.0f,0.0f }; };
};