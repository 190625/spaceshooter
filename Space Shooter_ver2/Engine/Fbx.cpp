#pragma once

#include "Fbx.h"
#include "Camera.h"
#include "Math.h"

Fbx::Fbx()
{
	//作成した変数を初期化
	pVertexBuffer_ = nullptr;				//頂点バッファ
	ppIndexBuffer_ = nullptr;				//インデックスバッファ
	pConstantBuffer_ = nullptr;				//コンスタントバッファ

	vertexCount_ = NULL;					//頂点数
	polygonCount_ = NULL;					//ポリゴン数

	materialCount_ = NULL;					//マテリアル数
	pMaterialList_ = nullptr;				//マテリアル数分の配列
	pIndexCountEachMaterial_ = nullptr;		//マテリアルごとのインデックス数

	pVertices_ = nullptr;					//頂点情報
	ppIndex_ = nullptr;						//インデックス情報
}

HRESULT Fbx::Load(std::string fileName)
{
	////////////////////////////////////FBXデータの読み込み////////////////////////////////////

	//マネージャを生成(全てを統括するオブジェクト)
	FbxManager* pFbxManager = FbxManager::Create();


	//インポーターを生成(FBXファイルからデータを取り出すオブジェクト)
	FbxImporter *fbxImporter = FbxImporter::Create(pFbxManager, "imp");
	//作成したインポーターを初期化
	fbxImporter->Initialize(fileName.c_str(), -1, pFbxManager->GetIOSettings());


	////////////////シーンオブジェクトにFBXファイルの情報を流し込む////////////////

	//シーンオブジェクト生成
	FbxScene*	pFbxScene = FbxScene::Create(pFbxManager, "fbxscene");
	fbxImporter->Import(pFbxScene);


	//////////////////////////メッシュ情報(形に関する情報)取得//////////////////////////

	//ルートノード(FBX情報のトップ)を取得
	FbxNode* rootNode = pFbxScene->GetRootNode();

	//子ノードを取得
	FbxNode* pNode = rootNode->GetChild(0);

	//メッシュ情報を取得
	FbxMesh* mesh = pNode->GetMesh();


	//////////各情報の個数を取得//////////
	vertexCount_ = mesh->GetControlPointsCount();		//頂点の数
	polygonCount_ = mesh->GetPolygonCount();			//ポリゴンの数
	materialCount_ = pNode->GetMaterialCount();			//マテリアルの数


	///////////////////////////////////////カレントディレクトリの変更///////////////////////////////////////

	//現在のカレントディレクトリを取得
	char defaultCurrentDir[MAX_PATH];						//現在のカレントディレクトリを入れる変数
	GetCurrentDirectory(MAX_PATH, defaultCurrentDir);		//現在のカレントディレクトリを取得して、変数に代入

	//使いたいファイルのディレクトリ名を取得
	char dir[MAX_PATH];																	//使いたいファイルのディレクトリ名を入れる変数
	_splitpath_s(fileName.c_str(), nullptr, 0, dir, MAX_PATH, nullptr, 0, nullptr, 0);	//使いたいファイルのディレクトリ名だけを取得し、変数に代入

	//カレントディレクトリを変更
	SetCurrentDirectory(dir);


	///////////////////////////頂点バッファ準備(メッシュ情報から頂点情報を抜き出す)///////////////////////////
	if (FAILED(InitVertex(mesh)))
	{
		MessageBox(nullptr, "メッシュ情報から頂点バッファの作成失敗", "エラー", MB_OK);
		return E_FAIL;
	}


	////////////////////インデックスバッファ準備(メッシュ情報からインデックス情報を抜き出す)////////////////////
	if (FAILED(InitIndex(mesh)))
	{
		MessageBox(nullptr, "メッシュ情報からインデックスバッファの作成失敗", "エラー", MB_OK);
		return E_FAIL;
	}


	//////////////////////////////////////////コンスタントバッファ準備//////////////////////////////////////////
	if (FAILED(InitConstantBuffer()))
	{
		MessageBox(nullptr, "コンスタントバッファの作成失敗", "エラー", MB_OK);
		return E_FAIL;
	}


	//////////////////////////マテリアル情報準備(ノードから探索してマテリアル情報を抜き出す)//////////////////////////
	if (FAILED(InitMaterial(pNode)))
	{
		MessageBox(nullptr, "マテリアル情報の抜き出し失敗", "エラー", MB_OK);
		return E_FAIL;
	}


	///////カレントディレクトリを変更前のカレントディレクトリに戻す///////
	SetCurrentDirectory(defaultCurrentDir);


	return S_OK;
}

//頂点バッファ準備
HRESULT Fbx::InitVertex(fbxsdk::FbxMesh * mesh)
{
	////////////////////////////////////頂点情報を取得////////////////////////////////////

	//頂点情報を入れる配列(メッシュ情報から抜き出した頂点数分の配列)
	pVertices_ = new VERTEX[vertexCount_];

	//全ポリゴン(メッシュ情報から抜き出したポリゴン数分繰り返す)
	for (DWORD poly = 0; poly < (DWORD)polygonCount_; poly++)
	{
		//3頂点分(三角形で表示するため)
		for (int vertex = 0; vertex < 3; vertex++)
		{
			//調べる頂点の番号(調べたい頂点を指定して(〜番目のポリゴンの〜番目の頂点)情報を取得)
			int index = mesh->GetPolygonVertex(poly, vertex);

			//調べたい頂点の位置を記憶
			FbxVector4 pos = mesh->GetControlPointAt(index);

			//記憶したものを配列に代入する
			pVertices_[index].position = XMVectorSet((float)-pos[0], (float)pos[1], (float)pos[2], 0.0f);
			//※DirectXとMayaでは、Z方向が違うので、Xの符号を反転させることで、正しく表示されるようにしている


			//調べたいUV座標の位置を記憶
			FbxVector2 uv = mesh->GetLayer(0)->GetUVs()->GetDirectArray().GetAt(index);

			//記憶したものを配列に代入する
			pVertices_[index].uv = XMVectorSet((float)uv.mData[0], (float)(1.0 - uv.mData[1]), 0, 0);
			//※V座標は上下を反転させなければいけない


			//調べたい法線の位置を記憶する変数
			FbxVector4 normal;

			//調べたものを変数に代入(調べたい法線を指定して(〜番目のポリゴンの〜番目の頂点の法線)情報を取得)
			mesh->GetPolygonVertexNormal(poly, vertex, normal);

			//変数に記憶したものを配列に代入する
			pVertices_[index].normal = XMVectorSet((float)-normal[0], (float)normal[1], (float)normal[2], 0.0f);
			//※DirectXとMayaでは、Z方向が違うので、Xの符号を反転させることで、正しく表示されるようにしている
		}
	}

	////////////////////////////////////頂点バッファの設定////////////////////////////////////
	//専用の変数を用意
	D3D11_BUFFER_DESC bd_vertex;

	//バッファのサイズ(メッシュ情報から取得した頂点数分をかけたサイズで指定する)
	bd_vertex.ByteWidth = sizeof(VERTEX) * vertexCount_;

	//バッファで想定されている読み込みおよび書き込みの方法を識別
	bd_vertex.Usage = D3D11_USAGE_DEFAULT;

	//バッファをどのようにパイプラインにバインドするかを識別
	bd_vertex.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	//CPUアクセスのフラグ
	bd_vertex.CPUAccessFlags = 0;

	//その他のフラグ
	bd_vertex.MiscFlags = 0;

	//バッファが構造化バッファを表す場合の、バッファ構造内の各要素のサイズ
	bd_vertex.StructureByteStride = 0;

	//専用の変数を用意
	D3D11_SUBRESOURCE_DATA data_vertex;

	//サブリソースを初期化するためのデータを指定
	data_vertex.pSysMem = pVertices_;

	//それを使って頂点バッファを作成
	if (FAILED(Direct3D::pDevice->CreateBuffer(&bd_vertex, &data_vertex, &pVertexBuffer_)))
	{
		MessageBox(nullptr, "頂点バッファ作成失敗", "エラー", MB_OK);
		return E_FAIL;
	}

	return S_OK;
}

//インデックスバッファ準備
HRESULT Fbx::InitIndex(fbxsdk::FbxMesh * mesh)
{
	//インデックス情報をバッファに入れるために配列にする(マテリアル数分の配列)
	//※色ごとにまとめる[(例)0,1,2 は　赤[0] , 0,2,3 は　青[1]　みたいなイメージ?]
	ppIndexBuffer_ = new ID3D11Buffer*[materialCount_];


	//インデックス情報を入れる配列(マテリアル数分の配列)
	ppIndex_ = new int*[materialCount_];


	//マテリアルごとのインデックス数を入れる配列(マテリアル数分の配列)
	pIndexCountEachMaterial_ = new int[materialCount_];


	//マテリアルの数分繰り返す
	for (DWORD i = 0; i < (DWORD)materialCount_; i++)
	{
		//i番目のマテリアルのインデックス数分
		ppIndex_[i] = new int[polygonCount_ * 3];

		//配列の要素を更新するための変数
		int count = 0;

		//全ポリゴン(メッシュ情報から抜き出したポリゴン数繰り返す)
		for (DWORD poly = 0; poly < (DWORD)polygonCount_; poly++)
		{
			//////poly番目のポリゴンに割り当てられているマテリアルの番号を調べる//////
			FbxLayerElementMaterial *   mtl = mesh->GetLayer(0)->GetMaterials();
			int mtlId = mtl->GetIndexArray().GetAt(poly);
			//※今、調べているポリゴンが何色なのかを取得
			//　例)調べたポリゴンが赤色を使っている

			////////////マテリアルの番号がi番目ならインデックス情報を追加////////////
			//※今、調べている色が赤色だったら、インデックス情報を追加する
			if (mtlId == i)
			{
				//3頂点分(三角形で表示するため)
				for (int vertex = 2; vertex >= 0; vertex--)
				{
					//調べるインデックスの番号(調べたい頂点を指定して(〜番目のマテリアルの〜番目のポリゴンの〜番目の頂点)情報を取得)を配列に代入
					ppIndex_[i][count] = mesh->GetPolygonVertex(poly, vertex);

					//配列の要素を進める
					count++;
				}
			}
		}


		//調べたマテリアルのインデックス数を記憶
		pIndexCountEachMaterial_[i] = count;


		///////////////////////////////////////インデックスバッファの設定///////////////////////////////////////

		//専用の変数を用意
		D3D11_BUFFER_DESC   bd;

		//バッファで想定されている読み込みおよび書き込みの方法を識別
		bd.Usage = D3D11_USAGE_DEFAULT;

		//バッファのサイズ(インデックス数分のサイズで指定する)
		bd.ByteWidth = sizeof(int) * count;

		//バッファをどのようにパイプラインにバインドするかを識別
		bd.BindFlags = D3D11_BIND_INDEX_BUFFER;

		//CPUアクセスのフラグ
		bd.CPUAccessFlags = 0;

		//その他のフラグ
		bd.MiscFlags = 0;

		//専用の変数を用意
		D3D11_SUBRESOURCE_DATA InitData;

		//サブリソースを初期化するためのデータを指定
		InitData.pSysMem = ppIndex_[i];

		//テクスチャの1行の先頭から次の行までの距離
		InitData.SysMemPitch = 0;

		//ある深度レベルの開始から次の深度レベルまでの距離
		InitData.SysMemSlicePitch = 0;

		//それを使ってインデックスバッファを作成
		if (FAILED(Direct3D::pDevice->CreateBuffer(&bd, &InitData, &ppIndexBuffer_[i])))
		{
			MessageBox(nullptr, "インデックスバッファ作成失敗", "エラー", MB_OK);
			return E_FAIL;
		}
	}

	return S_OK;
}

//コンスタントバッファ準備
HRESULT Fbx::InitConstantBuffer()
{
	///////////////////////////////////////コンスタントバッファの設定///////////////////////////////////////

	//専用の変数を用意
	D3D11_BUFFER_DESC cb;

	//バッファのサイズ
	cb.ByteWidth = sizeof(CONSTANT_BUFFER);

	//バッファで想定されている読み込みおよび書き込みの方法を識別
	cb.Usage = D3D11_USAGE_DYNAMIC;

	//バッファをどのようにパイプラインにバインドするかを識別
	cb.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

	//CPUアクセスのフラグ
	cb.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	//その他のフラグ
	cb.MiscFlags = 0;

	//バッファが構造化バッファを表す場合の、バッファ構造内の各要素のサイズ
	cb.StructureByteStride = 0;

	//それを使ってコンスタントバッファを作成
	if (FAILED(Direct3D::pDevice->CreateBuffer(&cb, nullptr, &pConstantBuffer_)))
	{
		MessageBox(nullptr, "コンスタントバッファ作成失敗", "エラー", MB_OK);
		return E_FAIL;
	}

	return S_OK;
}

//マテリアル情報準備
HRESULT Fbx::InitMaterial(fbxsdk::FbxNode * pNode)
{
	//マテリアル情報を入れる配列(マテリアル情報から抜き出したマテリアル数分の配列)
	pMaterialList_ = new MATERIAL[materialCount_];

	//マテリアル数分繰り返す
	for (DWORD i = 0; i < (DWORD)materialCount_; i++)
	{
		//i番目のマテリアル情報を取得
		FbxSurfaceMaterial* pMaterial = pNode->GetMaterial(i);

		//テクスチャ情報を取得
		FbxProperty  lProperty = pMaterial->FindProperty(FbxSurfaceMaterial::sDiffuse);

		//テクスチャの枚数を取得(指定された型のオブジェクトの数を返す)
		int fileTextureCount = lProperty.GetSrcObjectCount<FbxFileTexture>();


		////////テクスチャあり////////
		if (fileTextureCount)
		{
			//指定された型リストの〜番目のオブジェクトを返す
			FbxFileTexture* textureInfo = lProperty.GetSrcObject<FbxFileTexture>(0);

			//相対パス込みのファイル名を返す
			const char* textureFilePath = textureInfo->GetRelativeFileName();


			////////ファイルパスの整形(ファイル名 + 拡張子だけにする)////////

			char name[_MAX_FNAME];			//ファイル名を入れる変数
			char ext[_MAX_EXT];				//拡張子を入れる変数

			//パスを「ドライブ名」「フォルダ名」「ファイル名」「拡張子」に分割して、作成した変数に代入する(必要ない情報はnullptr と　0 を指定)
			_splitpath_s(textureFilePath, nullptr, 0, nullptr, 0, name, _MAX_FNAME, ext, _MAX_EXT);

			//ファイル名と拡張子をつなげて文字列にする
			wsprintf(name, "%s%s", name, ext);


			////////////////////ファイルからテクスチャ作成////////////////////
			pMaterialList_[i].pTexture = new Texture;
			if (FAILED(pMaterialList_[i].pTexture->Load(name)))
			{
				MessageBox(nullptr, "テクスチャ作成失敗", "エラー", MB_OK);
				return E_FAIL;
			}
		}

		////////テクスチャ無し////////
		else
		{
			pMaterialList_[i].pTexture = nullptr;

			//調べたいマテリアルの色を記憶
			FbxSurfaceLambert* pMaterial = (FbxSurfaceLambert*)pNode->GetMaterial(i);

			//調べたものを変数に代入
			FbxDouble3  diffuse = pMaterial->Diffuse;

			//変数に記憶したものを配列に代入する
			pMaterialList_[i].diffuse = XMFLOAT4((float)diffuse[0], (float)diffuse[1], (float)diffuse[2], 1.0f);
		}
	}

	return S_OK;
}

HRESULT Fbx::Draw(Transform & transform, SHADER_TYPE shaderType)
{
	///////使用するシェーダを指定///////
	Direct3D::SetShaderBundle(shaderType);


	////////////////////////////////////////作成した頂点バッファをパイプラインに流す////////////////////////////////////////

	//各要素のデータサイズ?(頂点情報の(構造体の)サイズ)
	UINT stride = sizeof(VERTEX);

	//何かしらの距離?
	UINT offset = 0;

	//データを流している
	Direct3D::pContext->IASetVertexBuffers(0, 1, &pVertexBuffer_, &stride, &offset);


	///////////////////////////////////作成したコンスタントバッファをパイプラインに流す///////////////////////////////////

	//データを流している
	Direct3D::pContext->VSSetConstantBuffers(0, 1, &pConstantBuffer_);	//頂点シェーダー用	
	Direct3D::pContext->PSSetConstantBuffers(0, 1, &pConstantBuffer_);	//ピクセルシェーダー用


	//インデックスバッファ数繰り返す
	for (int i = 0; i < materialCount_; i++)
	{
		///////////////////////////////////作成したインデックスバッファをパイプラインに流す///////////////////////////////////

		//各要素のデータサイズ?
		stride = sizeof(int);

		//何かしらの距離?
		offset = 0;

		//データを流している
		Direct3D::pContext->IASetIndexBuffer(ppIndexBuffer_[i], DXGI_FORMAT_R32_UINT, 0);


		//////////////////////////作成した構造体をインスタンス化して渡したい情報を設定する//////////////////////////

		CONSTANT_BUFFER cb;
		cb.matWVP = XMMatrixTranspose(transform.GetWorldMatrix() * Camera::GetViewMatrix() * Camera::GetProjectionMatrix());	//カメラ情報
		//cb.matW = XMMatrixTranspose(transform.GetWorldMatrix());																//ワールド行列

		//ワールド行列として、回転行列と拡大行列の転置行列をかけたものを渡している
		cb.matW = XMMatrixTranspose(transform.matRotate_ * XMMatrixInverse(nullptr, transform.matScale_));


		//テクスチャが貼ってあったら
		if (pMaterialList_[i].pTexture)
		{
			//テクスチャのフラグを変更
			cb.isTexture = TRUE;

			/////////////////////////////////////サンプラーをシェーダーに///////////////////////////////////

			ID3D11SamplerState* pSampler = pMaterialList_[i].pTexture->GetSampler();
			Direct3D::pContext->PSSetSamplers(0, 1, &pSampler);


			//////////////////////////////シェーダーリソースビューをシェーダーに////////////////////////////

			ID3D11ShaderResourceView* pSRV = pMaterialList_[i].pTexture->GetSRV();
			Direct3D::pContext->PSSetShaderResources(0, 1, &pSRV);
		}

		//テクスチャが貼ってなかったら
		else
		{
			//テクスチャのフラグを変更
			cb.isTexture = FALSE;

			//調べたマテリアルの色を代入
			cb.diffuseColor = pMaterialList_[i].diffuse;
		}


		///////////////////////////////////作成した情報をコンスタントバッファに格納///////////////////////////////////

		//専用の変数を用意(サブリソースデータへのアクセスを提供)
		D3D11_MAPPED_SUBRESOURCE pdata;

		//Map関数で動きをいったん止めて書き込み可能状態にする
		if (FAILED(Direct3D::pContext->Map(pConstantBuffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &pdata)))	// GPUからのデータアクセスを止める
		{
			MessageBox(nullptr, "コンスタントバッファ書き込み失敗", "エラー", MB_OK);
			return E_FAIL;
		}

		//memcpy_sでデータを送る
		memcpy_s(pdata.pData, pdata.RowPitch, (void*)(&cb), sizeof(cb));								// データの値を送る

		//Unmapで動きを再開させる
		Direct3D::pContext->Unmap(pConstantBuffer_, 0);													//再開

		///////////////////////////////////描画///////////////////////////////////

		//インデックス数を指定
		Direct3D::pContext->DrawIndexed(pIndexCountEachMaterial_[i], 0, 0);
	}

	return S_OK;
}

void Fbx::RayCast(RayCastData * rayData)
{
	//マテリアル数分繰り返す
	for (DWORD i = 0; i < (DWORD)materialCount_; i++)
	{
		//i番目のマテリアルのインデックス数分
		for (DWORD j = 0; j < (DWORD)pIndexCountEachMaterial_[i] / 3; j++)
		{
			//3頂点(i番目のマテリアルのj番目のポリゴンの頂点の位置)
			XMVECTOR v0 = pVertices_[ppIndex_[i][j * 3 + 0]].position;
			XMVECTOR v1 = pVertices_[ppIndex_[i][j * 3 + 1]].position;
			XMVECTOR v2 = pVertices_[ppIndex_[i][j * 3 + 2]].position;

			//指定したポリゴンとの当たり判定
			rayData->hit = Math::Intersect(rayData->start, rayData->dir, v0, v1, v2, &rayData->dist);

			//当たっていたら
			if (rayData->hit)
			{
				return;
			}
		}
	}
}

void Fbx::Release()
{
	for (DWORD i = 0; i < (DWORD)materialCount_; i++)
	{
		for (DWORD n = 0; n < (DWORD)polygonCount_ * 3; n++)
		{
			ppIndex_[i][n] = (int)nullptr;
		}
		pIndexCountEachMaterial_[i] = (int)nullptr;
		SAFE_RELEASE(pMaterialList_[i].pTexture);
		SAFE_DELETE(pMaterialList_[i].pTexture);
	}

	for (DWORD i = 0; i < (DWORD)vertexCount_; i++)
	{
		pVertices_[i].uv = XMVectorZero();
		pVertices_[i].position = XMVectorZero();
		pVertices_[i].normal = XMVectorZero();
	}

	SAFE_DELETE_(ppIndex_);
	SAFE_DELETE_(pVertices_);
	SAFE_DELETE_(pIndexCountEachMaterial_);
	SAFE_DELETE_(pMaterialList_);

	SAFE_RELEASE(pConstantBuffer_);				//コンスタントバッファのポインタ変数
	for (DWORD i = 0; i < (DWORD)materialCount_; i++)
	{
		SAFE_RELEASE(ppIndexBuffer_[i]);			//インデックスバッファのポインタ変数
	}
	SAFE_RELEASE(pVertexBuffer_);				//頂点バッファのポインタ変数
}
