#pragma once

#include "RootJob.h"
#include "SceneManager.h"

//コンストラクタ
RootJob::RootJob()
{
}

//デストラクタ
RootJob::~RootJob()
{
}

void RootJob::Initialize()
{
	Instantiate<SceneManager>(this);
}

void RootJob::Update()
{
}

void RootJob::Draw()
{
}

void RootJob::Release()
{
}
