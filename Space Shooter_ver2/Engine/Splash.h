#pragma once

#include <vector>
#include "Sprite.h"

using namespace DirectX;

namespace Splash
{
	//各画像の情報を管理する構造体
	struct SplashData
	{
		Sprite* pSprite;			//Sprite型のポインタ
		Transform transform;		//変形を扱う変数
		std::string fileName;		//ロードするファイル名
		float alpha;				//透明度
		RECT rect;					//切り抜き範囲


		//構想体のコンストラクタ
		SplashData()
		{
			pSprite = nullptr;	//Sprite型のポインタ
			fileName = "";		//ファイル名
			alpha = 1.0f;		//透明度
		}
	};


	int Load(std::string fileName);

	void SetTransform(int handle, Transform& transform);

	void SetAlpha(int handle, float alpha);

	//画像の切り抜き範囲を設定する関数
	//第一引数：画像番号
	//第二引数：切り抜きたい範囲の左端
	//第三引数：切り抜きたい範囲の上端
	//第四引数：切り抜きたい画像の幅
	//第五引数：切り抜きたい画像の高さ
	void SetRect(int handle, int x, int y, int width, int height);

	//画像の切り抜き範囲をリセットする関数
	//第一引数：画像番号
	void ResetRect(int handle);

	void Draw(int handle);

	void Release(int handle);

	void AllRelease();
};