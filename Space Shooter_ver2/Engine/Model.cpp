#pragma once

#include "Model.h"

namespace Model
{
	//構造体の配列
	std::vector<ModelData*> datas;

	int Load(std::string fileName)
	{
		//構造体のインスタンス作成
		ModelData* pModelData = new ModelData;

		//ファイル名を代入
		pModelData->fileName = fileName;

		//配列に追加されているモデル数分繰り返す
		for (int i = 0; i < datas.size(); i++)
		{
			//既にロード済みのモデルだったら
			if (datas[i]->fileName == fileName)
			{
				pModelData->pFbx = datas[i]->pFbx;
				break;
			}
		}

		//まだロードしていないモデルだったら
		if (pModelData->pFbx == nullptr)
		{
			//FBXのインスタンス作成
			pModelData->pFbx = new Fbx;

			//ファイルのロード
			if (FAILED(pModelData->pFbx->Load(pModelData->fileName.c_str())))
			{
				MessageBox(nullptr, "Fbxのロード失敗", "エラー", MB_OK);
			}
		}

		//構造体の情報を配列に格納
		datas.push_back(pModelData);


		//作成した構造体の解放
		pModelData = nullptr;
		SAFE_DELETE(pModelData);

		return (int)datas.size() - 1;
	}

	void SetTransform(int handle, Transform & transform)
	{
		//指定された番号のモデルの位置を設定
		datas[handle]->transform = transform;
	}

	void Draw(int handle, SHADER_TYPE shaderType)
	{
		//指定された番号のモデルの描画
		if (FAILED(datas[handle]->pFbx->Draw(datas[handle]->transform, shaderType)))
		{
			MessageBox(nullptr, "モデルの描画失敗", "エラー", MB_OK);
		}
	}

	void RayCast(int handle, RayCastData * rayData)
	{
		//指定されたモデルとの当たり判定
		datas[handle]->pFbx->RayCast(rayData);
	}

	void Release(int handle)
	{
		/////引数が無効の場合/////
		if (handle < 0 || handle >= datas.size() || datas[handle] == nullptr)
		{
			return;
		}

		/////解放したいモデルが他の場所でも使用されているかどうか/////

		//使用されているかどうかのフラグ
		bool IsUse = false;

		//配列に追加されているモデル数分繰り返す
		for (int i = 0; i < datas.size(); i++)
		{
			//使われていたら
			if (datas[i] != nullptr && i != handle && datas[i]->pFbx == datas[handle]->pFbx)
			{
				//フラグ変更
				IsUse = true;
				break;
			}
		}
		//使っていなかったら解放
		if (IsUse == false)
		{
			SAFE_RELEASE(datas[handle]->pFbx);
			SAFE_DELETE(datas[handle]->pFbx);
		}

		//配列から削除?????????????????
		SAFE_DELETE(datas[handle]);
	}

	void AllRelease()
	{
		//配列に追加されているモデル数分繰り返す
		for (int i = 0; i < datas.size(); i++)
		{
			if (datas[i] != nullptr)
			{
				Release(i);
				datas[i] = nullptr;
			}
		}
		datas.clear();
	}
}