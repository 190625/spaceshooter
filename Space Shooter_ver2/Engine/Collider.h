#pragma once

#include <d3d11.h>
#include "Transform.h"
using namespace DirectX;

//クラスの前方宣言
class GameObject;
class BoxCollider;
class SphereCollider;


//コライダーの種類
enum ColliderType
{
	COLLIDER_BOX,		//箱型
	COLLIDER_CIRCLE		//球体
};


class Collider
{
	//それぞれのクラスのメンバにアクセスできるようにする
	friend class BoxCollider;
	friend class SphereCollider;

protected:
	GameObject*		pGameObject_;	//コライダーを付与したいオブジェクト
	ColliderType	type_;			//付与したいコライダーの種類
	Transform		transform_;		//コライダーの変形用の変数
	int				hDebugModel_;	//デバッグ表示用のモデルのID

public:
	//コンストラクタ
	Collider();

	//デストラクタ
	virtual ~Collider();

	//接触判定(継承先のSphereColliderかBoxColliderでオーバーライド(純粋仮想関数))
	//第一引数：target 判定したい相手
	//戻値：接触してればtrue
	virtual bool IsHit(Collider* target) = 0;

	//箱型同士の衝突判定
	//第一引数：boxA　1つ目の箱型判定
	//第二引数：boxB　2つ目の箱型判定
	//戻値：接触していればtrue
	bool IsHitBoxVsBox(BoxCollider* boxA, BoxCollider* boxB);

	//箱型と球体の衝突判定
	//第一引数：box		箱型判定
	//第二引数：sphere	球体判定
	//戻値：接触していればtrue
	bool IsHitBoxVsCircle(BoxCollider* box, SphereCollider* sphere);

	//球体同士の衝突判定
	//第一引数：circleA　1つ目の球体判定
	//第二引数：circleB　2つ目の球体判定
	//戻値：接触していればtrue
	bool IsHitCircleVsCircle(SphereCollider* circleA, SphereCollider* circleB);

	//テスト表示用の枠を描画
	//第一引数：コライダーをつけたオブジェクトの移動量
	void Draw(XMVECTOR position);

	//引数で指定されたオブジェクトにコライダーを設定
	void SetGameObject(GameObject* gameObject) { pGameObject_ = gameObject; }

	void Release();
};

