#pragma once

#include "Splash.h"

namespace Splash
{
	//構造体の配列
	std::vector<SplashData*> datas;

	int Load(std::string fileName)
	{
		//構造体のインスタンス作成
		SplashData* pSplashData = new SplashData;

		//ファイル名を代入
		pSplashData->fileName = fileName;

		//配列に追加されている画像数分繰り返す
		for (int i = 0; i < datas.size(); i++)
		{
			//既にロード済みの画像だったら
			if (datas[i]->fileName == fileName)
			{
				pSplashData->pSprite = datas[i]->pSprite;
				break;
			}
		}

		//まだロードしていない画像だったら
		if (pSplashData->pSprite == nullptr)
		{
			//Spriteのインスタンス作成
			pSplashData->pSprite = new Sprite;

			//ファイルのロード
			if (FAILED(pSplashData->pSprite->Initialize(pSplashData->fileName.c_str())))
			{
				MessageBox(nullptr, "Spriteのロード失敗", "エラー", MB_OK);
			}
		}

		//構造体の情報を配列に格納
		datas.push_back(pSplashData);


		//作成した構造体の解放
		pSplashData = nullptr;
		SAFE_DELETE(pSplashData);

		return (int)datas.size() - 1;
	}

	void SetTransform(int handle, Transform & transform)
	{
		//指定された番号の画像の位置を設定
		datas[handle]->transform = transform;
	}

	void SetAlpha(int handle, float alpha)
	{
		//指定された番号の画像の透明度を設定
		datas[handle]->alpha = alpha / 255.0f;
	}

	void SetRect(int handle, int x, int y, int width, int height)
	{
		//画像の切り抜き範囲を設定する
		datas[handle]->rect.left = x;			//左端
		datas[handle]->rect.top = y;			//上端
		datas[handle]->rect.right = width;		//右端
		datas[handle]->rect.bottom = height;	//下端
	}

	void ResetRect(int handle)
	{
		XMVECTOR size = datas[handle]->pSprite->GetTextureSize();

		datas[handle]->rect.left = 0;					//左端
		datas[handle]->rect.top = 0;					//上端
		datas[handle]->rect.right = (long)size.vecX;	//右端
		datas[handle]->rect.bottom = (long)size.vecY;	//下端
	}

	void Draw(int handle)
	{
		//指定された番号の画像の描画
		if (FAILED(datas[handle]->pSprite->Draw(datas[handle]->transform, datas[handle]->alpha)))
		{
			MessageBox(nullptr, "モデルの描画失敗", "エラー", MB_OK);
		}
	}

	void Release(int handle)
	{
		/////引数が無効の場合/////
		if (handle < 0 || handle >= datas.size() || datas[handle] == nullptr)
		{
			return;
		}

		/////解放したい画像が他の場所でも使用されているかどうか/////

		//使用されているかどうかのフラグ
		bool IsUse = false;

		//配列に追加されている画像数分繰り返す
		for (int i = 0; i < datas.size(); i++)
		{
			//使われていたら
			if (datas[i] != nullptr && i != handle && datas[i]->pSprite == datas[handle]->pSprite)
			{
				//フラグ変更
				IsUse = true;
				break;
			}
		}
		//使っていなかったら解放
		if (IsUse == false)
		{
			SAFE_RELEASE(datas[handle]->pSprite);		//Sprite型のポインタ変数
			SAFE_DELETE(datas[handle]->pSprite);		//Spriteのインスタンス
		}

		//配列から削除?????????????????
		SAFE_DELETE(datas[handle]);
	}

	void AllRelease()
	{
		//配列に追加されている画像数分繰り返す
		for (int i = 0; i < datas.size(); i++)
		{
			if (datas[i] != nullptr)
			{
				Release(i);
				datas[i] = nullptr;
			}
		}
		datas.clear();
	}

}