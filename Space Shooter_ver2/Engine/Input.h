#pragma once

#include <dInput.h>
#include "Transform.h"
#include "XInput.h"

#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "dInput8.lib")
#pragma comment(lib,"Xinput.lib")


#define SAFE_RELEASE(p) if(p != nullptr){ p->Release(); p = nullptr;}


namespace Input
{
	//第一引数：ウィンドウハンドル
	HRESULT Initialize(HWND hWnd);

	HRESULT Update();

	////////////////////////キーボード関係////////////////////////

	//キーが押されているかどうか
	//第一引数：調べたいキー
	bool IsKey(int keyCode);

	//キーを押したかどうか
	//第一引数：調べたいキー
	bool IsKeyDown(int keyCode);

	//キーを離したかどうか
	//第一引数：調べたいキー
	bool IsKeyUp(int keyCode);


	//////////////////////////マウス関係////////////////////////

	//ボタンが押されているかどうか
	//第一引数：調べたいボタン
	bool IsMouseButton(int buttonCode);

	//ボタンを押したかどうか
	//第一引数：調べたいボタン
	bool IsMouseButtonDown(int buttonCode);

	//ボタンを離したかどうか
	//第一引数：調べたいボタン
	bool IsMouseButtonUp(int buttonCode);

	//マウスの移動量を取得する
	XMVECTOR GetMouseMove();

	//マウスの位置を取得
	XMVECTOR GetMousePosition();
	//マウスの位置を設定
	void SetMousePosition(int x, int y);


	///////////////////////コントローラー/////////////////////

	//ボタンが押されているかどうか
	//第一引数：調べたいボタン
	bool IsPadButton(int buttonCode);

	//ボタンを押したかどうか
	//第一引数：調べたいボタン
	bool IsPadButtonDown(int buttonCode);

	//ボタンを離したかどうか
	//第一引数：調べたいボタン
	bool IsPadButtonUp(int buttonCode);

	void Release();
};