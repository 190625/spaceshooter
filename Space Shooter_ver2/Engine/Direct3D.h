#pragma once

#include <d3d11.h>
#include <assert.h>

#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dcompiler.lib")

//マクロ(解放処理)
#define SAFE_DELETE(p) if(p != nullptr){ delete p; p = nullptr;}				//作成したインスタンスを解放
#define SAFE_DELETE_(p) if(p != nullptr){ delete[] p; p = nullptr;}				//作成したインスタンスを解放(配列用)
#define SAFE_RELEASE(p) if(p != nullptr){ p->Release(); p = nullptr;}			//ポインタ変数を解放

//定数宣言(シェーダー用)
enum SHADER_TYPE 
{ 
	SHADER_3D,
	SHADER_2D,
	SHADER_LINE,
	SHADER_NORMAL,
	SHADER_MAX
};


namespace Direct3D
{
	//外部ファイルからでも参照できるようにする
	extern ID3D11Device*           pDevice;
	extern ID3D11DeviceContext*    pContext;
	extern int scrWidth_;
	extern int scrHeight_;

	//初期化
	//第一引数：ウィンドウ幅
	//第二引数：ウィンドウ高さ
	//第三引数：ウィンドウハンドル
	HRESULT Initialize(int winW, int winH, HWND hWnd);

	//シェーダー準備
	HRESULT InitShader();


	//使用するシェーダーを指定する(3D or 2D)
	void SetShaderBundle(SHADER_TYPE type);


	//2D用のシェーダー
	HRESULT InitShader2D();


	//3D用のシェーダー
	HRESULT InitShader3D();


	//コライダーの枠用のシェーダー
	HRESULT InitShaderLine();


	//描画開始
	void BeginDraw();

	//描画終了
	HRESULT EndDraw();

	//解放
	void Release();
};