#pragma once
#include "Engine/GameObject.h"

const int durabilityNum_ = 10;          //耐久度数(体力ゲージ)

//耐久度(体力)を管理するクラス
class Durability : public GameObject
{

    int hModel_[durabilityNum_];            //モデル番号
    Transform trans_[durabilityNum_];       //各描画位置
    Transform prevTrans_[durabilityNum_];   //移動前の位置
    XMVECTOR move_;                         //キー入力の移動量
    int drawNum_;                           //描画数

public:
    //コンストラクタ
    Durability(GameObject* parent);

    //デストラクタ
    ~Durability();

    //初期化
    void Initialize() override;

    //更新
    void Update() override;

    //描画
    void Draw() override;

    //開放
    void Release() override;

    //移動
    //引数：移動量
    void Move(XMVECTOR move);

    //位置セット
    void SetPos();

    //位置変更関数
    //引数：選択カメラ(1:TPSカメラ 2:FPSカメラ)
    void ChangePos(int selectCam);

    //描画数変更関数
    void ChangeDraw();

    //描画数を取得
    int GetDrawNum() { return drawNum_; };
};