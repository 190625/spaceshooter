#pragma once

#include "Engine/Splash.h"
#include "Engine/Input.h"
#include "Engine/SceneManager.h"
#include "TitleScene.h"
#include "Engine/SceneTimer.h"

//コンストラクタ
TitleScene::TitleScene(GameObject * parent)
	: GameObject(parent, "TitleScene"), changeFlag_(true), changeTime_(800)
{
	for (int i = 0; i < splashNum; i++)
	{
		hSplash_[i] = -1;
	}
}

//初期化
void TitleScene::Initialize()
{
	for (int i = 0; i < splashNum; i++)
	{
		hSplash_[i] = Splash::Load("Assets/Title_" + std::to_string(i + 1) + ".png");
		assert(hSplash_[i] >= 0);
	}
}

//更新
void TitleScene::Update()
{
	//------------------------------ 画像変更 --------------------------------
	static DWORD startTime_ = timeGetTime();
	DWORD nowTime_ = timeGetTime();

	if (nowTime_ - startTime_ >= changeTime_)
	{
		if (changeFlag_)
			changeFlag_ = false;
		else
			changeFlag_ = true;

		startTime_ = nowTime_;
	}

	SceneTimer::UpdateFrameDelta();

	//------------------------------ 画面遷移 --------------------------------
	if (Input::IsMouseButtonDown(0))
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_PLAY);
	}

	if (Input::IsMouseButtonDown(1))
	{
		PostQuitMessage(0);
	}
}

//描画
void TitleScene::Draw()
{
	if (changeFlag_)
	{
		Splash::SetTransform(hSplash_[0], transform_);
		Splash::Draw(hSplash_[0]);
	}
	else
	{
		Splash::SetTransform(hSplash_[1], transform_);
		Splash::Draw(hSplash_[1]);
	}
}

//開放
void TitleScene::Release()
{
}