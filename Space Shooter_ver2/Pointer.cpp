#include "Pointer.h"
#include "Player.h"
#include "Engine/Model.h"
#include "Engine/Input.h"

//コンストラクタ
Pointer::Pointer(GameObject* parent)
    :GameObject(parent, "Pointer"), hModel_(-1), move_(XMVectorSet(0, 0, 0, 0)), drawFlag_(false), camType_(1)
{
}

//デストラクタ
Pointer::~Pointer()
{
}

//初期化
void Pointer::Initialize()
{
    //モデルデータのロード
    hModel_ = Model::Load("Assets/Pointer_2.fbx");
    assert(hModel_ >= 0);

    transform_.position_ = XMVectorSet(0, 0, 14, 0);
}

//更新
void Pointer::Update()
{
    pPlayer_ = (Player*)GameObject::FindObject("Player");

    SetPos();
    move_ = XMVectorSet(0, 0, 0, 0);
}

//描画
void Pointer::Draw()
{
    if (drawFlag_)
    {
        Model::SetTransform(hModel_, transform_);
        Model::Draw(hModel_, SHADER_3D);
    }
}

//開放
void Pointer::Release()
{
}

//位置セット
void Pointer::SetPos()
{
    transform_.position_ += move_;
}

//描画フラグ変更関数
void Pointer::SetDrawFlag(int currentCam)
{
    switch (currentCam)
    {
    case 1:
        drawFlag_ = false; 
        XMVECTOR pos = pPlayer_->GetPos();
        transform_.position_ = XMVectorSet(pos.vecX, pos.vecY, 14.f, 0);
        break;
    case 2:
        drawFlag_ = true; break;
    default:
        break;
    }
}

//移動
void Pointer::Move(XMVECTOR move)
{
    move_ += move;
}