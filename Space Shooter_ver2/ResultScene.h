/*
　クラス作成の手順(シーン)

  ヘッダー
　1. #pragma once　、　GameObject.h　を読み込む
  2. GameObjectクラスを継承したクラスを定義
  3. コンストラクタ、初期化、更新、描画、解放　処理を書く(オーバーライド)

  ソース
  1. ヘッダーを読み込む
  2. ヘッダーに書いた処理の内容を書く

  コンストラクタ
  引数にparentを格納できるよう変数を用意しておいて、
  GameObjectのコンストラクタ(parentの格納場所、クラス名)を呼ぶことによって、クラスの情報を登録する
*/

#pragma once
#include "Engine/GameObject.h"

class SceneManager;
const int backGroundNum_ = 2;

class ResultScene : public GameObject {

	SceneManager* pScene_;
	int hModel_[backGroundNum_];
	Transform trans_[backGroundNum_];

public:
	//コンストラクタ
	ResultScene(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//解放
	void Release() override;
};