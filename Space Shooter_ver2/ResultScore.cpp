#include "ResultScore.h"
#include "Engine/Model.h"
#include "Engine/SceneManager.h"

//コンストラクタ
ResultScore::ResultScore(GameObject* parent)
	:GameObject(parent, "ResultScore"),pScene_(nullptr)
{
	for (int i = 0; i < scoreNum_; i++)
	{
		hModel_[i] = -1;
	}
}

//初期化
void ResultScore::Initialize()
{
	pScene_ = (SceneManager*)GameObject::FindObject("SceneManager");

	Result first;
	Result second;
	Result third;

	first.score_ = pScene_->GetScore(1);
	second.score_ = pScene_->GetScore(2);
	third.score_ = pScene_->GetScore(3);

	////スコアを文字列に変換して、分割させる
	if (first.score_ != 0)
	{
		first.firstPlace_ = first.score_ % 10;
		first.tenthPlace_ = first.score_ / 10;
		hModel_[0] = Model::Load("Assets/Number_" + std::to_string(first.tenthPlace_) + ".fbx");
		assert(hModel_[0] >= 0);
		hModel_[1] = Model::Load("Assets/Number_" + std::to_string(first.firstPlace_) + ".fbx");
		assert(hModel_[1] >= 0);
	}
	else
	{
		hModel_[0] = Model::Load("Assets/Number_0.fbx");
		assert(hModel_[0] >= 0);
		hModel_[1] = Model::Load("Assets/Number_0.fbx");
		assert(hModel_[1] >= 0);
	}

	if (second.score_ != 0)
	{
		second.firstPlace_ = second.score_ % 10;
		second.tenthPlace_ = second.score_ / 10;
		hModel_[2] = Model::Load("Assets/Number_" + std::to_string(second.tenthPlace_) + ".fbx");
		assert(hModel_[2] >= 0);
		hModel_[3] = Model::Load("Assets/Number_" + std::to_string(second.firstPlace_) + ".fbx");
		assert(hModel_[3] >= 0);
	}
	else
	{
		hModel_[2] = Model::Load("Assets/Number_0.fbx");
		assert(hModel_[2] >= 0);
		hModel_[3] = Model::Load("Assets/Number_0.fbx");
		assert(hModel_[3] >= 0);
	}

	if (third.score_ != 0)
	{
		third.firstPlace_ = third.score_ % 10;
		third.tenthPlace_ = third.score_ / 10;
		hModel_[4] = Model::Load("Assets/Number_" + std::to_string(third.tenthPlace_) + ".fbx");
		assert(hModel_[4] >= 0);
		hModel_[5] = Model::Load("Assets/Number_" + std::to_string(third.firstPlace_) + ".fbx");
		assert(hModel_[5] >= 0);
	}
	else
	{
		hModel_[4] = Model::Load("Assets/Number_0.fbx");
		assert(hModel_[4] >= 0);
		hModel_[5] = Model::Load("Assets/Number_0.fbx");
		assert(hModel_[5] >= 0);
	}

	trans_[0].position_ = XMVectorSet(1.2f, 1.1f, 4.f, 0);
	trans_[1].position_ = XMVectorSet(2.f, 1.1f, 4.f, 0);

	trans_[2].position_ = XMVectorSet(1.2f, -0.2f, 4.f, 0);
	trans_[3].position_ = XMVectorSet(2.f, -0.2f, 4.f, 0);

	trans_[4].position_ = XMVectorSet(1.2f, -1.5f, 4.f, 0);
	trans_[5].position_ = XMVectorSet(2.f, -1.5f, 4.f, 0);

	for (int i = 0; i < scoreNum_; i++)
	{
		trans_[i].rotate_.vecY = XMConvertToRadians(180.f);
		trans_[i].scale_ *= 1.5f;
		trans_[i].Calclation();
	}
}

//更新
void ResultScore::Update()
{
}

//描画
void ResultScore::Draw()
{
	for (int i = 0; i < scoreNum_; i++)
	{
		Model::SetTransform(hModel_[i], trans_[i]);
		Model::Draw(hModel_[i], SHADER_3D);
	}
}

//解放
void ResultScore::Release()
{

}