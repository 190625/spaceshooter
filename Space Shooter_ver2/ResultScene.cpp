#include "ResultScene.h"
#include "ResultScore.h"
#include "Engine/SceneManager.h"
#include "ResultSceneCamera.h"
#include "ResultText.h"
#include "Engine/Input.h"
#include "Engine//Model.h"

//コンストラクタ
ResultScene::ResultScene(GameObject* parent)
	:GameObject(parent, "ResultScene"),pScene_(nullptr)
{
	for (int i = 0; i < backGroundNum_; i++)
	{
		hModel_[i] = -1;
	}
}

//初期化
void ResultScene::Initialize()
{
	Instantiate<ResultSceneCamera>(this);
	Instantiate<ResultText>(this);
	Instantiate<ResultScore>(this);

	pScene_ = (SceneManager*)GameObject::FindObject("SceneManager");

	for (int i = 0; i < backGroundNum_; i++)
	{
		hModel_[i] = Model::Load("Assets/BackGround.fbx");
		assert(hModel_[i] >= 0);
	}

	trans_[0].position_ = XMVectorSet(0, 0, 10.f, 0);
	trans_[1].position_ = XMVectorSet(28.f, 0, 10.f, 0);


	for (int i = 0; i < backGroundNum_; i++)
	{
		trans_[i].rotate_.vecX = XMConvertToRadians(90.f);
		trans_[i].scale_.vecX *= 1.4f;
	}
}

//更新
void ResultScene::Update()
{
	if (Input::IsMouseButtonDown(0))
	{
		pScene_->ChangeScene(SCENE_ID_TITLE);
	}

	if (Input::IsMouseButtonDown(1))
	{
		PostQuitMessage(0);
	}

	if (trans_[0].position_.vecX <= -28.f)
	{
		trans_[0].position_ = XMVectorSet(0, 0, 10.f, 0);
		trans_[1].position_ = XMVectorSet(28.f, 0, 10.f, 0);
	}

	for (int i = 0; i < backGroundNum_; i++)
	{
		trans_[i].position_.vecX -= 0.1f;
		trans_[i].Calclation();
	}
}

//描画
void ResultScene::Draw()
{
	for (int i = 0; i < backGroundNum_; i++)
	{
		Model::SetTransform(hModel_[i], trans_[i]);
		Model::Draw(hModel_[i], SHADER_3D);
	}
}

//解放
void ResultScene::Release()
{

}