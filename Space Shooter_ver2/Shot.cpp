#include "Shot.h"
#include "Engine/Model.h"
#include "Engine/BoxCollider.h"
#include "Score.h"
#include "Pointer.h"

//コンストラクタ
Shot::Shot(GameObject* parent)
    :GameObject(parent, "Shot"), hModel_(-1), pCollision_(nullptr), pScore_(nullptr),pPointer_(nullptr)
{
}

//デストラクタ
Shot::~Shot()
{
}

//初期化
void Shot::Initialize()
{
    //モデルデータのロード
    hModel_ = Model::Load("Assets/Shot.fbx");
    assert(hModel_ >= 0);

    transform_.scale_ *= 1.4f;

    pPointer_ = (Pointer*)GameObject::FindObject("Pointer");
    transform_.position_ = pPointer_->GetPos();

    transform_.position_.vecZ = 12.0f;

    pCollision_ = new BoxCollider(XMVectorSet(0, 0, 0, 0), XMVectorSet(1, 1, 1, 0), XMVectorSet(0, 0, 0, 0));
    AddCollider(pCollision_);
}

//更新
void Shot::Update()
{
    pScore_ = (Score*)GameObject::FindObject("Score");

    transform_.position_.vecZ += 0.5f;
    transform_.rotate_.vecZ += 0.5f;

    //やる事
    //プレイヤーを動かしたときに、左右に機体が傾くようにする
    //敵の攻撃も回転させる

    if (transform_.position_.vecZ > 50.0f)
        KillMe();
}

//描画
void Shot::Draw()
{
    Model::SetTransform(hModel_, transform_);
    Model::Draw(hModel_,SHADER_3D);
    //pCollision_->Draw(transform_.position_);
}

//開放
void Shot::Release()
{
}

//何かに当たった
void Shot::OnCollision(GameObject* pTarget)
{
    if (pTarget->GetObjectName() == "Enemy")
    {
        KillMe();
        pScore_->PlusScore();
    }
}
