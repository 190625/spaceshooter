#pragma once
#include "Engine/GameObject.h"

class Player;


//ポインター(一人称視点のみ)を管理するクラス
class Pointer : public GameObject
{

    int hModel_;            //モデル番号
    XMVECTOR move_;         //キー入力の移動量
    bool drawFlag_;         //描画フラグ
    Player* pPlayer_;       //インスタンス参照(プレイヤー)
    int camType_;           //選択中のカメラ

public:
    //コンストラクタ
    Pointer(GameObject* parent);

    //デストラクタ
    ~Pointer();

    //初期化
    void Initialize() override;

    //更新
    void Update() override;

    //描画
    void Draw() override;

    //開放
    void Release() override;

    //位置セット
    void SetPos();

    //描画フラグ変更関数
    //引数：選択されているカメラ
    void SetDrawFlag(int currentCam);

    //移動
    //引数：移動量
    void Move(XMVECTOR move);

    //ポインターの位置を渡す
    XMVECTOR GetPos() { return transform_.position_; };
};