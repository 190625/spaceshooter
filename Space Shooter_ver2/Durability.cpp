#include "Durability.h"
#include "Engine/Model.h"

//コンストラクタ
Durability::Durability(GameObject* parent)
    :GameObject(parent, "Durability"), move_(XMVectorSet(0, 0, 0, 0)),drawNum_(10)
{
    for (int i = 0; i < durabilityNum_; i++)
    {
        hModel_[i] = -1;
    }
}

//デストラクタ
Durability::~Durability()
{
}

//初期化
void Durability::Initialize()
{
    for (int i = 0; i < drawNum_; i++)
    {
        //モデルデータのロード
        hModel_[i] = Model::Load("Assets/Durability.fbx");
        assert(hModel_[i] >= 0);

        trans_[i].position_ = XMVectorSet(-2.5f + (i * 0.23f), 1.7f, 2, 0);
        trans_[i].Calclation();
    }
}

//更新
void Durability::Update()
{
    SetPos();

    move_ = XMVectorSet(0, 0, 0, 0);
}

//描画
void Durability::Draw()
{
    for (int i = 0; i < drawNum_; i++)
    {
        Model::SetTransform(hModel_[i], trans_[i]);
        Model::Draw(hModel_[i],SHADER_3D);
    }
}

//開放
void Durability::Release()
{
}

//移動
void Durability::Move(XMVECTOR move)
{
    move_ += move;
}

//位置セット
void Durability::SetPos()
{
    for (int i = 0; i < drawNum_; i++)
    {
        trans_[i].position_ += move_;
        trans_[i].Calclation();
    }
}

//位置変更関数
void Durability::ChangePos(int selectCam)
{
    switch (selectCam)
    {
    case 1:     //TPSカメラ
        for (int i = 0; i < drawNum_; i++)
        {
            trans_[i].position_ = prevTrans_[i].position_;
            trans_[i].position_.vecZ = 2;
            trans_[i].Calclation();
        }
        break;
    case 2:     //FPSカメラ
        for (int i = 0; i < drawNum_; i++)
        {
            prevTrans_[i].position_ = trans_[i].position_;
            trans_[i].position_.vecZ = 12;
            trans_[i].Calclation();
        }
        break;
    default:
        break;
    }
}

//描画数変更関数
void Durability::ChangeDraw()
{
    drawNum_--;
}
