#pragma once
#include "Engine/GameObject.h"


//タイトルシーンを管理するクラス
class TitleScene : public GameObject
{
	int hSplash_[splashNum];	//画像番号
	bool changeFlag_;			//画像変更フラグ(切り替え)
	int changeTime_;			//変更タイム

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	TitleScene(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};