#include "ResultSceneCamera.h"

ResultSceneCamera::ResultSceneCamera(GameObject* parent)
	:GameObject(parent, "ResultSceneCamera"), camPos_(XMVectorSet(0, 0, 0, 0)), camTar_(XMVectorSet(0, 0, 10, 0))
{

}

ResultSceneCamera::~ResultSceneCamera()
{

}

void ResultSceneCamera::Initialize()
{
	Camera::SetPosition(camPos_);
	Camera::SetTarget(camTar_);
}

void ResultSceneCamera::Update()
{

}

void ResultSceneCamera::Draw()
{

}

void ResultSceneCamera::Release()
{

}