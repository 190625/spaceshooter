#include "Enemy.h"
#include "Engine/Model.h"
#include "Engine/SphereCollider.h"
#include "Engine/SceneTimer.h"

//コンストラクタ
Enemy::Enemy(GameObject* parent)
    :GameObject(parent, "Enemy"), hModel_(-1), randam_(0), dis_(XMVectorSet(0, 0, 0, 0)), move_(0.2f), moveFlag_(false),finishFlag_(0), 
    pCollision_(nullptr),clashFlag_(false),flashTime_(100)
{
}

//デストラクタ
Enemy::~Enemy()
{
}

//初期化
void Enemy::Initialize()
{
    //モデルデータのロード
    hModel_ = Model::Load("Assets/Enemy.fbx");
    assert(hModel_ >= 0);

    //初期位置設定
    SetPos(INIT_POS);
    transform_.rotate_.vecY = XMConvertToRadians(180.0f);
    transform_.scale_ *= 2.0f;

    pCollision_ = new SphereCollider(XMVectorSet(0, 0, 0, 0), 1.8f);
    AddCollider(pCollision_);
}

//更新
void Enemy::Update()
{
    if (!moveFlag_)
    {
        //----- 移動地点設定 -----
        SetPos(MOVE_POS);

        if(dis_.vecX != 0 || dis_.vecY != 0)
            moveFlag_ = true;
    }
    else
    {
        //----- 移動 -----
        finishFlag_ += MoveX();
        finishFlag_ += MoveY();

        if (finishFlag_ == 2)
            moveFlag_ = false;

        finishFlag_ = 0;
    }

    static DWORD prevTime = timeGetTime();
    DWORD nowTime = timeGetTime();

    if (nowTime - prevTime >= flashTime_)
    {
        clashFlag_ = false;
        prevTime = nowTime;
    }

    SceneTimer::UpdateFrameDelta();
}

//描画
void Enemy::Draw()
{
    if (!clashFlag_)
    {
        Model::SetTransform(hModel_, transform_);
        Model::Draw(hModel_, SHADER_3D);
        //pCollision_->Draw(transform_.position_);
    }
}

//開放
void Enemy::Release()
{
}

//位置設定関数
void Enemy::SetPos(int type)
{
    srand((unsigned int)time(NULL));
    randam_ = (rand() % 9) + 1;

    //初期位置設定
    if (type == INIT_POS)
    {
        switch (randam_)
        {
        case 1:
            transform_.position_ = pattern_One; break;
        case 2:
            transform_.position_ = pattern_Two; break;
        case 3:
            transform_.position_ = pattern_Three; break;
        case 4:
            transform_.position_ = pattern_Four; break;
        case 5:
            transform_.position_ = pattern_Five; break;
        case 6:
            transform_.position_ = pattern_Six; break;
        case 7:
            transform_.position_ = pattern_Seven; break;
        case 8:
            transform_.position_ = pattern_Eight; break;
        case 9:
            transform_.position_ = pattern_Nine; break;
        default:
            break;
        }

        transform_.position_.vecZ = 38.0f;
    }
    else//移動位置設定
    {
        switch (randam_)
        {
        case 1:
            dis_ = pattern_One - transform_.position_; break;
        case 2:
            dis_ = pattern_Two - transform_.position_; break;
        case 3:
            dis_ = pattern_Three - transform_.position_; break;
        case 4:
            dis_ = pattern_Four - transform_.position_; break;
        case 5:
            dis_ = pattern_Five - transform_.position_; break;
        case 6:
            dis_ = pattern_Six - transform_.position_; break;
        case 7:
            dis_ = pattern_Seven - transform_.position_; break;
        case 8:
            dis_ = pattern_Eight - transform_.position_; break;
        case 9:
            dis_ = pattern_Nine - transform_.position_; break;
        default:
            break;
        }
    }
}

//X軸の移動関数
int Enemy::MoveX()
{
    if (dis_.vecX < -0.1f || 0.1f < dis_.vecX)
    {
        if (dis_.vecX > 0)//+移動
        {
            transform_.position_.vecX += move_;
            dis_.vecX -= move_;
        }
        else//-移動
        {
            transform_.position_.vecX -= move_;
            dis_.vecX += move_;
        }

        return 0;
    }
    else
        return 1;
}

//Y軸の移動関数
int Enemy::MoveY()
{
    if (dis_.vecY < -0.1f || 0.1f < dis_.vecY)
    {
        if (dis_.vecY > 0)//+移動
        {
            transform_.position_.vecY += move_;
            dis_.vecY -= move_;
        }
        else//-移動
        {
            transform_.position_.vecY -= move_;
            dis_.vecY += move_;
        }

        return 0;
    }
    else
        return 1;
}

//何かに当たった
void Enemy::OnCollision(GameObject* pTarget)
{
    if (pTarget->GetObjectName() == "Shot")
        clashFlag_ = true;
}
