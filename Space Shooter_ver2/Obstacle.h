#pragma once
#include "Engine/GameObject.h"
#include "Engine/SphereCollider.h"


//障害物を管理するクラス
class Obstacle : public GameObject
{
    int hModel_;                   //モデル番号
    int randam_;                   //乱数
    SphereCollider* pCollision_;   //インスタンス(衝突判定)

public:
    //コンストラクタ
    Obstacle(GameObject* parent);

    //デストラクタ
    ~Obstacle();

    //初期化
    void Initialize() override;

    //更新
    void Update() override;

    //描画
    void Draw() override;

    //開放
    void Release() override;

    //何かに当たった
    //引数：当たった相手
    void OnCollision(GameObject* pTarget) override;
};