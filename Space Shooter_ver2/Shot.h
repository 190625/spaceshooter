#pragma once
#include "Engine/GameObject.h"

class BoxCollider;
class Score;
class Pointer;

//ショットを管理するクラス
class Shot : public GameObject
{

    int hModel_;                //モデル番号
    BoxCollider* pCollision_;   //インスタンス(衝突判定)
    Score* pScore_;             //インスタンス参照(スコア)
    Pointer* pPointer_;         //インスタンス(ポインター)

public:
    //コンストラクタ
    Shot(GameObject* parent);

    //デストラクタ
    ~Shot();

    //初期化
    void Initialize() override;

    //更新
    void Update() override;

    //描画
    void Draw() override;

    //開放
    void Release() override;

    //何かに当たった
    //引数：当たった相手
    void OnCollision(GameObject* pTarget) override;
};