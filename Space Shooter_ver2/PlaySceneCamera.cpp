#include "PlaySceneCamera.h"
#include "Engine/Camera.h"

//コンストラクタ
PlaySceneCamera::PlaySceneCamera(GameObject* parent)
    :GameObject(parent, "PlaySceneCamera"), camPos_(XMVectorSet(0, 0, 0, 0)), camTar_(XMVectorSet(0, 0, 10, 0)), prevPos_(XMVectorSet(0, 0, 0, 0)), prevTar_(XMVectorSet(0, 0, 0, 0))
{
}

//デストラクタ
PlaySceneCamera::~PlaySceneCamera()
{
}

//初期化
void PlaySceneCamera::Initialize()
{
    Camera::SetPosition(camPos_);
    Camera::SetTarget(camTar_);
}

//更新
void PlaySceneCamera::Update()
{
    SetCam();
}

//描画
void PlaySceneCamera::Draw()
{
}

//開放
void PlaySceneCamera::Release()
{
}

//移動(位置/焦点)
//引数：移動量
void PlaySceneCamera::Move(XMVECTOR move)
{
    camPos_ += move;
    camTar_ += move;
}

//位置セット
void PlaySceneCamera::SetCam()
{
    Camera::SetPosition(camPos_);
    Camera::SetTarget(camTar_);
}

//カメラ変更関数
void PlaySceneCamera::ChangeCamera(int selectCam)
{
    switch (selectCam)
    {
    case 1:     //TPSカメラ
        camPos_ = prevPos_;
        camTar_ = prevTar_;
        camTar_.vecZ = 10.0f;
        break;
    case 2:     //FPSカメラ
        prevPos_ = camPos_;
        prevTar_ = camTar_;
        camPos_.vecZ = 10.0f;
        camTar_.vecZ = 20.0f;
        break;
    default:
        break;
    }
}
