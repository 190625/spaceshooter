#pragma once
#include "Engine/GameObject.h"

const int scoreDigit_ = 2;         //スコアの桁数

//スコアを管理するクラス
class Score : public GameObject
{

    int hModel_[scoreDigit_];           //モデル番号
    Transform trans_[scoreDigit_];      //モデルの位置
    int firstPlace_;                    //一の位を管理
    int tenthPlace_;                    //十の位を管理
    XMVECTOR move_;                     //キー入力の移動量
    int score_;                         //スコア
    Transform prevTrans_[scoreDigit_];  //移動前の位置

public:
    //コンストラクタ
    Score(GameObject* parent);

    //デストラクタ
    ~Score();

    //初期化
    void Initialize() override;

    //更新
    void Update() override;

    //描画
    void Draw() override;

    //開放
    void Release() override;

    //スコア加算関数
    void PlusScore();

    //移動
    //引数：移動量
    void Move(XMVECTOR move);

    //位置セット
    void SetPos();

    //位置変更関数
   //引数：選択カメラ(1:TPSカメラ 2:FPSカメラ)
    void ChangePos(int selectCam);

    //スコア取得関数
    int GetScore();
};