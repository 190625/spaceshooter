/*
　クラス作成の手順(オブジェクト)

  ヘッダー
　1. #pragma once　、　GameObject.h　を読み込む
  2. GameObjectクラスを継承したクラスを定義
  3. コンストラクタ、初期化、更新、描画、解放　処理を書く(オーバーライド)

  ソース
  1. ヘッダーを読み込む
  2. ヘッダーに書いた処理の内容を書く

  コンストラクタ
  引数にparentを格納できるよう変数を用意しておいて、
  GameObjectのコンストラクタ(parentの格納場所、クラス名)を呼ぶことによって、クラスの情報を登録する
*/

/*
　モデル表示の手順

 1.　モデル番号を格納する変数を作成　-1で初期化
 2.　Model.hを読み込み、ロードする　assertで確認
 3.　描画させる
	
*/


#pragma once
#include "Engine/GameObject.h"

class SceneManager;

const int scoreNum_ = 6;

class ResultScore :public GameObject
{
	int hModel_[scoreNum_];
	Transform trans_[scoreNum_];
	SceneManager* pScene_;

	struct Result {
		int score_;
		int firstPlace_;
		int tenthPlace_;
	};

public:
	//コンストラクタ
	ResultScore(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//解放
	void Release() override;
};

