#pragma once
#include "Engine/GameObject.h"

class ShpereCollider;

#define INIT_POS 1
#define MOVE_POS 2

//敵(宇宙人)を管理するクラス
class Enemy : public GameObject
{

    int hModel_;                 //モデル番号
    int randam_;                 //乱数
    XMVECTOR dis_;               //目標地点までの距離
    float move_;                 //移動量(1フレームの)
    bool moveFlag_;              //移動フラグ
    int finishFlag_;             //終了フラグ(移動)
    SphereCollider* pCollision_; //インスタンス(衝突判定)
    bool clashFlag_;             //衝突フラグ(障害物との)
    int flashTime_;              //点滅間隔※ミリ秒

public:
    //コンストラクタ
    Enemy(GameObject* parent);

    //デストラクタ
    ~Enemy();

    //初期化
    void Initialize() override;

    //更新
    void Update() override;

    //描画
    void Draw() override;

    //開放
    void Release() override;

    //位置設定関数
    //引数：セットタイプ(1:初期位置 2:移動位置)
    void SetPos(int type);

    //X軸の移動関数
    //戻り値：終了したか
    int MoveX();

    //Y軸の移動関数
    //戻り値：終了したか
    int MoveY();

    //何かに当たった
    //引数：当たった相手
    void OnCollision(GameObject* pTarget) override;
};