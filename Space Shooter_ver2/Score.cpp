#include "Score.h"
#include "Engine/Model.h"

//コンストラクタ
Score::Score(GameObject* parent)
    :GameObject(parent, "Score"), firstPlace_(0), tenthPlace_(0), move_(move_ = XMVectorSet(0, 0, 0, 0)),score_(0)
{
    for (int i = 0; i < scoreDigit_; i++)
    {
        hModel_[i] = -1;
    }
}

//デストラクタ
Score::~Score()
{
}

//初期化
void Score::Initialize()
{
    //--------------- 位置設定 ---------------
    trans_[0].position_ = XMVectorSet(2.4f, 1.35f, 2, 0);
    trans_[1].position_ = XMVectorSet(1.9f, 1.35f, 2, 0);

    for (int i = 0; i < scoreDigit_; i++)
    {
        //モデルデータのロード
        hModel_[i] = Model::Load("Assets/Number_0.fbx");
        assert(hModel_[i] >= 0);
        trans_[i].rotate_.vecY = XMConvertToRadians(180.0f);
        trans_[i].Calclation();
    }
}

//更新
void Score::Update()
{
    //----------- 一の位のモデルデータのロード -----------
    hModel_[0] = Model::Load("Assets/Number_" + std::to_string(firstPlace_) + ".fbx");
    assert(hModel_[0] >= 0);

    //----------- 十の位のモデルデータのロード -----------
    hModel_[1] = Model::Load("Assets/Number_" + std::to_string(tenthPlace_) + ".fbx");
    assert(hModel_[1] >= 0);

    SetPos();

    move_ = XMVectorSet(0, 0, 0, 0);
}

//描画
void Score::Draw()
{
    for (int i = 0; i < scoreDigit_; i++)
    {
        Model::SetTransform(hModel_[i], trans_[i]);
        Model::Draw(hModel_[i],SHADER_3D);
    }
}

//開放
void Score::Release()
{
}

//スコア加算関数
void Score::PlusScore()
{
    if (firstPlace_ + 1 < 10)
    {
        firstPlace_++;
    }
    else
    {
        tenthPlace_++;
        firstPlace_ = 0;
    }

    score_++;
}

//移動
void Score::Move(XMVECTOR move)
{
    move_ += move;
}

//位置セット
void Score::SetPos()
{
    for (int i = 0; i < scoreDigit_; i++)
    {
        trans_[i].position_ += move_;
        trans_[i].Calclation();
    }
}

//位置変更関数
void Score::ChangePos(int selectCam)
{
    switch (selectCam)
    {
    case 1:     //TPSカメラ
        for (int i = 0; i < scoreDigit_; i++)
        {
            trans_[i].position_ = prevTrans_[i].position_;
            trans_[i].position_.vecZ = 2;
            trans_[i].Calclation();
        }
        break;
    case 2:     //FPSカメラ
        for (int i = 0; i < scoreDigit_; i++)
        {
            prevTrans_[i].position_ = trans_[i].position_;
            trans_[i].position_.vecZ = 12;
            trans_[i].Calclation();
        }
        break;
    default:
        break;
    }
}

//スコア取得関数
int Score::GetScore()
{
    return score_;
}