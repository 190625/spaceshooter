#pragma once
#include "Engine/GameObject.h"

class Pointer;
class Durability;
class PlaySceneCamera;
class Score;
class SceneManager;
class Player;

//プレイシーンを管理するクラス
class PlayScene : public GameObject
{
	int currentCam_;			//選択中のカメラ
	int generateTime_;			//(障害物)生成時間※ミリ秒
	int finishTime_;			//ゲーム終了時間※ミリ秒
	Pointer* pPointer_;			//インスタンス参照(ポインター)
	Durability* pDurability_;	//インスタンス参照(耐久度)
	PlaySceneCamera* pCam_;		//インスタンス参照(カメラ)
	Score* pScore_;				//インスタンス参照(スコア)
	SceneManager* pScene_;		//インスタンス参照(シーンマネージャー)
	Player* pPlayer_;			//インスタンス参照(プレイヤー)
	DWORD nowTime_;				//時間(prevTime_からの経過時間)
	DWORD prevTime_;			//時間(設定した任意の時間)
	DWORD startTime_;			//時間(シーン開始からの経過時間)

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	PlayScene(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//解放
	void Release() override;

	//カメラを切り替えたときに行う処理をまとめた関数
	void RelateChangeCam();

	//移動処理をまとめた関数
	void RelateMove();
};
