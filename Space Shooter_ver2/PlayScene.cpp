#pragma once

#include "Engine/Splash.h"
#include "Engine/Model.h"
#include "Engine/Input.h"
#include "Engine/SceneManager.h"
#include "Engine/SceneTimer.h"
#include "Stage.h"
#include "PlayScene.h"

#include "Player.h"
#include "PlaySceneCamera.h"
#include "Pointer.h"
#include "Obstacle.h"
#include "Durability.h"
#include "Enemy.h"
#include "Score.h"

//コンストラクタ
PlayScene::PlayScene(GameObject* parent)
	: GameObject(parent, "PlayScene"), currentCam_(TPS_CAMERA), generateTime_(800), finishTime_(60000), pPointer_(nullptr),pPlayer_(nullptr)
	, pDurability_(nullptr), pCam_(nullptr), pScore_(nullptr), pScene_(nullptr), nowTime_(NULL), prevTime_(NULL), startTime_(NULL)
{
}

//初期化
void PlayScene::Initialize()
{
	//プレイシーンカメラの描画
	Instantiate<PlaySceneCamera>(this);

	//ステージの描画
	Instantiate<Stage>(this);

	//プレイヤーの描画
	Instantiate<Player>(this);

	//ポインターの描画
	Instantiate<Pointer>(this);

	//体力ゲージの描画
	Instantiate<Durability>(this);

	//敵の描画
	Instantiate<Enemy>(this);

	//スコアの描画
	Instantiate<Score>(this);

	//時間設定
	prevTime_ = timeGetTime();
	startTime_ = timeGetTime();
}

//更新
void PlayScene::Update()
{
	pPointer_ = (Pointer*)GameObject::FindObject("Pointer");
	pDurability_ = (Durability*)GameObject::FindObject("Durability");
	pCam_ = (PlaySceneCamera*)GameObject::FindObject("PlaySceneCamera");
	pScore_ = (Score*)GameObject::FindObject("Score");
	pScene_ = (SceneManager*)GameObject::FindObject("SceneManager");
	pPlayer_ = (Player*)GameObject::FindObject("Player");

	//----------------------------- カメラ変更 -----------------------------
	if (Input::IsMouseButtonDown(1))
		RelateChangeCam();

	if (Input::IsMouseButtonUp(1))
		RelateChangeCam();

	RelateMove();

	//------------------------------ 障害物生成 --------------------------------
	nowTime_ = timeGetTime();

	if (nowTime_ - prevTime_ >= generateTime_)
	{
		Instantiate<Obstacle>(this);
		prevTime_ = nowTime_;
	}

	SceneTimer::UpdateFrameDelta();

	//------------------------------ 画面変更 --------------------------------
	if (pDurability_->GetDrawNum() == 0 || nowTime_ - startTime_ >= finishTime_)
	{
		pScene_->SetScore(pScore_->GetScore());
		pScene_->UpdateScore();
		pScene_->ChangeScene(SCENE_ID_RESULT);
	}
}

//描画
void PlayScene::Draw()
{
}

//開放
void PlayScene::Release()
{
}

//カメラを切り替えたときに行う処理をまとめた関数
void PlayScene::RelateChangeCam()
{
	//カメラ切り替え
	if (currentCam_ == TPS_CAMERA)
		currentCam_ = FPS_CAMERA;
	else
		currentCam_ = TPS_CAMERA;

	pCam_->ChangeCamera(currentCam_);
	pPointer_->SetDrawFlag(currentCam_);
	pDurability_->ChangePos(currentCam_);
	pScore_->ChangePos(currentCam_);
	pPlayer_->CheckCam(currentCam_);
}

//移動処理をまとめた関数
void PlayScene::RelateMove()
{
	XMVECTOR move;
	move = pPlayer_->GetMove();

	if (currentCam_ == TPS_CAMERA)
	{
		pCam_->Move(move);
		pDurability_->Move(move);
		pScore_->Move(move);
		pPlayer_->Move();
		pPointer_->Move(move);
	}
	else
	{
		pPointer_->Move(move);
	}
}
