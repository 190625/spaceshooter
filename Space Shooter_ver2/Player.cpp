#include "Player.h"
#include "PlaySceneCamera.h"
#include "Engine/Model.h"
#include "Engine/Input.h"
#include "Shot.h"
#include "Pointer.h"
#include "Engine/BoxCollider.h"
#include "Durability.h"
#include "Engine/SceneTimer.h"
#include "Score.h"

//コンストラクタ
Player::Player(GameObject* parent)
    :GameObject(parent, "Player"), hModel_(-1), keyMove_(XMVectorSet(0, 0, 0, 0)), calcMove_(XMVectorSet(0, 0, 0, 0)), pCollision_(nullptr),pPointer_(nullptr)
    , pDurability_(nullptr), clashFlag_(false), flashCnt_(0), flashTime_(100), mouseMove_(XMVectorSet(0, 0, 0, 0)), calcMouseMove_(XMVectorSet(0, 0, 0, 0)),camType_(1)
{
}

//デストラクタ
Player::~Player()
{
}

//初期化
void Player::Initialize()
{
    //モデルデータのロード
    hModel_ = Model::Load("Assets/AirPlane.fbx");
    assert(hModel_ >= 0);

    transform_.position_ = XMVectorSet(0, 0, 5, 0);

    pCollision_ = new BoxCollider(XMVectorSet(0, 0.05f, 0, 0), XMVectorSet(1.8f, 0.4f, 0.3f, 0), XMVectorSet(0, 0, 0, 0));
    AddCollider(pCollision_);

    //--------------(仮)--------------
    transform_.scale_ *= 1.4f;
}

//更新
void Player::Update()
{
    pDurability_ = (Durability*)GameObject::FindObject("Durability");
    pPointer_ = (Pointer*)GameObject::FindObject("Pointer");

    keyMove_ = XMVectorSet(0, 0, 0, 0);
    mouseMove_ = XMVectorSet(0, 0, 0, 0);
    move_ = XMVectorSet(0, 0, 0, 0);

    switch (camType_)
    {
    case 1:
        MoveTPS(); break;
    case 2:
        MoveFPS(); break;
    default:
        break;
    }

    //------------------ ショット ------------------
    if (Input::IsMouseButtonDown(0))
        Instantiate<Shot>(this);

    //------------------ 点滅処理 -----------------
    if (clashFlag_)
    {
        static DWORD prevTime = timeGetTime();
        DWORD nowTime = timeGetTime();
        if (nowTime - prevTime >= flashTime_)
        {
            flashCnt_++;
            prevTime = nowTime;
        }

        if (flashCnt_ == 11)
        {
            clashFlag_ = false;
            flashCnt_ = 0;
        }
    }
    SceneTimer::UpdateFrameDelta();

    FixRotate();
}

//描画
void Player::Draw()
{
    if (flashCnt_ % 2 == 0)
    {
        Model::SetTransform(hModel_, transform_);
        Model::Draw(hModel_, SHADER_3D);
        //pCollision_->Draw(transform_.position_);
    }
}

//開放
void Player::Release()
{
}

//何かに当たった
void Player::OnCollision(GameObject* pTarget)
{
    if (pTarget->GetObjectName() == "Obstacle")
        if (!clashFlag_)
        {
            pDurability_->ChangeDraw();
            clashFlag_ = true;
        }
}

//TPSカメラ時の移動処理
void Player::MoveTPS()
{
    //--------------------- キー入力移動 ---------------------
    if (Input::IsKey(DIK_W))
        keyMove_.vecY = 0.6f;

    if (Input::IsKey(DIK_S))
        keyMove_.vecY = -0.6f;

    if (Input::IsKey(DIK_D))
        keyMove_.vecX = 0.6f;

    if (Input::IsKey(DIK_A))
        keyMove_.vecX = -0.6f;

    calcMove_.vecX = transform_.position_.vecX + keyMove_.vecX;
    calcMove_.vecY = transform_.position_.vecY + keyMove_.vecY;

    //--------------------- 移動制限 ---------------------
    if (-10 <= calcMove_.vecX && calcMove_.vecX <= 10 && keyMove_.vecX != 0)
        move_.vecX = keyMove_.vecX;

    if (-10 <= calcMove_.vecY && calcMove_.vecY <= 10 && keyMove_.vecY != 0)
        move_.vecY = keyMove_.vecY;
}

//FPSカメラ時の移動処理
void Player::MoveFPS()
{
    mouseMove_ = Input::GetMouseMove() * 0.08f;

    calcMouseMove_.vecX = pPointer_->GetPos().vecX + mouseMove_.vecX;
    calcMouseMove_.vecY = pPointer_->GetPos().vecY + mouseMove_.vecY;

    //--------------------- 移動制限 ---------------------
    if (transform_.position_.vecX - 2.4f < calcMouseMove_.vecX && calcMouseMove_.vecX < transform_.position_.vecX + 2.4f && mouseMove_.vecX != 0)
        move_.vecX = mouseMove_.vecX;

    if (transform_.position_.vecY - 2.4f < calcMouseMove_.vecY && calcMouseMove_.vecY < transform_.position_.vecY + 2.4f && mouseMove_.vecY != 0)
        move_.vecY = mouseMove_.vecY;
}

//選択中のカメラを調べる関数
void Player::CheckCam(int camType)
{
    camType_ = camType;
}

//移動
void Player::Move()
{
    transform_.position_ += move_;
    transform_.rotate_.vecZ -= move_.vecX * 0.03f;
    transform_.rotate_.vecX -= move_.vecY * 0.03f;
}

//傾きを直す関数
void Player::FixRotate()
{
    if (transform_.rotate_.vecZ != 0)
    {
        if (transform_.rotate_.vecZ > 0)
            transform_.rotate_.vecZ -= 0.01f;
        else
            transform_.rotate_.vecZ += 0.01f;
    }

    if (transform_.rotate_.vecX != 0)
    {
        if (transform_.rotate_.vecX > 0)
            transform_.rotate_.vecX -= 0.01f;
        else
            transform_.rotate_.vecX += 0.01f;
    }
}
