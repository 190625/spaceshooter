#pragma once

#include "Engine/Model.h"
#include "Engine//SphereCollider.h"
#include "Obstacle.h"
#include "Pointer.h"

//コンストラクタ
Obstacle::Obstacle(GameObject* parent)
    :GameObject(parent, "Obstacle"), hModel_(0)
{
	//乱数生成の準備
	srand((unsigned int)time(NULL));

	//乱数生成
	randam_ = rand() % randRange_ + addRange_;

	//乱数をもとに障害物の情報を生成
	data_.defence_ = randam_;
	data_.size_ = randam_;
	data_.collisionSize_ = data_.size_ - ajustCollision_;
	data_.speed_ = data_.size_ * ajustSpeed_;
	data_.damage_ = data_.size_ * ajustDamage_;
	data_.posX_ = rand() % randPosUpX_ - randPosLowX_;
	data_.posY_ = rand() % randPosUpY_ - randPosLowY_;
}

//デストラクタ
Obstacle::~Obstacle()
{
}

//初期化
void Obstacle::Initialize()
{
    //モデルデータのロード
    hModel_ = Model::Load("Assets/ball.fbx");
    assert(hModel_ >= 0);

	//初期位置設定
	transform_.position_.vecX = data_.posX_;
	transform_.position_.vecY = data_.posY_;
	transform_.position_.vecZ = posZ_;
	transform_.scale_ *= data_.size_;

    //障害物モデルに当たり判定を追加
    collision_ = new SphereCollider(XMVectorSet(0, 0, 0, 0), data_.collisionSize_);
    AddCollider(collision_);

	//Durabilityクラス(体力ゲージを管理する)のポインタを検索して、変数に代入
	pDurability_ = (Durability*)GameObject::FindObject("Durability");
}

//更新
void Obstacle::Update()
{
	//奥に飛んでいくようにする
	move_.vecZ = -data_.speed_;

	transform_.position_ += move_;

	//一定距離飛んでいったとき
	if (transform_.position_.vecZ < ObstacleRnageZ_)
	{
		//障害物を削除する
		KillMe();
		//撃ち落とせなかったとき
		pDurability_->HitObstacle(data_.damage_);
	}

	if (data_.defence_ <= 0)
	{
		//障害物を削除する
		KillMe();
	}
}

//描画
void Obstacle::Draw()
{
    Model::SetTransform(hModel_, transform_);
    Model::Draw(hModel_,SHADER_3D);
}

//開放
void Obstacle::Release()
{
	//ポインタ変数を解放
	pDurability_ = nullptr;
}

void Obstacle::HitAttack(float damage)
{
	//耐久値を減少
	data_.defence_ -= damage;
}
