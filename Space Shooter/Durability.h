#pragma once

#include "Engine/GameObject.h"

const int durabillityNum_ = 10;			//モデル数(体力ゲージ)
const float durabillityX_ = -2.5;		//位置(X)
const float durabillityY_ = 3.7f;		//位置(Y)
const float durabillityZ_ = -28.0f;		//位置(Z)
const float drawJudge_ = 0.5f;			//描画判定値
const float addPosX_ = 0.27f;			//位置調整値(X)

//耐久度を管理するクラス
class Durability : public GameObject
{
	int hModel_[durabillityNum_];		//モデル番号
	Transform trans_[durabillityNum_];	//各モデルの位置
	float remaining_;					//残りの体力ゲージを管理する
	float remainingJudge_;				//どこまで表示するのかを判断する
	float ajustPosX_;					//位置調整(X)

public:
	//コンストラクタ
	Durability(GameObject* parent);

	//デストラクタ
	~Durability();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//障害物を撃ち落とせなかったとき
	void HitObstacle(float damage);

	//体力ゲージが残っているかどうか
	//戻り値：残っている体力ゲージの数
	float GetRemaining() { return remaining_; };
};