#pragma once

#include "ShotNumber9.h"
#include "Engine/Model.h"
#include "Engine/SphereCollider.h"

ShotNumber9::ShotNumber9(GameObject* parent)
	:Diffusion(parent, "ShotNumber9")
{
}

ShotNumber9::~ShotNumber9()
{
}

void ShotNumber9::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Assets/Attack.fbx");
	assert(hModel_ >= 0);

	//ポインターの位置に弾が描画されるようにする
	transform_.position_ = pPointer_->GetPos();
	transform_.scale_ *= data_.size_;

	//弾に当たり判定を追加
	collision_ = new SphereCollider(XMVectorSet(0, 0, 0, 0), data_.collisionSize_);
	AddCollider(collision_);
}

void ShotNumber9::Update()
{
	transform_.position_.vecX -= diffusion_;
	transform_.position_.vecY -= diffusion_;
}

void ShotNumber9::OnCollision(GameObject* pTarget)
{
	//障害物に当たったとき
	if (pTarget->GetObjectName() == "Obstacle")
	{
		//耐久値を減少させる
		pObstacle_->HitAttack(data_.damage_);
		//弾を削除する
		KillMe();
	}
}
