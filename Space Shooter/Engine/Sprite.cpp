#pragma once

#include "Sprite.h"
#include "Camera.h"

Sprite::Sprite()
{
	//初期化
	pVertexBuffer_ = nullptr;		//頂点バッファ
	pIndexBuffer_ = nullptr;		//インデックスバッファ
	pConstantBuffer_ = nullptr;		//コンスタントバッファ

	pTexture_ = nullptr;			//Texture型のポインタ
}

Sprite::~Sprite()
{
}


HRESULT Sprite::Initialize(std::string fileName)
{

	// 頂点情報(位置とUV座標を渡している)
	VERTEX vertices[] =
	{
		//位置・UV座標
		/*( 0)*/{ XMVectorSet(-1.0f,  1.0f, -1.0f, 0.0f), XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f)}, //左上
		/*( 1)*/{ XMVectorSet( 1.0f,  1.0f, -1.0f, 0.0f), XMVectorSet(1.0f, 0.0f, 0.0f, 0.0f)}, //右上
		/*( 2)*/{ XMVectorSet( 1.0f, -1.0f, -1.0f, 0.0f), XMVectorSet(1.0f, 1.0f, 0.0f, 0.0f)}, //右下
		/*( 3)*/{ XMVectorSet(-1.0f, -1.0f, -1.0f, 0.0f), XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f)}, //左下
	};

	///////////////////////////////////////頂点バッファの設定///////////////////////////////////////

	//専用の変数を用意
	D3D11_BUFFER_DESC bd_vertex;

	//バッファのサイズ
	bd_vertex.ByteWidth = sizeof(vertices);

	//バッファで想定されている読み込みおよび書き込みの方法を識別
	bd_vertex.Usage = D3D11_USAGE_DEFAULT;

	//バッファをどのようにパイプラインにバインドするかを識別
	bd_vertex.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	//CPUアクセスのフラグ
	bd_vertex.CPUAccessFlags = 0;

	//その他のフラグ
	bd_vertex.MiscFlags = 0;

	//バッファが構造化バッファを表す場合の、バッファ構造内の各要素のサイズ
	bd_vertex.StructureByteStride = 0;

	//専用の変数を用意
	D3D11_SUBRESOURCE_DATA data_vertex;

	//サブリソースを初期化するためのデータを指定
	data_vertex.pSysMem = vertices;

	//それを使って頂点バッファを作成
	if (FAILED(Direct3D::pDevice->CreateBuffer(&bd_vertex, &data_vertex, &pVertexBuffer_)))
	{
		MessageBox(nullptr, "頂点バッファ作成失敗", "エラー", MB_OK);
		return E_FAIL;
	}


	//インデックス情報
	int index[] = { 0,2,3, 0,1,2};

	///////////////////////////////////////インデックスバッファの設定///////////////////////////////////////

	//専用の変数を用意
	D3D11_BUFFER_DESC   bd;

	//バッファで想定されている読み込みおよび書き込みの方法を識別
	bd.Usage = D3D11_USAGE_DEFAULT;

	//バッファのサイズ
	bd.ByteWidth = sizeof(index);

	//バッファをどのようにパイプラインにバインドするかを識別
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;

	//CPUアクセスのフラグ
	bd.CPUAccessFlags = 0;

	//その他のフラグ
	bd.MiscFlags = 0;

	//専用の変数を用意
	D3D11_SUBRESOURCE_DATA InitData;

	//サブリソースを初期化するためのデータを指定
	InitData.pSysMem = index;

	//テクスチャの1行の先頭から次の行までの距離
	InitData.SysMemPitch = 0;

	//ある深度レベルの開始から次の深度レベルまでの距離
	InitData.SysMemSlicePitch = 0;

	//それを使ってインデックスバッファを作成
	if (FAILED(Direct3D::pDevice->CreateBuffer(&bd, &InitData, &pIndexBuffer_)))
	{
		MessageBox(nullptr, "インデックスバッファ作成失敗", "エラー", MB_OK);
		return E_FAIL;
	}


	///////////////////////////////////////コンスタントバッファの設定///////////////////////////////////////

	//専用の変数を用意
	D3D11_BUFFER_DESC cb;

	//バッファのサイズ
	cb.ByteWidth = sizeof(CONSTANT_BUFFER);

	//バッファで想定されている読み込みおよび書き込みの方法を識別
	cb.Usage = D3D11_USAGE_DYNAMIC;

	//バッファをどのようにパイプラインにバインドするかを識別
	cb.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

	//CPUアクセスのフラグ
	cb.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	//その他のフラグ
	cb.MiscFlags = 0;

	//バッファが構造化バッファを表す場合の、バッファ構造内の各要素のサイズ
	cb.StructureByteStride = 0;

	//それを使ってコンスタントバッファを作成
	if (FAILED(Direct3D::pDevice->CreateBuffer(&cb, nullptr, &pConstantBuffer_)))
	{
		MessageBox(nullptr, "コンスタントバッファ作成失敗", "エラー", MB_OK);
		return E_FAIL;
	}


	///////////////////////////////////////テクスチャを作成///////////////////////////////////////

	//インスタンス作成
	pTexture_ = new Texture;

	//テクスチャの作成
	if (FAILED(pTexture_->Load(fileName)))
	{
		MessageBox(nullptr, "テクスチャ作成失敗", "エラー", MB_OK);
		return E_FAIL;
	}


	return S_OK;
}

HRESULT Sprite::Draw(Transform& transform, float alpha)
{
	///////使用するシェーダを指定///////
	Direct3D::SetShaderBundle(SHADER_2D);

	////////////////////////////////////////////////////サイズ変更////////////////////////////////////////////////////

	//画像のサイズの拡大行列を作成
	XMMATRIX ajustScale = XMMatrixScaling((float)pTexture_->GetWidth() / Direct3D::scrWidth_, (float)pTexture_->GetHeight() / Direct3D::scrHeight_, 1.0f);

	//ワールド行列に画像サイズ分の拡縮をかけて、(スクリーンに対しての)本来の大きさ(比率)を求める
	XMMATRIX mat = transform.GetWorldMatrix() * ajustScale;

	//視錐台の範囲での大きさを求める
	//XMMATRIX miniScale = XMMatrixScaling(1.0f / Direct3D::scrWidth_, 1.0f / Direct3D::scrHeight_, 1);


	//////////////////////////作成した構造体をインスタンス化して渡したい情報を設定する//////////////////////////

	CONSTANT_BUFFER cb;
	cb.matW = XMMatrixTranspose(mat);				//ワールド行列と拡大行列(画像サイズの行列)の合成行列
	cb.color = XMFLOAT4(1, 1, 1, alpha);			//指定されたアルファ値


	///////////////////////////////////作成した情報をコンスタントバッファに格納///////////////////////////////////

	//専用の変数を用意(サブリソースデータへのアクセスを提供)
	D3D11_MAPPED_SUBRESOURCE pdata;

	//Map関数で動きをいったん止めて書き込み可能状態にする
	if (FAILED(Direct3D::pContext->Map(pConstantBuffer_, 0, D3D11_MAP_WRITE_DISCARD, 0, &pdata)))	// GPUからのデータアクセスを止める
	{
		MessageBox(nullptr, "コンスタントバッファ書き込み失敗", "エラー", MB_OK);
		return E_FAIL;
	}
	//memcpy_sでデータを送る
	memcpy_s(pdata.pData, pdata.RowPitch, (void*)(&cb), sizeof(cb));								// データの値を送る


	///////////////////////////////////サンプラーをシェーダーに///////////////////////////////////

	ID3D11SamplerState* pSampler = pTexture_->GetSampler();
	Direct3D::pContext->PSSetSamplers(0, 1, &pSampler);


	////////////////////////////シェーダーリソースビューをシェーダーに////////////////////////////

	ID3D11ShaderResourceView* pSRV = pTexture_->GetSRV();
	Direct3D::pContext->PSSetShaderResources(0, 1, &pSRV);


	//関数内で作成したポインタ変数を解放
	/*SAFE_RELEASE(pSRV);
	SAFE_RELEASE(pSampler);*/


	//Unmapで動きを再開させる
	Direct3D::pContext->Unmap(pConstantBuffer_, 0);													//再開


	////////////////////////////////////////作成した頂点バッファをパイプラインに流す////////////////////////////////////////

	//各要素のデータサイズ?(頂点情報の(構造体の)サイズ)
	UINT stride = sizeof(VERTEX);

	//何かしらの距離?
	UINT offset = 0;

	//データを流している
	Direct3D::pContext->IASetVertexBuffers(0, 1, &pVertexBuffer_, &stride, &offset);


	///////////////////////////////////作成したインデックスバッファをパイプラインに流す///////////////////////////////////

	//各要素のデータサイズ?
	stride = sizeof(int);

	//何かしらの距離?
	offset = 0;

	//データを流している
	Direct3D::pContext->IASetIndexBuffer(pIndexBuffer_, DXGI_FORMAT_R32_UINT, 0);


	///////////////////////////////////作成したコンスタントバッファをパイプラインに流す///////////////////////////////////

	//データを流している
	Direct3D::pContext->VSSetConstantBuffers(0, 1, &pConstantBuffer_);	//頂点シェーダー用	
	Direct3D::pContext->PSSetConstantBuffers(0, 1, &pConstantBuffer_);	//ピクセルシェーダー用


	///////////////////////////////////描画///////////////////////////////////

	//インデックス数を指定
	Direct3D::pContext->DrawIndexed(6, 0, 0);


	return S_OK;
}

void Sprite::Release()
{
	SAFE_RELEASE(pTexture_);				//Texture型のポインタ変数
	SAFE_DELETE(pTexture_);					//Textureのインスタンス

	SAFE_RELEASE(pConstantBuffer_);			//コンスタントバッファのポインタ変数
	SAFE_RELEASE(pIndexBuffer_);			//インデックスバッファのポインタ変数
	SAFE_RELEASE(pVertexBuffer_);			//頂点バッファのポインタ変数
}
