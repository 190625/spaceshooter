#pragma once

#include <vector>
#include "Fbx.h"

namespace Model
{
	//各モデルの情報を管理する構造体
	struct ModelData
	{
		Fbx* pFbx;					//Fbx型のポインタ
		Transform transform;		//変形を扱う変数
		std::string fileName;		//ロードするファイル名


		//構想体のコンストラクタ
		ModelData()
		{
			pFbx = nullptr;
		}
	};


	int Load(std::string fileName);

	void SetTransform(int handle, Transform& transform);

	void Draw(int handle, SHADER_TYPE shaderType);

	void RayCast(int handle, RayCastData * rayData);

	void Release(int handle);

	void AllRelease();
};

