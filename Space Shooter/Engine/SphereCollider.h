#pragma once

#include "Collider.h"

class SphereCollider : public Collider
{
	//Colliderクラスのメンバにアクセスできるようにする
	friend class Collider;

public:
	//コンストラクタ(コライダーの作成)
	//第一引数：basePos	コライダーの中心位置(ゲームオブジェクトの原点から見た位置)
	//第二引数：radius	コライダーのサイズ(半径)
	SphereCollider(XMVECTOR center, float radius);

private:
	//接触判定
	//第一引数：target	判定したい相手
	//戻値：接触してればtrue
	bool IsHit(Collider* target) override;
};

