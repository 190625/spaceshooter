#pragma once

#include "GameObject.h"
#include "Direct3D.h"

//引数なしの(自分を作るための?)コンストラクタ
GameObject::GameObject() :GameObject(nullptr, "")
{
}

//引数ありのコンストラクタ
GameObject::GameObject(GameObject * parent, const std::string & name)
{
	//初期化
	childList_.clear();		//自分以下のオブジェクトのリスト(ポインタ)
	colliderList_.clear();	//(自オブジェクトに追加した)コライダーのリスト(ポインタ)
	pParent_ = parent;		//引数で受け取ったオブジェクトを自分の直上のオブジェクトに設定する
	objectName_ = name;		//引数で受け取った名前をオブジェクト名に設定する
	isDead_ = false;		//消さないように設定
}

//デストラクタ
GameObject::~GameObject()
{
	pParent_ = nullptr;
	AllChildKill(); 
}

void GameObject::UpdateSub()
{
	//自分の更新処理
	Update();

	//行列作成
	transform_.Calclation();

	//リストに追加されている自分の子供の更新処理を呼ぶ
	for (auto itr = childList_.begin(); itr != childList_.end(); itr++)
	{
		(*itr)->UpdateSub();
	}


	////////////////すべてのオブジェクトの更新処理が呼ばれた後に、一番下のオブジェクトから順に行われる処理////////////////


	//リストに追加されている子供の数分繰り返す
	//※リストの要素を消す可能性がある場合は、無条件でインクリメントしない
	for (auto itr = childList_.begin(); itr != childList_.end();)
	{
		//オブジェクトの消去フラグが立っていたら
		if ((*itr)->isDead_)
		{
			/////オブジェクトの消去処理/////
			(*itr)->ReleaseSub();

			/////削除した次の要素を設定/////
			itr = childList_.erase(itr);
		}
		//オブジェクトの消去フラグが立っていなかったら
		else
		{
			//「自分の所持しているコライダー」と「自分の親が所持しているコライダー」の当たり判定をする
			(*itr)->Collision(GetParent());

			//リストの要素を進める
			itr++;
		}
	}
}

void GameObject::DrawSub()
{
	Draw();

	for (auto itr = childList_.begin(); itr != childList_.end(); itr++)
	{
		(*itr)->DrawSub();
	}
}

void GameObject::ReleaseSub()
{
	Release();

	for (auto itr = childList_.begin(); itr != childList_.end(); itr++)
	{
		(*itr)->ReleaseSub();
	}
}

//消去フラグの変更
void GameObject::KillMe()
{
	isDead_ = true;
}

//直下のオブジェクトを全て消去
void GameObject::AllChildKill()
{
	for (auto itr = childList_.begin(); itr != childList_.end(); itr++)
	{
		(*itr)->Release();
		SAFE_DELETE(*itr);
	}
	childList_.clear();


	//もしかするといらないかも???????????/
	for (auto itr = colliderList_.begin(); itr != colliderList_.end(); itr++)
	{
		(*itr)->Release();
		SAFE_DELETE(*itr);
	}
	colliderList_.clear();
}

GameObject * GameObject::FindChildObject(std::string objectName)
{
	//リストに追加されている数分繰り返す
	for (auto itr = childList_.begin(); itr != childList_.end(); itr++)
	{
		//指定したオブジェクトが見つかったら
		if ((*itr)->objectName_ == objectName)
		{
			//オブジェクトのアドレスを返す
			return *itr;
		}

		//再帰処理
		GameObject* pObject = (*itr)->FindChildObject(objectName);

		//見つかっていたら
		if (pObject != nullptr)
		{
			//オブジェクトのアドレスを返す
			return pObject;
		}

		//使い終わったポインタ変数を解放
		SAFE_RELEASE(pObject);
	}

	return nullptr;
}

GameObject * GameObject::GetRootJob()
{
	//見つかったら
	if (pParent_ == nullptr)
	{
		return this;
	}
	//見つからなかったら
	else
	{
		//再帰処理
		return pParent_->GetRootJob();
	}
}

GameObject* GameObject::FindObject(std::string objectName)
{
	//RootJobを探した後、指定されたオブジェクトを探す
	return GetRootJob()->FindChildObject(objectName);
}

void GameObject::AddCollider(Collider * collider)
{
	//コライダーを作成
	collider->SetGameObject(this);

	//リストに追加する
	colliderList_.push_back(collider);
}

//衝突判定
//第一引数：pTarget	衝突しているかを調べる相手
void GameObject::Collision(GameObject * pTarget)
{
	//例外(自コライダー同士の当たり判定はしない)
	if (pTarget == nullptr || this == pTarget)
	{
		return;
	}


	//自分と相手(pTarget)のコライダーを使って当たり判定
	//(1つのオブジェクトが複数のコライダーを持ってる場合もあるので二重ループ(全ての組み合わせを調べる))

	//自分のリストに追加されている分繰り返す
	for (auto i = this->colliderList_.begin(); i != this->colliderList_.end(); i++)
	{
		//相手のリストに追加されている分繰り返す
		for (auto j = pTarget->colliderList_.begin(); j != pTarget->colliderList_.end(); j++)
		{
			//当たっているか判定
			if ((*i)->IsHit(*j))
			{
				//pTargetと当たっていた場合のみ、呼ばれる処理
				//※(純粋仮想関数で定義されているので、当たった場合の処理を使いたいオブジェクトのクラスでオーバーライドしておくと、その処理が呼ばれるようになる)
				this->OnCollision(pTarget);
			}
		}
	}


	//相手の直下にオブジェクトが(子供がいない)なら終わり
	if (pTarget->childList_.empty())
		return;


	//相手の子供も当たり判定
	for (auto i = pTarget->childList_.begin(); i != pTarget->childList_.end(); i++)
	{
		//再帰処理
		Collision(*i);
	}
}

void GameObject::CollisionDraw()
{
	//描画用のシェーダーを設定
	Direct3D::SetShaderBundle(SHADER_LINE);

	//自分のリストに追加されている分繰り返す
	for (auto i = this->colliderList_.begin(); i != this->colliderList_.end(); i++)
	{
		//描画処理
		(*i)->Draw(transform_.position_);
	}

}