#pragma once

#include "GameObject.h"

class RootJob :
	public GameObject
{
public:
	RootJob();		//コンストラクタ
	~RootJob();		//デストラクタ


	//仮想関数
	virtual void Initialize() override;
	virtual void Update() override;
	virtual void Draw() override;
	virtual void Release() override;
};

