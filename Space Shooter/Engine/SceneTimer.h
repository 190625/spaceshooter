#pragma once

#include <stdint.h>

#include <windows.h>
#pragma comment(lib, "winmm.lib")

namespace SceneTimer
{
	static uint64_t elapsedSeconds_ = 0;		//経過時間(秒)
	static float	second_ = 0.0f;				//端数の秒[0〜1]
	static float	delta_ = 0.0f;				//デルタタイム??????
	static uint64_t	elapsedFrames_ = 0;			//経過フレーム
	static uint64_t totalFrames_ = 0;			//トータル経過フレーム
	static DWORD	elapsedMilliSecond_ = 0;	//経過時間(ミリ秒)


	//シーンがスタートしてからの経過時間(sec)
	float GetElapsedSecounds();

	//フレームのデルタタイム(sec)
	float GetDelta();

	//シーンタイマーのリセット
	void Reset();

	//デルタタイムの更新
	void UpdateFrameDelta();
};