#pragma once

#include "Collider.h"

class BoxCollider : public Collider
{
	//Colliderクラスのメンバにアクセスできるようにする
	friend class Collider;

public:
	//コンストラクタ(コライダーの作成)
	//第一引数：basePos	コライダーの中心位置(ゲームオブジェクトの原点から見た位置)
	//第二引数：size	コライダーのサイズ(幅、高さ、奥行き)
	//第三引数：rotate  コライダーの回転率
	BoxCollider(XMVECTOR basePos, XMVECTOR size, XMVECTOR rotate);

private:
	//接触判定
	//第一引数：target	判定したい相手
	//戻値：接触してればtrue
	bool IsHit(Collider* target) override;
};
