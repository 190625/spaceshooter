#pragma once

#include "BoxCollider.h"
#include "SphereCollider.h"
#include "Model.h"

BoxCollider::BoxCollider(XMVECTOR basePos, XMVECTOR size, XMVECTOR rotate)
{
	transform_.position_ = basePos;
	transform_.scale_ = size;
	transform_.rotate_ = rotate;
	type_ = COLLIDER_BOX;

	//リリース時は判定枠は表示しない
#ifdef _DEBUG
	//テスト表示用判定枠
	hDebugModel_ = Model::Load("Assets/DebugCollision/boxCollider.fbx");
#endif
}

bool BoxCollider::IsHit(Collider * target)
{
	//指定された相手が箱型コライダーの場合
	if (target->type_ == COLLIDER_BOX)
		return IsHitBoxVsBox((BoxCollider*)target, this);			//判定結果を返す
	//指定された相手が球体コライダーの場合
	else
		return IsHitBoxVsCircle(this, (SphereCollider*)target);		//判定結果を返す
}
