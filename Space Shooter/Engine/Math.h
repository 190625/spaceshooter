#pragma once

#include "Transform.h"

namespace Math
{
	//行列式(クラメルの公式)
	float Det(XMVECTOR a, XMVECTOR b, XMVECTOR c);

	//三角形と線分の交差判定
	//第一引数：レイのスタート地点
	//第二引数：レイの方向
	//第三、第四、第五引数：ポリゴンの各頂点の位置
	//第六引数：レイとの距離を代入するためのポインタ
	bool Intersect(XMVECTOR origin, XMVECTOR ray, XMVECTOR v0, XMVECTOR v1, XMVECTOR v2, float* t);
}