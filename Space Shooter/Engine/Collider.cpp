#pragma once

#include "Collider.h"
#include "BoxCollider.h"
#include "SphereCollider.h"
#include "GameObject.h"
#include "Model.h"
#include "Direct3D.h"

Collider::Collider()
{
	pGameObject_ = nullptr;
	hDebugModel_ = -1;
}

Collider::~Collider()
{
}

//箱型同士の衝突判定
//第一引数：boxA　判定したい(オブジェクトの)1つ目の箱型コライダーの情報を持ったインスタンス
//第二引数：boxB　判定したい(オブジェクトの)2つ目の箱型コライダーの情報を持ったインスタンス
//戻値：接触していればtrue
bool Collider::IsHitBoxVsBox(BoxCollider * boxA, BoxCollider * boxB)
{
	//引数のインスタンスの情報から判定用のデータを作成する
	XMVECTOR boxPosA = boxA->pGameObject_->GetPosition() + boxA->transform_.position_;
	XMVECTOR boxPosB = boxB->pGameObject_->GetPosition() + boxB->transform_.position_;

	//判定式
	if ((boxPosA.vecX + boxA->transform_.scale_.vecX / 2) > (boxPosB.vecX - boxB->transform_.scale_.vecX / 2) &&
		(boxPosA.vecX - boxA->transform_.scale_.vecX / 2) < (boxPosB.vecX + boxB->transform_.scale_.vecX / 2) &&
		(boxPosA.vecY + boxA->transform_.scale_.vecY / 2) > (boxPosB.vecY - boxB->transform_.scale_.vecY / 2) &&
		(boxPosA.vecY - boxA->transform_.scale_.vecY / 2) < (boxPosB.vecY + boxB->transform_.scale_.vecY / 2) &&
		(boxPosA.vecZ + boxA->transform_.scale_.vecZ / 2) > (boxPosB.vecZ - boxB->transform_.scale_.vecZ / 2) &&
		(boxPosA.vecZ - boxA->transform_.scale_.vecZ / 2) < (boxPosB.vecZ + boxB->transform_.scale_.vecZ / 2))
	{
		return true;
	}

	return false;
}

//箱型と球体の衝突判定
//第一引数：box		判定したい(オブジェクトの)箱型コライダーの情報を持ったインスタンス
//第二引数：sphere	判定したい(オブジェクトの)球体コライダーの情報を持ったインスタンス
//戻値：接触していればtrue
bool Collider::IsHitBoxVsCircle(BoxCollider * box, SphereCollider * sphere)
{
	//引数のインスタンスの情報から判定用のデータを作成する
	XMVECTOR circlePos = sphere->pGameObject_->GetPosition() + sphere->transform_.position_;
	XMVECTOR boxPos = box->pGameObject_->GetPosition() + box->transform_.position_;

	//判定式
	if (circlePos.vecX > ((boxPos.vecX - box->transform_.scale_.vecX) - sphere->transform_.scale_.vecX) &&
		circlePos.vecX < ((boxPos.vecX + box->transform_.scale_.vecX) + sphere->transform_.scale_.vecX) &&
		circlePos.vecY > ((boxPos.vecY - box->transform_.scale_.vecY) - sphere->transform_.scale_.vecX) &&
		circlePos.vecY < ((boxPos.vecY + box->transform_.scale_.vecY) + sphere->transform_.scale_.vecX) &&
		circlePos.vecZ > ((boxPos.vecZ - box->transform_.scale_.vecZ) - sphere->transform_.scale_.vecX) &&
		circlePos.vecZ < ((boxPos.vecZ + box->transform_.scale_.vecZ) + sphere->transform_.scale_.vecX))
	{
		return true;
	}

	return false;
}

//球体同士の衝突判定
//第一引数：circleA　判定したい(オブジェクトの)1つ目の球体コライダーの情報を持ったインスタンス
//第二引数：circleB　判定したい(オブジェクトの)2つ目の球体コライダーの情報を持ったインスタンス
//戻値：接触していればtrue
bool Collider::IsHitCircleVsCircle(SphereCollider * circleA, SphereCollider * circleB)
{
	//引数のインスタンスの情報から判定用のデータを作成する
	XMVECTOR Vec = (circleA->transform_.position_ + circleA->pGameObject_->GetPosition()) - (circleB->transform_.position_ + circleB->pGameObject_->GetPosition());
	Vec = XMVector3Length(Vec);

	//判定式
	if (Vec.vecX <= (circleA->transform_.scale_.vecX + circleB->transform_.scale_.vecX))
	{
		return true;
	}

	return false;
}

//テスト表示用の枠を描画
//第一引数：コライダーをつけたオブジェクトの移動量
void Collider::Draw(XMVECTOR position)
{
	//変形を扱う変数
	Transform transform;

	//コライダーの位置を設定
	transform.position_ = XMVectorSet((transform_.position_.vecX + position.vecX), (transform_.position_.vecY + position.vecY), (transform_.position_.vecZ + position.vecZ), 0.0f);

	//コライダーのサイズを設定
	transform.scale_ = XMVectorSet(transform_.scale_.vecX, transform_.scale_.vecY, transform_.scale_.vecZ, 0.0f);

	//コライダーの回転率を設定
	transform.rotate_ = XMVectorSet(transform_.rotate_.vecX, transform_.rotate_.vecY, transform_.rotate_.vecZ, 0.0f);

	//行列を作成
	transform.Calclation();

	//コライダーの枠を描画
	Model::SetTransform(hDebugModel_, transform);
	Model::Draw(hDebugModel_, SHADER_LINE);
}

void Collider::Release()
{
	pGameObject_ = nullptr;
	Model::AllRelease();
}
