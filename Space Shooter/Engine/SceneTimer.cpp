#pragma once

#include "SceneTimer.h"


namespace SceneTimer
{
	//シーンがスタートしてからの経過時間(sec)
	float GetElapsedSecounds()
	{
		return (float)elapsedSeconds_ + second_;	// IEEE754精度に注意
	}


	//フレームのデルタタイム(sec)
	float GetDelta()
	{
		return delta_;
	}


	//シーンタイマーのリセット
	void Reset()
	{
		elapsedSeconds_ = 0;		//経過時間(秒)をリセット
		elapsedFrames_ = 0;			//経過フレームをリセット
		elapsedMilliSecond_ = 0;	//経過時間(ミリ秒)をリセット
		delta_ = 0.0f;				//デルタタイム????をリセット
	}


	//デルタタイムの更新
	void UpdateFrameDelta()
	{
		static DWORD prevTime = timeGetTime();		//最後にフレームを更新したときの時間(ミリ秒)
		DWORD nowTime = timeGetTime();				//現在の時間(ミリ秒)
		DWORD deltaMS;								//デルタタイムを入れる変数(ミリ秒)

		//最後にフレームを更新したときの時間と現在の時間を比較
		if (prevTime <= nowTime)
			deltaMS = nowTime - prevTime;			//最後にフレームを更新してから今までに経過した時間(ミリ秒)
		else
			deltaMS = (MAXDWORD - prevTime) + nowTime + 1;		//???????????

		//最終フレーム更新時間を更新する(ミリ秒)
		prevTime = nowTime;

		//経過時間(ミリ秒)にデルタタイムを加算
		elapsedMilliSecond_ += deltaMS;

		//経過時間(ミリ秒)をマイクロ秒に変換して代入
		DWORD second = elapsedMilliSecond_ / 1000;

		//経過時間(ミリ秒)をマイクロ秒に変換したときの余りを代入
		elapsedMilliSecond_ %= 1000;

		//上記の余りを 0 〜 1 の間に丸め込む
		second_ = (float)elapsedMilliSecond_ * 0.001f;

		//デルタタイムを????????
		delta_ = (float)deltaMS * 0.001f;

		//経過時間(マイクロ秒)を加算???
		elapsedSeconds_ += second;

		//経過フレームを加算
		++elapsedFrames_;

		//トータル経過フレームを加算
		++totalFrames_;
	}

};