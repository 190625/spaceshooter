#pragma once

#include <d3d11.h>
#include <fbxsdk.h>
#include <string>
#include "Transform.h"
#include "Direct3D.h"
#include "Texture.h"


#pragma comment(lib, "LibFbxSDK-MT.lib")
#pragma comment(lib, "LibXml2-MT.lib")
#pragma comment(lib, "zlib-MT.lib")


//レイキャスト用構造体
struct RayCastData
{
	XMVECTOR	start;	//レイ発射位置
	XMVECTOR	dir;	//レイの向きベクトル
	float		dist;	//衝突点までの距離
	BOOL        hit;	//レイが当たったか
};


class Fbx
{
	////////////////////////////////////ポリゴン表示に必要なもの////////////////////////////////////

	//コンスタントバッファ(渡す情報をまとめた構造体を用意)
	struct CONSTANT_BUFFER
	{
		XMMATRIX	matWVP;			//カメラの情報(ワールド・ビュー・プロジェクションの合成行列)
		XMMATRIX	matW;			//ワールド行列
		XMFLOAT4	diffuseColor;	//ディフューズカラー(マテリアルの色)(初期化方法不明)
		BOOL		isTexture;		//テクスチャ貼ってあるかどうか
	};

	//頂点情報(頂点シェーダーに渡す情報の構造体)
	struct VERTEX
	{
		XMVECTOR position;			//位置
		XMVECTOR uv;				//UV座標
		XMVECTOR normal;			//法線
	};

	//変数宣言
	ID3D11Buffer *pVertexBuffer_;		//頂点バッファ(ポインタ)(頂点情報の置き場所)
	ID3D11Buffer **ppIndexBuffer_;		//インデックスバッファ(ポインタ)(インデックス情報の置き場所)を分けるための配列(既にポインタなので、ポインタのポインタにする)
	//※同じ色を使っている部分をすべて表示してから、次の色を使っている部分をすべて表示する　という形式なので
	// マテリアルごとにインデックスバッファを分ける必要がある
	ID3D11Buffer *pConstantBuffer_;		//コンスタントバッファ(ポインタ)(カメラ・ライト....情報の置き場所)


	////////////////////////////////////FBXを扱うために必要なもの////////////////////////////////////

	//変数宣言
	int vertexCount_;			//頂点数(FBXの)
	int polygonCount_;			//ポリゴン数(FBXの)


	////////////////////////////////////マテリアル(テクスチャ + 色情報)を扱うために必要なもの////////////////////////////////////

	//マテリアル情報
	struct MATERIAL
	{
		Texture*	pTexture;		//テクスチャ情報
		XMFLOAT4	diffuse;		//マテリアルの色(初期化方法不明)
	};

	//変数宣言
	int materialCount_;						//マテリアル数(FBXの)
	MATERIAL* pMaterialList_;				//マテリアル数分の配列
	int* pIndexCountEachMaterial_;			//マテリアルごとののインデックス数


	////////////////////////////////////当たり判定に必要なもの////////////////////////////////////

	//各ポリゴンの頂点情報の配列
	VERTEX* pVertices_;

	//マテリアルごとのインデックス情報(ポインタのポインタにして2次元配列にする)
	int** ppIndex_;


public:

	Fbx();
	HRESULT Load(std::string fileName);
	//頂点バッファ準備
	HRESULT	InitVertex(fbxsdk::FbxMesh * mesh);
	//インデックスバッファ準備
	HRESULT InitIndex(fbxsdk::FbxMesh * mesh);
	//コンスタントバッファ準備
	HRESULT InitConstantBuffer();
	//マテリアル情報準備
	HRESULT InitMaterial(fbxsdk::FbxNode* pNode);
	HRESULT	Draw(Transform& transform, SHADER_TYPE shaderType);
	//ポリゴンとレイの当たり判定
	void	RayCast(RayCastData* rayData);
	void    Release();
};