#pragma once

#include <list>
#include <assert.h>
#include "Transform.h"
#include "Collider.h"
#include <string>

//マクロ(解放処理)
#define SAFE_DELETE(p) if(p != nullptr){ delete p; p = nullptr;}				//作成したインスタンスを解放
#define SAFE_DELETE_(p) if(p != nullptr){ delete[] p; p = nullptr;}				//作成したインスタンスを解放(配列用)
#define SAFE_RELEASE(p) if(p != nullptr){ p->Release(); p = nullptr;}			//ポインタ変数を解放

//定数
const int reset_ = 0;		//リセット

class GameObject
{
	//変数
	std::string	objectName_;					//オブジェクト名
	bool isDead_;								//オブジェクトを消すか消さないかのフラグ

public:
	GameObject();												//コンストラクタ(引数なし)
	GameObject(GameObject* parent, const std::string& name);	//コンストラクタ(引数あり)
	~GameObject();												//デストラクタ


	//変数
	GameObject*	pParent_;						//自分の直上のオブジェクトを示す変数(ポインタ)
	std::list<GameObject*> childList_;			//自分以下のオブジェクトのリスト(ポインタ)
	Transform	transform_;						//変形を扱うインスタンス
	std::list<Collider*>	colliderList_;		//(自オブジェクトに追加した)コライダーのリスト


	//純粋仮想関数
	virtual void Initialize() = 0;
	virtual void Update() = 0;
	virtual void Draw() = 0;
	virtual void Release() = 0;


	//何かと衝突した場合に呼ばれる(オーバーライド用)
	//引数：pTarget	衝突した相手
	virtual void OnCollision(GameObject* pTarget) {};


	//関数テンプレート(このように書くと「T」と書いた部分は、この関数を呼ぶときに何型にするかを決めることができる)
	//シーン(オブジェクトを)を作成し、自分の直上と(親と)直上の(親の)オブジェクトが持っているリストに自分を追加する関数
	template <class T>
	void Instantiate(GameObject* parent)
	{
		T* P;
		P = new T(parent);
		P->Initialize();
		childList_.push_back(P);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	//オブジェクト名を決め打ちで指定したい場合の関数
	//関数テンプレート(このように書くと「T」と書いた部分は、この関数を呼ぶときに何型にするかを決めることができる)
	//シーン(オブジェクト)を作成し、自分の直上と(親と)直上の(親の)オブジェクトが持っているリストに自分を追加する関数
	template <class T>
	void Instantiation(GameObject* parent, const std::string& name)
	{
		T* P;
		P = new T(parent, name);
		P->Initialize();
		childList_.push_back(P);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//自分の処理を呼んだ後、自分の直下のリストに入っているオブジェクトの処理を呼ぶ
	void UpdateSub();
	void DrawSub();
	void ReleaseSub();

	//消去フラグの変更
	void KillMe();

	//直下のオブジェクトを全て消去
	void AllChildKill();

	//子や孫からオブジェクトを探す処理
	GameObject* FindChildObject(std::string objectName);

	//RootJobを探す処理
	GameObject* GetRootJob();

	//オブジェクトを探す処理
	GameObject* FindObject(std::string objectName);

	//オブジェクトの位置を渡す
	XMVECTOR GetPosition() { return transform_.position_; };

	//コライダー(衝突判定)を追加する
	void AddCollider(Collider * collider);

	//衝突判定
	//第一引数：pTarget	衝突しているかを調べる相手
	void Collision(GameObject* pTarget);

	//テスト用の衝突判定枠を表示
	void CollisionDraw();

	//オブジェクトの名前を渡す
	std::string GetObjectName() { return objectName_; };

	//自分の親オブジェクトを渡す
	GameObject* GetParent() { return pParent_; };
};