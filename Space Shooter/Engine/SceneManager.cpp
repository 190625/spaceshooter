#pragma once

#include "SceneManager.h"
#include "../PlayScene.h"
#include "../TitleScene.h"
#include "Model.h"
#include "Splash.h"


SceneManager::SceneManager(GameObject * parent)
	:GameObject(parent, "SceneManager")
{
	//最初のシーンを設定して初期化
	currentScene_ = SCENE_ID_TITLE;
	nextScene_ = SCENE_ID_TITLE;
}

SceneManager::~SceneManager()
{
}

void SceneManager::Initialize()
{
	Instantiate<TitleScene>(this);
}

void SceneManager::Update()
{
	//シーンを切り替えるとき
	if (currentScene_ != nextScene_)
	{
		//SceneManager以下のオブジェクトを全て削除
		AllChildKill();
		//使用したモデルを全て削除
		Model::AllRelease();
		//使用した画像を全て削除
		Splash::AllRelease();

		//次のシーンを設定する
		switch (nextScene_)
		{
		case SCENE_ID_PLAY:
			Instantiate<PlayScene>(this); break;
		case SCENE_ID_TITLE:
			Instantiate<TitleScene>(this); break;
		default:
			break;
		}

		//現在のシーンも更新
		currentScene_ = nextScene_;
	}
}

void SceneManager::Draw()
{
}

void SceneManager::Release()
{
}

void SceneManager::ChangeScene(SCENE_ID nextScene)
{
	nextScene_ = nextScene;
}
