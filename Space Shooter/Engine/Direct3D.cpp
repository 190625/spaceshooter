#pragma once

#include <d3dcompiler.h>
#include "Direct3D.h"
#include "Camera.h"


//変数
namespace Direct3D
{
	//変数宣言
	ID3D11Device*           pDevice = nullptr;				//デバイス(ポインタ)
	ID3D11DeviceContext*    pContext = nullptr;				//デバイスコンテキスト(ポインタ)
	IDXGISwapChain*         pSwapChain = nullptr;			//スワップチェイン(ポインタ)
	ID3D11RenderTargetView* pRenderTargetView = nullptr;	//レンダーターゲットビュー(ポインタ)

	ID3D11Texture2D*		pDepthStencil = nullptr;		//深度ステンシル(ポインタ)(描画した箇所の深度情報を書き込むバッファ(テクスチャ))
	ID3D11DepthStencilView* pDepthStencilView = nullptr;	//深度ステンシルビュー(ポインタ)(深度情報が書き込んであるバッファにアクセスするためのもの)

	ID3D11BlendState*		pBlendState;					//ブレンドステート(ポインタ)

	int scrWidth_ = NULL;									//スクリーン幅
	int scrHeight_ = NULL;									//スクリーン高さ


	//シェーダーを使い分ける為に(3D用 or 2D用)構造体にする
	struct ShaderBundle
	{
		//(出来上がったシェーダーを入れるための変数)
		ID3D11VertexShader*	pVertexShader = nullptr;			//頂点シェーダー(ポインタ)
		ID3D11PixelShader*	pPixelShader = nullptr;				//ピクセルシェーダー(ポインタ)

		//(頂点に持たせたい情報を入れるための変数)
		ID3D11InputLayout*	pVertexLayout = nullptr;			//頂点インプットレイアウト(ポインタ)

		//(どのピクセルを光らせるのかの情報を入れるための変数)
		ID3D11RasterizerState*	pRasterizerState = nullptr;		//ラスタライザ(ポインタ)
	};


	//作成した構造体の配列
	ShaderBundle shaderBundle[SHADER_MAX];
}

//初期化
HRESULT Direct3D::Initialize(int winW, int winH, HWND hWnd)
{
	///////////////////////////いろいろ準備するための設定///////////////////////////////

	//いろいろな設定項目をまとめた構造体
	DXGI_SWAP_CHAIN_DESC scDesc;

	//とりあえず全部0(メモリのブロックをゼロで埋める)
	ZeroMemory(&scDesc, sizeof(scDesc));

	//描画先のフォーマット
	scDesc.BufferDesc.Width = winW;							//画面幅
	scDesc.BufferDesc.Height = winH;						//画面高さ
	scDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;	//何色使えるか

	//FPS（1/60秒に1回）
	scDesc.BufferDesc.RefreshRate.Numerator = 60;
	scDesc.BufferDesc.RefreshRate.Denominator = 1;

	//その他
	scDesc.Windowed = TRUE;									//ウィンドウモードかフルスクリーンか
	scDesc.OutputWindow = hWnd;								//ウィンドウハンドル
	scDesc.BufferCount = 1;									//バックバッファの枚数
	scDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;	//バックバッファの使い道＝画面に描画するために
	scDesc.SampleDesc.Count = 1;							//MSAA（アンチエイリアス）の設定
	scDesc.SampleDesc.Quality = 0;							//　〃


	////////////////上記設定をもとにデバイス、コンテキスト、スワップチェインを作成////////////////////////

	D3D_FEATURE_LEVEL level;
	if (FAILED(D3D11CreateDeviceAndSwapChain(
		nullptr,						// どのビデオアダプタを使用するか？既定ならばnullptrで
		D3D_DRIVER_TYPE_HARDWARE,		// ドライバのタイプを渡す。ふつうはHARDWARE
		nullptr,						// 上記をD3D_DRIVER_TYPE_SOFTWAREに設定しないかぎりnullptr
		0,								// 何らかのフラグを指定する。（デバッグ時はD3D11_CREATE_DEVICE_DEBUG？）
		nullptr,						// デバイス、コンテキストのレベルを設定。nullptrにしとけばOK
		0,								// 上の引数でレベルを何個指定したか
		D3D11_SDK_VERSION,				// SDKのバージョン。必ずこの値
		&scDesc,						// 上でいろいろ設定した構造体
		&pSwapChain,					// 無事完成したSwapChainのアドレスが返ってくる
		&pDevice,						// 無事完成したDeviceアドレスが返ってくる
		&level,							// 無事完成したDevice、Contextのレベルが返ってくる
		&pContext)))					// 無事完成したContextのアドレスが返ってくる
	{
		MessageBox(nullptr, "Direct3D初期化失敗", "エラー", MB_OK);
		return E_FAIL;
	}


	///////////////////////////レンダーターゲットビュー作成///////////////////////////////

	//スワップチェインからバックバッファを取得（バックバッファ ＝ レンダーターゲット）
	ID3D11Texture2D* pBackBuffer;
	if (FAILED(pSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer)))
	{
		MessageBox(nullptr, "バックバッファ取得失敗", "エラー", MB_OK);
		return E_FAIL;
	}

	//レンダーターゲットビューを作成
	if (FAILED(pDevice->CreateRenderTargetView(pBackBuffer, NULL, &pRenderTargetView)))
	{
		MessageBox(nullptr, "レンダーターゲットビュー作成失敗", "エラー", MB_OK);
		return E_FAIL;
	}

	//一時的にバックバッファを取得しただけなので解放
	SAFE_RELEASE(pBackBuffer);


	///////////////////////////ビューポート（描画範囲）設定///////////////////////////////

	//レンダリング結果を表示する範囲
	D3D11_VIEWPORT vp;
	vp.Width = (float)winW;		//幅
	vp.Height = (float)winH;	//高さ
	vp.MinDepth = 0.0f;			//手前
	vp.MaxDepth = 1.0f;			//奥
	vp.TopLeftX = 0;			//左
	vp.TopLeftY = 0;			//上


	///////////////////////深度ステンシル(テクスチャ)と深度ステンシルビューの作成///////////////////////

	//2Dテクスチャを作成するために必要な構造体
	D3D11_TEXTURE2D_DESC descDepth;
	descDepth.Width = winW;														//テクスチャ幅(テクセル単位)
	descDepth.Height = winH;													//テクスチャ高さ(テクセル単位)
	descDepth.MipLevels = 1;													//テクスチャ内のミップマップレベルの最大数
	descDepth.ArraySize = 1;													//テクスチャ配列内のテクスチャの数
	descDepth.Format = DXGI_FORMAT_D32_FLOAT;									//テクスチャフォーマット
	descDepth.SampleDesc.Count = 1;												//ピクセルあたりのマルチサンプルの数
	descDepth.SampleDesc.Quality = 0;											//画質レベル
	descDepth.Usage = D3D11_USAGE_DEFAULT;										//テクスチャの読み取りおよび書き込み方法を識別する値
	descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;								//パイプラインステージにバインドするためのフラグ
	descDepth.CPUAccessFlags = 0;												//許可されるCPUアクセスのタイプを指定するフラグ
	descDepth.MiscFlags = 0;													//他のあまり一般的ではないリソースオプションを識別するフラグ

	////2Dテクスチャの配列を作成
	if (FAILED(pDevice->CreateTexture2D(&descDepth, NULL, &pDepthStencil)))
	{
		MessageBox(nullptr, "深度ステンシルの作成失敗", "エラー", MB_OK);
		return E_FAIL;
	}

	////深度ステンシルビューを作成
	if (FAILED(pDevice->CreateDepthStencilView(pDepthStencil, NULL, &pDepthStencilView)))
	{
		MessageBox(nullptr, "深度ステンシルビューの作成失敗", "エラー", MB_OK);
		return E_FAIL;
	}


	//////////////////////////////////ブレンドステートの作成//////////////////////////////////
	
	//ブレンドステートを作成するために必要な構造体
	D3D11_BLEND_DESC BlendDesc;

	//とりあえず全部0(メモリのブロックをゼロで埋める)
	ZeroMemory(&BlendDesc, sizeof(BlendDesc));

	//ピクセルをレンダーターゲットに設定するときに、
	//マルチサンプリング手法としてアルファからカバレッジを使用するかどうかを指定
	BlendDesc.AlphaToCoverageEnable = FALSE;

	//同時レンダーターゲットで独立したブレンドを有効にするかどうかを指定
	BlendDesc.IndependentBlendEnable = FALSE;

	////レンダーターゲットのブレンド状態を記述するD3D11_RENDER_TARGET_BLEND_DESC構造体の配列
	BlendDesc.RenderTarget[0].BlendEnable = TRUE;									//ブレンディングを有効(または無効)にする
	BlendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;						//ピクセルシェーダーが出力するRGB値に対して実行する操作を指定
	BlendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;				//レンダーターゲットの現在のRGB値に対して実行する操作を指定
	BlendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;							//結合する方法を定義
	BlendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;						//ピクセルシェーダーが出力するアルファ値に対して実行する操作を指定
	BlendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;					//レンダーターゲットの現在のアルファ値に対して実行する操作を指定
	BlendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;					//どのように組み合わせるかを定義
	BlendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;	//書き込みマスク

	////ブレンドステートを作成
	if (FAILED(pDevice->CreateBlendState(&BlendDesc, &pBlendState)))
	{
		MessageBox(nullptr, "ブレンドステートの作成失敗", "エラー", MB_OK);
		return E_FAIL;
	}

	////RGBAコンポーネントごとに1つずつ、ブレンドファクターの配列
	float blendFactor[4] = { D3D11_BLEND_ZERO, D3D11_BLEND_ZERO, D3D11_BLEND_ZERO, D3D11_BLEND_ZERO };

	////出力マージステージのブレンド状態を設定
	pContext->OMSetBlendState(pBlendState, blendFactor, 0xffffffff);


	//データを画面に描画するための一通りの設定(パイプライン)
	pContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);	//データの入力種類を指定
	pContext->OMSetRenderTargets(1, &pRenderTargetView, pDepthStencilView);		//描画先を設定
	pContext->RSSetViewports(1, &vp);											//ビューポートの配列をパイプラインのラスタライザステージにバインド


	//シェーダー準備
	if (FAILED(InitShader()))
	{
		return E_FAIL;
	}

	//カメラ準備
	Camera::Initialize();

	//スクリーンサイズを渡す
	scrWidth_ = winW;
	scrHeight_ = winH;

	return S_OK;
}

//シェーダー準備
HRESULT Direct3D::InitShader()
{
	//2D用のシェーダー
	InitShader2D();
	//3D用のシェーダー
	InitShader3D();
	//コライダーの枠用のシェーダー
	InitShaderLine();

	//使用するシェーダーを指定する(3D or 2D)
	SetShaderBundle(SHADER_3D);

	return S_OK;
}

//使用するシェーダーを指定する(3D or 2D)
void Direct3D::SetShaderBundle(SHADER_TYPE type)
{
	//それぞれをデバイスコンテキストにセット(作成したものを使って描画できるようにデバイスコンテキストに情報を渡す)
	pContext->VSSetShader(shaderBundle[type].pVertexShader, NULL, 0);	//頂点シェーダー
	pContext->PSSetShader(shaderBundle[type].pPixelShader, NULL, 0);	//ピクセルシェーダー
	pContext->IASetInputLayout(shaderBundle[type].pVertexLayout);		//頂点インプットレイアウト
	pContext->RSSetState(shaderBundle[type].pRasterizerState);			//ラスタライザー
}

//2D用のシェーダー
HRESULT Direct3D::InitShader2D()
{
	////////////////////////頂点シェーダーの作成(コンパイルする処理)・頂点インプットレイアウトの作成////////////////////////

	//専用の変数を用意
	ID3DBlob *pCompileVS = nullptr;

	//「Simple3D.hlsl」というファイルの中の「VS」という関数を「バージョン5」でコンパイル
	if (FAILED(D3DCompileFromFile(L"Simple2D.hlsl", nullptr, nullptr, "VS", "vs_5_0", NULL, 0, &pCompileVS, NULL)))
	{
		MessageBox(nullptr, "頂点シェーダーコンパイル失敗", "エラー", MB_OK);
		return E_FAIL;
	}

	//失敗していないかどうか
	assert(pCompileVS != nullptr);

	//それをつかって頂点シェーダーを作成
	if (FAILED(pDevice->CreateVertexShader(pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), NULL, &shaderBundle[SHADER_2D].pVertexShader)))
	{
		MessageBox(nullptr, "頂点シェーダー作成失敗", "エラー", MB_OK);
		return E_FAIL;
	}

	/////////////////頂点インプットレイアウトを作成/////////////////

	//頂点に持たせたい情報を設定
	D3D11_INPUT_ELEMENT_DESC layout[] = {
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,  D3D11_INPUT_PER_VERTEX_DATA, 0 },						//頂点の位置
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, sizeof(XMVECTOR) , D3D11_INPUT_PER_VERTEX_DATA, 0 },			//UV座標
	};

	//それをつかって頂点インプットレイアウトを作成
	if (FAILED(pDevice->CreateInputLayout(layout, 2, pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), &shaderBundle[SHADER_2D].pVertexLayout)))
	{
		MessageBox(nullptr, "頂点インプットレイアウト作成失敗", "エラー", MB_OK);
		return E_FAIL;
	}


	//終わったらコンパイルしたデータはいらないので削除する(頂点シェーダー・頂点インプットレイアウトを作成するのが目的)
	SAFE_RELEASE(pCompileVS);


	///////////////////////////////////ピクセルシェーダーの作成(コンパイルする処理)///////////////////////////////////

	//専用の変数を用意
	ID3DBlob *pCompilePS = nullptr;

	//「Simple3D.hlsl」というファイルの中の「VS」という関数を「バージョン5」でコンパイル
	if (FAILED(D3DCompileFromFile(L"Simple2D.hlsl", nullptr, nullptr, "PS", "ps_5_0", NULL, 0, &pCompilePS, NULL)))
	{
		MessageBox(nullptr, "ピクセルシェーダーコンパイル失敗", "エラー", MB_OK);
		return E_FAIL;
	}

	//失敗していないかどうか
	assert(pCompilePS != nullptr);

	//それをつかってピクセルシェーダーを作成
	if (FAILED(pDevice->CreatePixelShader(pCompilePS->GetBufferPointer(), pCompilePS->GetBufferSize(), NULL, &shaderBundle[SHADER_2D].pPixelShader)))
	{
		MessageBox(nullptr, "ピクセルシェーダー作成失敗", "エラー", MB_OK);
		return E_FAIL;
	}

	//終わったらコンパイルしたデータはいらないので削除する(ピクセルシェーダーを作成するのが目的)
	SAFE_RELEASE(pCompilePS);


	//////////////////////////////////////////////////ラスタライザの作成//////////////////////////////////////////////////

	//専用の変数を用意
	D3D11_RASTERIZER_DESC rdc = {};

	//ポリゴンの表面だけ描画するか、両面描画するかなどを設定する
	rdc.CullMode = D3D11_CULL_BACK;

	//ポリゴンを面として描画するか、線だけ（ワイヤーフレーム）描画するかなどを設定する
	rdc.FillMode = D3D11_FILL_SOLID;

	//ポリゴンのどっちの面を「表側」とみなすかを設定する
	rdc.FrontCounterClockwise = FALSE;

	//それをつかってラスタライザを作成
	if (FAILED(pDevice->CreateRasterizerState(&rdc, &shaderBundle[SHADER_2D].pRasterizerState)))
	{
		MessageBox(nullptr, "ラスタライザ作成失敗", "エラー", MB_OK);
		return E_FAIL;
	}

	return S_OK;
}

//3D用のシェーダー
HRESULT Direct3D::InitShader3D()
{
	////////////////////////頂点シェーダーの作成(コンパイルする処理)・頂点インプットレイアウトの作成////////////////////////

	//専用の変数を用意
	ID3DBlob *pCompileVS = nullptr;

	//「Simple3D.hlsl」というファイルの中の「VS」という関数を「バージョン5」でコンパイル
	if (FAILED(D3DCompileFromFile(L"Simple3D.hlsl", nullptr, nullptr, "VS", "vs_5_0", NULL, 0, &pCompileVS, NULL)))
	{
		MessageBox(nullptr, "頂点シェーダーコンパイル失敗", "エラー", MB_OK);
		return E_FAIL;
	}

	//失敗していないかどうか
	assert(pCompileVS != nullptr);

	//それをつかって頂点シェーダーを作成
	if (FAILED(pDevice->CreateVertexShader(pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), NULL, &shaderBundle[SHADER_3D].pVertexShader)))
	{
		MessageBox(nullptr, "頂点シェーダー作成失敗", "エラー", MB_OK);
		return E_FAIL;
	}

	/////////////////頂点インプットレイアウトを作成/////////////////

	//頂点に持たせたい情報を設定
	D3D11_INPUT_ELEMENT_DESC layout[] = {
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,  D3D11_INPUT_PER_VERTEX_DATA, 0 },						//頂点の位置
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, sizeof(XMVECTOR) , D3D11_INPUT_PER_VERTEX_DATA, 0 },			//UV座標
		{ "NORMAL",	0, DXGI_FORMAT_R32G32B32_FLOAT, 0, sizeof(XMVECTOR) * 2 ,	D3D11_INPUT_PER_VERTEX_DATA, 0 },	//法線
	};

	//それをつかって頂点インプットレイアウトを作成
	if (FAILED(pDevice->CreateInputLayout(layout, 3, pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), &shaderBundle[SHADER_3D].pVertexLayout)))
	{
		MessageBox(nullptr, "頂点インプットレイアウト作成失敗", "エラー", MB_OK);
		return E_FAIL;
	}

	//終わったらコンパイルしたデータはいらないので削除する(頂点シェーダー・頂点インプットレイアウトを作成するのが目的)
	SAFE_RELEASE(pCompileVS);


	///////////////////////////////////ピクセルシェーダーの作成(コンパイルする処理)///////////////////////////////////

	//専用の変数を用意
	ID3DBlob *pCompilePS = nullptr;

	//「Simple3D.hlsl」というファイルの中の「VS」という関数を「バージョン5」でコンパイル
	if (FAILED(D3DCompileFromFile(L"Simple3D.hlsl", nullptr, nullptr, "PS", "ps_5_0", NULL, 0, &pCompilePS, NULL)))
	{
		MessageBox(nullptr, "ピクセルシェーダーコンパイル失敗", "エラー", MB_OK);
		return E_FAIL;
	}

	//失敗していないかどうか
	assert(pCompilePS != nullptr);

	//それをつかってピクセルシェーダーを作成
	if (FAILED(pDevice->CreatePixelShader(pCompilePS->GetBufferPointer(), pCompilePS->GetBufferSize(), NULL, &shaderBundle[SHADER_3D].pPixelShader)))
	{
		MessageBox(nullptr, "ピクセルシェーダー作成失敗", "エラー", MB_OK);
		return E_FAIL;
	}

	//終わったらコンパイルしたデータはいらないので削除する(ピクセルシェーダーを作成するのが目的)
	SAFE_RELEASE(pCompilePS);


	//////////////////////////////////////////////////ラスタライザの作成//////////////////////////////////////////////////

	//専用の変数を用意
	D3D11_RASTERIZER_DESC rdc = {};

	//ポリゴンの表面だけ描画するか、両面描画するかなどを設定する
	rdc.CullMode = D3D11_CULL_BACK;

	//ポリゴンを面として描画するか、線だけ（ワイヤーフレーム）描画するかなどを設定する
	rdc.FillMode = D3D11_FILL_SOLID;

	//ポリゴンのどっちの面を「表側」とみなすかを設定する
	rdc.FrontCounterClockwise = FALSE;

	//それをつかってラスタライザを作成
	if (FAILED(pDevice->CreateRasterizerState(&rdc, &shaderBundle[SHADER_3D].pRasterizerState)))
	{
		MessageBox(nullptr, "ラスタライザ作成失敗", "エラー", MB_OK);
		return E_FAIL;
	}

	return S_OK;
}

HRESULT Direct3D::InitShaderLine()
{
	////////////////////////頂点シェーダーの作成(コンパイルする処理)・頂点インプットレイアウトの作成////////////////////////

	//専用の変数を用意
	ID3DBlob *pCompileVS = nullptr;

	//「Simple3D.hlsl」というファイルの中の「VS」という関数を「バージョン5」でコンパイル
	if (FAILED(D3DCompileFromFile(L"Line.hlsl", nullptr, nullptr, "VS", "vs_5_0", NULL, 0, &pCompileVS, NULL)))
	{
		MessageBox(nullptr, "頂点シェーダーコンパイル失敗", "エラー", MB_OK);
		return E_FAIL;
	}

	//失敗していないかどうか
	assert(pCompileVS != nullptr);

	//それをつかって頂点シェーダーを作成
	if (FAILED(pDevice->CreateVertexShader(pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), NULL, &shaderBundle[SHADER_LINE].pVertexShader)))
	{
		MessageBox(nullptr, "頂点シェーダー作成失敗", "エラー", MB_OK);
		return E_FAIL;
	}

	/////////////////頂点インプットレイアウトを作成/////////////////

	//頂点に持たせたい情報を設定
	D3D11_INPUT_ELEMENT_DESC layout[] = {
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,  D3D11_INPUT_PER_VERTEX_DATA, 0 },						//頂点の位置
	};

	//それをつかって頂点インプットレイアウトを作成
	if (FAILED(pDevice->CreateInputLayout(layout, 1, pCompileVS->GetBufferPointer(), pCompileVS->GetBufferSize(), &shaderBundle[SHADER_LINE].pVertexLayout)))
	{
		MessageBox(nullptr, "頂点インプットレイアウト作成失敗", "エラー", MB_OK);
		return E_FAIL;
	}


	//終わったらコンパイルしたデータはいらないので削除する(頂点シェーダー・頂点インプットレイアウトを作成するのが目的)
	SAFE_RELEASE(pCompileVS);


	///////////////////////////////////ピクセルシェーダーの作成(コンパイルする処理)///////////////////////////////////

	//専用の変数を用意
	ID3DBlob *pCompilePS = nullptr;

	//「Simple3D.hlsl」というファイルの中の「VS」という関数を「バージョン5」でコンパイル
	if (FAILED(D3DCompileFromFile(L"Line.hlsl", nullptr, nullptr, "PS", "ps_5_0", NULL, 0, &pCompilePS, NULL)))
	{
		MessageBox(nullptr, "ピクセルシェーダーコンパイル失敗", "エラー", MB_OK);
		return E_FAIL;
	}

	//失敗していないかどうか
	assert(pCompilePS != nullptr);

	//それをつかってピクセルシェーダーを作成
	if (FAILED(pDevice->CreatePixelShader(pCompilePS->GetBufferPointer(), pCompilePS->GetBufferSize(), NULL, &shaderBundle[SHADER_LINE].pPixelShader)))
	{
		MessageBox(nullptr, "ピクセルシェーダー作成失敗", "エラー", MB_OK);
		return E_FAIL;
	}

	//終わったらコンパイルしたデータはいらないので削除する(ピクセルシェーダーを作成するのが目的)
	SAFE_RELEASE(pCompilePS);


	//////////////////////////////////////////////////ラスタライザの作成//////////////////////////////////////////////////

	//専用の変数を用意
	D3D11_RASTERIZER_DESC rdc = {};

	//ポリゴンの表面だけ描画するか、両面描画するかなどを設定する
	rdc.CullMode = D3D11_CULL_BACK;

	//ポリゴンを面として描画するか、線だけ（ワイヤーフレーム）描画するかなどを設定する
	rdc.FillMode = D3D11_FILL_WIREFRAME;

	//ポリゴンのどっちの面を「表側」とみなすかを設定する
	rdc.FrontCounterClockwise = FALSE;

	//それをつかってラスタライザを作成
	if (FAILED(pDevice->CreateRasterizerState(&rdc, &shaderBundle[SHADER_LINE].pRasterizerState)))
	{
		MessageBox(nullptr, "ラスタライザ作成失敗", "エラー", MB_OK);
		return E_FAIL;
	}

	return S_OK;
}

//描画開始
void Direct3D::BeginDraw()
{
	//背景の色
	float clearColor[4] = { 0.0f, 0.5f, 0.5f, 1.0f };//R,G,B,A


	//画面をクリア
	pContext->ClearRenderTargetView(pRenderTargetView, clearColor);


	//カメラ情報の更新
	Camera::Update();


	//深度バッファをクリア
	pContext->ClearDepthStencilView(pDepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);
}

//描画終了
HRESULT Direct3D::EndDraw()
{
	//スワップ（バックバッファを表に表示する）
	if (FAILED(pSwapChain->Present(0, 0)))
	{
		MessageBox(nullptr, "スワップチェイン失敗", "エラー", MB_OK);
		return E_FAIL;
	}
	return S_OK;
}

//解放処理
void Direct3D::Release()
{
	//作成したシェーダー数解放する
	for (int i = 0; i < SHADER_MAX; i++)
	{
		SAFE_RELEASE(shaderBundle[i].pRasterizerState);		//ラスタライザのポインタ変数
		SAFE_RELEASE(shaderBundle[i].pVertexLayout);		//頂点インプットレイアウトのポインタ変数
		SAFE_RELEASE(shaderBundle[i].pPixelShader);			//ピクセルシェーダーのポインタ変数
		SAFE_RELEASE(shaderBundle[i].pVertexShader);		//頂点シェーダーのポインタ変数
	}

	SAFE_RELEASE(pBlendState);			//ブレンドステートのポインタ変数
	SAFE_RELEASE(pDepthStencilView);	//深度ステンシルビューのポインタ変数
	SAFE_RELEASE(pDepthStencil);		//深度ステンシルのポインタ変数
	SAFE_RELEASE(pRenderTargetView);	//レンダーターゲットビューのポインタ変数
	SAFE_RELEASE(pSwapChain);			//スワップチェインのポインタ変数
	SAFE_RELEASE(pContext);				//デバイスコンテキストのポインタ変数
	SAFE_RELEASE(pDevice);				//デバイスのポインタ変数
}