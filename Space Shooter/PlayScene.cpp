#pragma once

#include "Engine/Model.h"
#include "Engine/Input.h"
#include "Engine/Splash.h"
#include "Engine/SceneManager.h"
#include "Engine/SceneTimer.h"
#include "Singleshot.h"
#include "MultiShot.h"
#include "Stage.h"
#include "Diffusion.h"
#include "Pointer.h"
#include "PlayScene.h"
#include "PlaySceneCamera.h"
#include "Durability.h"
#include "Obstacle.h"

//コンストラクタ
PlayScene::PlayScene(GameObject* parent)
	: GameObject(parent, "PlayScene"), drawFlg_(true), judgeFlg_(0), countObstacle_(0), countDrawFlg_(0), shotTypeFlg_(0), shotTime_(0),
	changeTime_(singleTime_)
{
	for (int i = 0; i < splashNum_; i++)
	{
		hSplash_[i] = -1;
	}
}

//初期化
void PlayScene::Initialize()
{
	//乱数生成の準備
	srand((unsigned int)time(NULL));

	//プレイシーンカメラの描画
	Instantiate<PlaySceneCamera>(this);

	//ステージの描画
	Instantiate<Stage>(this);

	//ポインターの描画
	Instantiate<Pointer>(this);

	//体力ゲージの描画
	Instantiate<Durability>(this);


	for (int i = 0; i < splashNum_; i++)
	{
		//画像データのロード
		hSplash_[i] = Splash::Load("Assets/Play_" + std::to_string(i + 1) + ".png");
		assert(hSplash_[i] >= 0);
	}
}

//更新
void PlayScene::Update()
{
	//ゲームプレイ中
	if (judgeFlg_ == 0)
	{
		//攻撃手段に単発を選択しているとき
		if (shotTypeFlg_ == single_)
		{
			changeTime_ = singleTime_;

			//左クリックした
			if (Input::IsMouseButtonDown(0))
			{
				//攻撃の描画
				Instantiate<SingleShot>(this);
			}
		}


		//攻撃手段に連発を選択しているとき
		if (shotTypeFlg_ == multi_)
		{
			shotTime_++;

			changeTime_ = multiTime_;

			//左クリックした
			if (Input::IsMouseButton(0))
			{
				//タイミングを調整
				//攻撃の描画
				if (shotTime_ >= multiShot_)
				{
					Instantiate<MultiShot>(this);
					shotTime_ = reset_;
				}
			}
		}

		//攻撃手段に拡散を選択しているとき
		if (shotTypeFlg_ == diff_)
		{
			changeTime_ = diffTime_;

			//左クリックした
			if (Input::IsMouseButtonDown(0))
			{
				//攻撃の描画
				Instantiation<Diffusion>(this, "Diffusion");
			}
		}

		static DWORD startTime_ = timeGetTime();
		DWORD nowTime_ = timeGetTime();


		if (nowTime_ - startTime_ >= changeTime_)
		{
			countObstacle_++;

			//障害物の描画
			Instantiate<Obstacle>(this);

			startTime_ = nowTime_;
		}

		//デルタタイムの更新
		SceneTimer::UpdateFrameDelta();

		//一定のタイミングになったとき
		if (countObstacle_ == victory_)
		{
			//画像描画フラグを切り替える(勝利)
			judgeFlg_ = 1;
		}

		//Durabilityクラス(体力ゲージを管理する)のポインタを検索して、変数に代入
		auto pDurability_ = (Durability*)GameObject::FindObject("Durability");

		//体力ゲージが残っていないとき
		if (pDurability_->GetRemaining() <= defeat_)
		{
			//画像描画フラグを切り替える(敗北)
			judgeFlg_ = 2;
			//ポインタ変数の解放
			pDurability_ = nullptr;
		}

		//右クリックした
		if (Input::IsMouseButtonDown(1))
		{
			//攻撃手段を変更する//

			if (shotTypeFlg_ == single_)
				shotTypeFlg_++;
			else
				if (shotTypeFlg_ == multi_)
					shotTypeFlg_++;
				else
					shotTypeFlg_ = single_;
		}
	}

	//勝利演出か敗北演出中
	if (judgeFlg_ > 0)
	{
		//画像を切り替えるタイミングを更新
		countDrawFlg_++;

		//一定のタイミングになったとき
		if (countDrawFlg_ > changeSplash_)
		{
			//フラグを交互に更新させる
			if (drawFlg_ == false)
			{
				//画像の描画フラグを切り替える
				drawFlg_ = true;
				//画像を切り替えるタイミングをリセット
				countDrawFlg_ = reset_;
			}
			else
			{
				//画像の描画フラグを切り替える
				drawFlg_ = false;
				//画像を切り替えるタイミングをリセット
				countDrawFlg_ = reset_;
			}
		}

		//左クリックした
		if (Input::IsMouseButtonDown(0))
		{
			//タイトル画面に遷移
			SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
			pSceneManager->ChangeScene(SCENE_ID_TITLE);
		}

		//右クリックした
		if (Input::IsMouseButtonDown(1))
		{
			//プログラム終了
			PostQuitMessage(0);
		}
	}
}

//描画
void PlayScene::Draw()
{
	//勝利演出画像を描画する場合
	if (judgeFlg_ == 1)
	{
		//フラグによって描画する画像を切り替えて点滅しているように見せる//

		if (drawFlg_ == true)		//選択肢無し画像
		{
			Splash::SetTransform(hSplash_[0], transform_);
			Splash::Draw(hSplash_[0]);
		}

		if (drawFlg_ == false)		//選択肢有り画像
		{
			Splash::SetTransform(hSplash_[1], transform_);
			Splash::Draw(hSplash_[1]);
		}
	}

	//敗北演出画像を描画する場合
	if (judgeFlg_ == 2)
	{
		//フラグによって描画する画像を切り替えて点滅しているように見せる//

		if (drawFlg_ == true)		//選択肢無し画像
		{
			Splash::SetTransform(hSplash_[2], transform_);
			Splash::Draw(hSplash_[2]);
		}

		if (drawFlg_ == false)		//選択肢有り画像
		{
			Splash::SetTransform(hSplash_[3], transform_);
			Splash::Draw(hSplash_[3]);
		}
	}
}

//開放
void PlayScene::Release()
{
}
