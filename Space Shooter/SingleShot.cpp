#pragma once

#include "Engine/Model.h"
#include "Engine/SphereCollider.h"
#include "Obstacle.h"
#include "SingleShot.h"

//コンストラクタ
SingleShot::SingleShot(GameObject* parent)
	:Attack(parent, "SingleShot")
{
	hModel_ = 0;

	data_.damage_ = 1.3f;		//ダメージ
	data_.size_ = 0.35f;		//サイズ
	data_.collisionSize_ = 0.4f;//当たり判定サイズ
	data_.speed_ = 1.5f;		//スピード
}

//デストラクタ
SingleShot::~SingleShot()
{
}

//初期化
void SingleShot::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Assets/Attack.fbx");
	assert(hModel_ >= 0);

	//ポインターの位置にビームが描画されるようにする
	transform_.position_ = pPointer_->GetPos();
	transform_.scale_ *= data_.size_;

	//弾に当たり判定を追加
	collision_ = new SphereCollider(XMVectorSet(0, 0, 0, 0), data_.collisionSize_);
	AddCollider(collision_);
}

//更新
void SingleShot::Update()
{
	//奥に飛んでいくようにする
	transform_.position_.vecZ += data_.speed_;

	//一定距離飛んでいったとき
	if (transform_.position_.vecZ > attackRangeZ_)
	{
		//弾を削除する
		KillMe();
	}
}

//描画
void SingleShot::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_, SHADER_3D);
}

//開放
void SingleShot::Release()
{
	//ポインタ変数の解放
	pObstacle_ = nullptr;
	pPointer_ = nullptr;
}

//何かに当たった
void SingleShot::OnCollision(GameObject* pTarget)
{
	//障害物に当たったとき
	if (pTarget->GetObjectName() == "Obstacle")
	{
		//耐久値を減少させる
		pObstacle_->HitAttack(data_.damage_);
		//弾を削除する
		KillMe();
	}
}