#pragma once

#include "Engine/GameObject.h"

#define STAGE_RIGHT_LOW (3)		//右壁の最小モデル番号
#define STAGE_RIGHT_UPPER (5)	//右壁の最大モデル番号
#define STAGE_LEFT_LOW (6)		//左壁の最小モデル番号
#define STAGE_LEFT_UPPER (8)	//左壁の最大モデル番号

//定数
const int stageNum_ = 13;			//ステージモデルの枚数
const float stageSize_ = 48.0f;		//ステージのサイズ
const float rotAngle_ = 90.0f;		//モデルの回転角度
const float moveSpeed_ = 0.5f;		//モデルの移動速度
const float drawPosZ_ = 95.6f;		//描画位置を設定
const XMVECTOR ajustPos_ = XMVectorSet(14.0f, 2.0f, 82.0f, 0);	//初期位置を調整

//ステージを管理するクラス
class Stage : public GameObject
{
	int hModel_[stageNum_];			//モデル番号
	Transform trans_[stageNum_];	//各モデルの位置を扱う

public:
	//コンストラクタ
	Stage(GameObject* parent);

	//デストラクタ
	~Stage();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};