#pragma once

#include "Attack.h"

//定数
const float diffusion_ = 0.1f;		//弾の拡散率

class Diffusion : public Attack
{
public:
	//コンストラクタ
	Diffusion(GameObject* parent, const std::string& name);

	//デストラクタ
	~Diffusion();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};

