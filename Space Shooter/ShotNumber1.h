#pragma once

#include "Diffusion.h"

class ShotNumber1 : public Diffusion
{

public:
	//コンストラクタ
	ShotNumber1(GameObject* parent);

	//デストラクタ
	~ShotNumber1();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//何かに当たった
	//引数：pTarget 当たった相手
	void OnCollision(GameObject* pTarget) override;
};

