#pragma once

#include "Diffusion.h"

class ShotNumber8 : public Diffusion
{

public:
	//コンストラクタ
	ShotNumber8(GameObject* parent);

	//デストラクタ
	~ShotNumber8();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//何かに当たった
	//引数：pTarget 当たった相手
	void OnCollision(GameObject* pTarget) override;
};

