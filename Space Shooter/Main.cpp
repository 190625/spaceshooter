#pragma once

#include <Windows.h>
#include <dInput.h>
#include <stdlib.h>
#include "Engine/Camera.h"
#include "Engine/Input.h"
#include "Engine/RootJob.h"
#include "Engine/SceneTimer.h"

#pragma comment(lib, "winmm.lib")


//定数宣言
const char* WIN_CLASS_NAME = "Space Shooter";				//ウィンドウクラス名
const int WINDOW_WIDTH = 800;								//ウィンドウの幅(クライアント領域)
const int WINDOW_HEIGHT = 600;								//ウィンドウの高さ(クライアント領域)
const int DISPLAY_WIDTH = GetSystemMetrics(SM_CXSCREEN);	//ディスプレイの幅
const int DISPLAY_HEIGHT = GetSystemMetrics(SM_CYSCREEN);	//ディスプレイの高さ


//プロトタイプ宣言
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);


//エントリーポイント
//第一引数：インスタンスハンドル
//第二引数： Win16の産物
//第三引数：コマンドラインから受け取った引数
//第四引数：アプリケーションの初期表示方法を指定
int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInst, LPSTR lpCmdLine, int nCmdShow)
{
	//ウィンドウクラス（設計図）を作成
	WNDCLASSEX wc;
	wc.cbSize = sizeof(WNDCLASSEX);							//この構造体のサイズ
	wc.hInstance = hInstance;								//インスタンスハンドル
	wc.lpszClassName = WIN_CLASS_NAME;						//ウィンドウクラス名
	wc.lpfnWndProc = WndProc;								//ウィンドウプロシージャ
	wc.style = CS_VREDRAW | CS_HREDRAW;						//スタイル（デフォルト）
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);				//アイコン
	wc.hIconSm = LoadIcon(NULL, IDI_WINLOGO);				//小さいアイコン
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);				//マウスカーソル
	wc.lpszMenuName = NULL;									//メニュー（なし）
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);	//背景（白）
	RegisterClassEx(&wc);									//クラスを登録


	//ウィンドウサイズの計算
	RECT winRect = { 0, 0, WINDOW_WIDTH, WINDOW_HEIGHT };
	//第一引数：希望するサイズ(RECT構造体)
	//第二引数：ウィンドウスタイル
	//第三引数：メニューバーの有無
	AdjustWindowRect(&winRect, WS_OVERLAPPEDWINDOW, FALSE);
	int winW = winRect.right - winRect.left;				//ウィンドウ幅(ウィンドウサイズ)
	int winH = winRect.bottom - winRect.top;				//ウィンドウ高さ(ウィンドウサイズ)


	//ウィンドウを作成
	HWND hWnd = CreateWindow(
		WIN_CLASS_NAME,		//ウィンドウクラス名
		"Space Shooter",	//タイトルバーに表示する内容
		WS_OVERLAPPEDWINDOW, //スタイル（普通のウィンドウ）
		CW_USEDEFAULT,       //表示位置左（おまかせ）
		CW_USEDEFAULT,       //表示位置上（おまかせ）
		winW,				 //ウィンドウ幅(ウィンドウサイズ)
		winH,				 //ウィンドウ高さ(ウィンドウサイズ)
		NULL,                //親ウインドウ（なし）
		NULL,                //メニュー（なし）
		hInstance,           //インスタンス
		NULL                 //パラメータ（なし）
	);


	//ウィンドウを表示
	ShowWindow(hWnd, nCmdShow);

	////ウィンドウを表示(全画面表示)
	//ShowWindow(hWnd, SW_MAXIMIZE);


	//ウィンドウの位置をディスプレイの中心に設定
	SetWindowPos(hWnd, NULL, (DISPLAY_WIDTH - winW) / 2, (DISPLAY_HEIGHT - winH) / 2,
		winW, winH, SWP_SHOWWINDOW);


	//Direct3Dの初期化処理
	if (FAILED(Direct3D::Initialize(WINDOW_WIDTH, WINDOW_HEIGHT, hWnd)))
	{
		return 0;
	}


	//DirectInputの初期化処理
	if (FAILED(Input::Initialize(hWnd)))
	{
		return 0;
	}

	//マウスカーソルの非表示
	ShowCursor(false);

	//RootJobのインスタンス作成
	RootJob* pRootJob = new RootJob;
	pRootJob->Initialize();

	


	//メッセージループ（何か起きるのを待つ）
	MSG msg;
	//メモリのブロックをゼロで埋める
	ZeroMemory(&msg, sizeof(msg));
	while (msg.message != WM_QUIT)
	{
		//メッセージあり
		if (PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		//メッセージなし
		else
		{
			timeBeginPeriod(1);		//時間計測の精度を上げる

			///////////////////////////時間の計測///////////////////////////
			// 
			//画面更新回数を記憶する変数
			static DWORD countFps = 0;

			//Windowsが起動してからのミリ秒を取得
			static DWORD startTime = timeGetTime();		//プログラム起動時の時間を記憶する変数(スタート時間)
			DWORD nowTime = timeGetTime();				//プログラム実行中常に時間を更新する変数(スタートからの経過時間)

			//最後に画面更新した時間を記憶する変数
			static DWORD lastUpdateTime = nowTime;

			//1秒経過したら更新回数とスタート時間の更新
			if (nowTime - startTime >= 1000)
			{
				//表示するテキスト用の変数
				char str[16];
				//指定したデータを書式制御文字列にしたがってフォーマットする
				wsprintf(str, "%u", countFps);

				//指定されたウィンドウのタイトルバーのテキスト変更
				SetWindowText(hWnd, str);

				countFps = 0;
				startTime = nowTime;
			}

			//最後に画面を更新してから1/60秒(1000/60ミリ秒)経過していないなら更新しない
			if (nowTime - lastUpdateTime <= 1000.0f / 60)
			{
				continue;
			}

			//最後に画面更新した時間を更新
			lastUpdateTime = nowTime;

			//更新回数を更新
			countFps++;


			//デルタタイムの更新
			SceneTimer::UpdateFrameDelta();


			//入力更新処理
			if (FAILED(Input::Update()))
			{
				return 0;
			}


			//RootJobの更新処理
			pRootJob->UpdateSub();


			/////////////////////////////////ゲームの処理/////////////////////////////////
			//Direct3Dの描画開始処理
			Direct3D::BeginDraw();

			static bool test = true;
			test ^= Input::IsKeyDown(DIK_ESCAPE);
			if (test)
			{
				//ウィンドウの位置取得
				RECT lprc;
				GetWindowRect(hWnd, &lprc);

				//マウスカーソルをセットする位置
				POINT setMoucePos;

				//x(ウィンドウの中心)
				setMoucePos.x = lprc.left + winW / 2;

				//y(ウィンドウの中心)
				setMoucePos.y = lprc.top + winH / 2;

				//マウスカーソルの位置設定
				SetCursorPos(setMoucePos.x, setMoucePos.y);
			}


			//描画処理
			pRootJob->DrawSub();


			//Direct3Dの描画終了処理
			if (FAILED(Direct3D::EndDraw()))
			{
				return 0;
			}


			Sleep(1);				//ちょっと休ませる


			timeEndPeriod(1);		//時間計測の精度を戻す
		}
	}

	//RootJobの解放処理
	pRootJob->ReleaseSub();
	SAFE_DELETE(pRootJob);
	//DirectInputの解放処理
	Input::Release();
	//Direct3Dの解放処理
	Direct3D::Release();

	return 0;
}


//ウィンドウプロシージャ（何かあった時によばれる関数）
//第一引数：ウィンドウハンドル
//第二引数：何が起きたか
//第三引数：何かしらの情報?
//第四引数：何かしらの情報?
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{

	//何が起きたか
	switch (msg)
	{

		//マウスが移動したとき
	case WM_MOUSEMOVE:
		// lParam の下位ビットにX座標、上位ビットにY座標が入っている
		Input::SetMousePosition(LOWORD(lParam), HIWORD(lParam));
		return 0;


		//ウィンドウの×ボタンが押された場合
	case WM_DESTROY:
		PostQuitMessage(0);  //プログラム終了
		return 0;
	}

	//それ以外(デフォルトの設定)
	return DefWindowProc(hWnd, msg, wParam, lParam);
}