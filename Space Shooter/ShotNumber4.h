#pragma once

#include "Diffusion.h"

class ShotNumber4 : public Diffusion
{

public:
	//コンストラクタ
	ShotNumber4(GameObject* parent);

	//デストラクタ
	~ShotNumber4();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//何かに当たった
	//引数：pTarget 当たった相手
	void OnCollision(GameObject* pTarget) override;
};

