#pragma once

#include "Engine/Model.h"
#include "Stage.h"

//コンストラクタ
Stage::Stage(GameObject* parent)
	:GameObject(parent, "Stage")
{
	for (int i = 0; i < stageNum_; i++)
	{
		hModel_[i] = -1;
	}
}

//デストラクタ
Stage::~Stage()
{
}

//初期化
void Stage::Initialize()
{

	//モデルデータのロード
	for (int i = 0; i < stageNum_; i++)
	{
		hModel_[i] = Model::Load("Assets/Stage.fbx");
		assert(hModel_[i] >= 0);
	}

	//------------------------各モデルの位置調整------------------------
	trans_[0].position_ = XMVectorSet(0, -(stageSize_ / 4), 0, 0);
	trans_[1].position_ = XMVectorSet(0, -(stageSize_ / 4), stageSize_, 0);
	trans_[2].position_ = XMVectorSet(0, -(stageSize_ / 4), stageSize_ * 2, 0);

	trans_[3].position_ = XMVectorSet(ajustPos_.vecX, ajustPos_.vecY, 0, 0);
	trans_[4].position_ = XMVectorSet(ajustPos_.vecX, ajustPos_.vecY, stageSize_, 0);
	trans_[5].position_ = XMVectorSet(ajustPos_.vecX, ajustPos_.vecY, stageSize_ * 2, 0);

	trans_[6].position_ = XMVectorSet(-ajustPos_.vecX, ajustPos_.vecY, 0, 0);
	trans_[7].position_ = XMVectorSet(-ajustPos_.vecX, ajustPos_.vecY, stageSize_, 0);
	trans_[8].position_ = XMVectorSet(-ajustPos_.vecX, ajustPos_.vecY, stageSize_ * 2, 0);

	trans_[9].position_ = XMVectorSet(0, stageSize_ * 0.75f, 0, 0);
	trans_[10].position_ = XMVectorSet(0, stageSize_ * 0.75f, stageSize_, 0);
	trans_[11].position_ = XMVectorSet(0, stageSize_ * 0.75f, stageSize_ * 2, 0);


	trans_[12].rotate_.vecX = XMConvertToRadians(rotAngle_);
	trans_[12].position_ = XMVectorSet(0, ajustPos_.vecY, ajustPos_.vecZ, 0);

	for (int i = 0; i < stageNum_; i++)
	{
		if (STAGE_RIGHT_LOW <= i && i <= STAGE_RIGHT_UPPER)
			trans_[i].rotate_.vecZ = XMConvertToRadians(rotAngle_);

		if(STAGE_LEFT_LOW <= i && i <= STAGE_LEFT_UPPER)
			trans_[i].rotate_.vecZ = XMConvertToRadians(-rotAngle_);

		trans_[i].scale_.vecX *= 2.0f;
		trans_[i].scale_.vecZ *= 2.0f;
		trans_[i].Calclation();
	}
}

//更新
void Stage::Update()
{
	//---------------モデルの移動---------------
	for (int i = 0; i < stageNum_ - 1; i++)
	{
		trans_[i].position_.vecZ -= moveSpeed_;
		if (trans_[i].position_.vecZ < -stageSize_)
		{
			trans_[i].position_.vecZ = drawPosZ_;
		}
		trans_[i].Calclation();
	}
}

//描画
void Stage::Draw()
{
	for (int m = 0; m < stageNum_; m++)
	{
		Model::SetTransform(hModel_[m], trans_[m]);
		Model::Draw(hModel_[m], SHADER_3D);
	}
}

//開放
void Stage::Release()
{
}