#pragma once

#include "Diffusion.h"

class ShotNumber9 : public Diffusion
{

public:
	//コンストラクタ
	ShotNumber9(GameObject* parent);

	//デストラクタ
	~ShotNumber9();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//何かに当たった
	//引数：pTarget 当たった相手
	void OnCollision(GameObject* pTarget) override;
};

