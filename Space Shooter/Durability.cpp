#pragma once

#include "Pointer.h"
#include "Durability.h"
#include "Engine/Model.h"
#include "Engine/Input.h"
#include "Engine/Camera.h"

//コンストラクタ
Durability::Durability(GameObject* parent)
	:GameObject(parent, "Durability"), remaining_(5.0f), remainingJudge_(0),ajustPosX_(0.0f)
{
	for (int i = 0; i < durabillityNum_; i++)
	{
		hModel_[i] = 0;
	}
}

//デストラクタ
Durability::~Durability()
{
}

//初期化
void Durability::Initialize()
{
	for (int i = 0; i < durabillityNum_; i++)
	{
		//モデルデータのロード
		hModel_[i] = Model::Load("Assets/Durabillity.fbx");
		assert(hModel_[i] >= 0);

		//各モデルの位置を調整
		trans_[i].position_.vecX = durabillityX_ + ajustPosX_;
		trans_[i].position_.vecY = durabillityY_;
		trans_[i].position_.vecZ = durabillityZ_;
	/*	trans_[i].scale_.vecX *= 2;
		trans_[i].scale_.vecY *= 2;*/
		trans_[i].Calclation();

		//位置調整値の更新
		ajustPosX_ += addPosX_;
	}
}

//更新
void Durability::Update()
{
	if (Input::GetMouseMove().vecX < 0)
		if (transform_.position_.vecX + pointerMove_ < stageSideRight_)
			transform_.position_.vecX += pointerMove_;

	if (Input::GetMouseMove().vecX > 0)
		if (transform_.position_.vecX - pointerMove_ > stageSideLeft_)
			transform_.position_.vecX -= pointerMove_;

	if (Input::GetMouseMove().vecY > 0)
		if (transform_.position_.vecY + pointerMove_ < stageSideRight_)
			transform_.position_.vecY += pointerMove_;

	if (Input::GetMouseMove().vecY < 0)
		if (transform_.position_.vecY - pointerMove_ > stageSideLeft_)
			transform_.position_.vecY -= pointerMove_;

	if (Input::IsKeyDown(DIK_SPACE))
	{
	}
}

//描画
void Durability::Draw()
{
	for (int i = 0; i < durabillityNum_; i++)
	{
		//表示できるかどうか判定
		if (remaining_ >= remainingJudge_)
		{
			Model::SetTransform(hModel_[i], trans_[i]);
			Model::Draw(hModel_[i], SHADER_3D);
		}

		//判定の値を更新する
		remainingJudge_ += drawJudge_;
	}

	//判定の値をリセットする
	remainingJudge_ = drawJudge_;
}

//開放
void Durability::Release()
{
}

//障害物を撃ち落とせなかったとき
void Durability::HitObstacle(float damage)
{
	//体力を減少させる
	remaining_ -= damage;
}
