#pragma once

#include "Engine/Model.h"
#include "Engine/Input.h"
#include "Pointer.h"

//コンストラクタ
Pointer::Pointer(GameObject* parent)
	:GameObject(parent, "Pointer")
{
	hModel_ = 0;
	pCam_ = nullptr;
	flag_ = true;
}

//デストラクタ
Pointer::~Pointer()
{
}

//初期化
void Pointer::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Assets/Pointer_2.fbx");
	assert(hModel_ >= 0);
}

//更新
void Pointer::Update()
{
	//一回だけ行いたい処理
	if (flag_)
	{
		pCam_ = (PlaySceneCamera*)GameObject::FindObject("PlaySceneCamera");
		transform_.position_ = pCam_->GetPos();
		transform_.position_.vecZ += 6.0f;
		flag_ = false;
	}

	if (Input::GetMouseMove().vecX < 0)
		if (transform_.position_.vecX + pointerMove_ < stageSideRight_)
			transform_.position_.vecX += pointerMove_;

	if (Input::GetMouseMove().vecX > 0)
		if (transform_.position_.vecX - pointerMove_ > stageSideLeft_)
			transform_.position_.vecX -= pointerMove_;

	if (Input::GetMouseMove().vecY > 0)
		if (transform_.position_.vecY + pointerMove_ < stageSideRight_)
			transform_.position_.vecY += pointerMove_;

	if (Input::GetMouseMove().vecY < 0)
		if (transform_.position_.vecY - pointerMove_ > stageSideLeft_)
			transform_.position_.vecY -= pointerMove_;

	if (Input::IsKeyDown(DIK_SPACE))
	{
		transform_.position_ = pCam_->GetPos();
		transform_.position_.vecZ += 6.0f;
	}
}

//描画
void Pointer::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_, SHADER_3D);
}

//開放
void Pointer::Release()
{
}