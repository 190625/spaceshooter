#pragma once

#include "Diffusion.h"

class ShotNumber2 : public Diffusion
{

public:
	//コンストラクタ
	ShotNumber2(GameObject* parent);

	//デストラクタ
	~ShotNumber2();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//何かに当たった
	//引数：pTarget 当たった相手
	void OnCollision(GameObject* pTarget) override;
};

