#pragma once

#include "Engine/GameObject.h"
#include "Pointer.h"
#include "Obstacle.h"

//定数
const float attackRangeZ_ = 24.0f;		//弾が飛ぶ距離

//攻撃手段を管理するクラス
class Attack : public GameObject
{
protected:
	int hModel_;				//モデル番号

	struct attackData
	{
		float damage_;			//ダメージ
		float size_;			//サイズ
		float collisionSize_;	//当たり判定のサイズ
		float speed_;			//スピード
	};

	attackData data_;			//弾に関する構造体

public:
	//コンストラクタ
	Attack(GameObject* parent, const std::string& name);

	//デストラクタ
	~Attack();

	//当たり判定を扱う
	SphereCollider* collision_ = nullptr;

	//クラスのポインタを検索して、変数に代入
	Pointer* pPointer_ = (Pointer*)GameObject::FindObject("Pointer");
	Obstacle* pObstacle_ = (Obstacle*)GameObject::FindObject("Obstacle");

	//純粋仮想関数
	virtual void Initialize() = 0;
	virtual void Update() = 0;
	virtual void Draw() = 0;
	virtual void Release() = 0;
};