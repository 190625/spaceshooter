#pragma once

#include "Engine/Model.h"
#include "Engine/SphereCollider.h"
#include "Obstacle.h"
#include "MultiShot.h"

//コンストラクタ
MultiShot::MultiShot(GameObject* parent)
	:Attack(parent, "MultiShot")
{
	hModel_ = 0;

	data_.damage_ = 0.7f;
	data_.size_ = 0.3f;
	data_.collisionSize_ = 0.5f;
	data_.speed_ = 1.8f;
}

//デストラクタ
MultiShot::~MultiShot()
{
}

//初期化
void MultiShot::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Assets/Attack.fbx");
	assert(hModel_ >= 0);

	//ポインターの位置に弾が描画されるようにする
	transform_.position_ = pPointer_->GetPos();
	transform_.scale_ *= data_.size_;

	//弾に当たり判定を追加
	collision_ = new SphereCollider(XMVectorSet(0, 0, 0, 0), data_.collisionSize_);
	AddCollider(collision_);
}

//更新
void MultiShot::Update()
{
	//奥に飛んでいくようにする
	transform_.position_.vecZ += data_.speed_;

	//一定距離飛んでいったとき
	if (transform_.position_.vecZ > attackRangeZ_)
	{
		//弾を削除する
		KillMe();
	}
}

//描画
void MultiShot::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_, SHADER_3D);
}

//開放
void MultiShot::Release()
{
	//ポインタ変数の解放
	pObstacle_ = nullptr;
	pPointer_ = nullptr;
}

//何かに当たった
void MultiShot::OnCollision(GameObject* pTarget)
{
	//障害物に当たったとき
	if (pTarget->GetObjectName() == "Obstacle")
	{
		//耐久値を減少させる
		pObstacle_->HitAttack(data_.damage_);
		//弾を削除する
		KillMe();
	}
}