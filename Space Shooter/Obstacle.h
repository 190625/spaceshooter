#pragma once

#include "Engine/GameObject.h"
#include "Durability.h"

//定数
const float posZ_ = 80.0f;              //生成位置(Z)
const float ObstacleRnageZ_ = -24.0f;   //障害物が消える距離
const int randRange_ = 2;               //乱数生成範囲
const int addRange_ = 1;                //乱数生成範囲の拡大
const float ajustCollision_ = 0.3f;     //当たり判定範囲の調整
const float ajustSpeed_ = 0.2f;         //速度の調整
const float ajustDamage_ = 0.5f;        //ダメージの調整
const int randPosUpX_ = 2;              //障害物生成上限値(X)
const int randPosLowX_ = 1;             //障害物生成下限値(X)
const int randPosUpY_ = 15;             //障害物生成上限値(Y)
const int randPosLowY_ = 5;             //障害物生成下限値(Y)

//障害物を管理するクラス
class Obstacle : public GameObject
{

    int hModel_;                    //モデル番号
    SphereCollider* collision_;     //当たり判定を扱う
    Durability* pDurability_;       //Durabilityを扱う
    float randam_;                  //乱数(生成する障害物にかかわる)範囲：1〜3
    XMVECTOR move_;                 //障害物の移動量

    struct ObstacleData
    {
        float defence_;			    //障害物の耐久力
        float damage_;			    //障害物の攻撃力
        float speed_;			    //障害物の速度
        float size_;			    //障害物のサイズ
        float collisionSize_;	    //当たり判定のサイズ
        int posX_;                  //初期位置(X)範囲：-7〜6
        int posY_;			        //初期位置(Y)範囲：-6〜6
    };
    ObstacleData data_;			    //障害物のデータをまとめた構造体

public:
    //コンストラクタ
    Obstacle(GameObject* parent);

    //デストラクタ
    ~Obstacle();

    //初期化
    void Initialize() override;

    //更新
    void Update() override;

    //描画
    void Draw() override;

    //開放
    void Release() override;

    //攻撃が当たったとき
    //引数：当たった攻撃手段の攻撃力
    void HitAttack(float damage);
};