#pragma once

#include "Diffusion.h"

class ShotNumber3 : public Diffusion
{

public:
	//コンストラクタ
	ShotNumber3(GameObject* parent);

	//デストラクタ
	~ShotNumber3();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//何かに当たった
	//引数：pTarget 当たった相手
	void OnCollision(GameObject* pTarget) override;
};

