#pragma once

#include "Engine/GameObject.h"
#include "PlaySceneCamera.h"

const float pointerMove_ = 0.26f;	//ポインターの移動量


//ポインターを管理するクラス
class Pointer : public GameObject
{
	int hModel_;				//モデル番号
	PlaySceneCamera* pCam_;		//プレイシーンカメラの変数
	bool flag_;					//フラグ

public:
	//コンストラクタ
	Pointer(GameObject* parent);

	//デストラクタ
	~Pointer();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//ポインターの位置を渡す
	XMVECTOR GetPos() { return transform_.position_; };

};