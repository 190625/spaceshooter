#pragma once

#include "PlaySceneCamera.h"
#include "Engine/Input.h"

//コンストラクタ
PlaySceneCamera::PlaySceneCamera(GameObject* parent)
    :GameObject(parent, "PlaySceneCamera")
{
    position_ = XMVectorZero();
    target_ = XMVectorZero();
    rotate_ = XMVectorZero();
    mX_ = XMMatrixIdentity();
    mY_ = XMMatrixIdentity();
}

//デストラクタ
PlaySceneCamera::~PlaySceneCamera()
{
}

//初期化
void PlaySceneCamera::Initialize()
{
    position_ = XMVectorSet(0, camPosY_, camPosZ_, 0);
    Camera::SetPosition(position_);
    target_ = XMVectorSet(0, camPosY_, camTarZ_, 0);
    Camera::SetTarget(target_);
}

//更新
void PlaySceneCamera::Update()
{
   rotate_.vecX = Input::GetMouseMove().vecY * mauseRate_;
   rotate_.vecY = Input::GetMouseMove().vecX * mauseRate_;

   mX_ = XMMatrixRotationX(XMConvertToRadians(XMConvertToRadians(rotate_.vecX)));
   mY_ = XMMatrixRotationY(XMConvertToRadians(XMConvertToRadians(rotate_.vecY)));
      
   target_ = XMVector3TransformCoord(target_, mX_);
   target_ = XMVector3TransformCoord(target_, mY_);

   if (Input::IsKeyDown(DIK_SPACE))
   {
       target_ = XMVectorSet(0, camPosY_, camTarZ_, 0);
   }

   Camera::SetTarget(target_);
}

//描画
void PlaySceneCamera::Draw()
{
}

//開放
void PlaySceneCamera::Release()
{
}
