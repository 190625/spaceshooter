#pragma once

#include "Engine/GameObject.h"
#include "Pointer.h"

//定数
const int splashNum_ = 4;		//画像数
const int singleTime_ = 1400;	//障害物発生タイミング(単発モード)
const int multiTime_ = 1300;	//障害物発生タイミング(連発モード)
const int diffTime_ = 1200;		//障害物発生タイミング(拡散モード)
const int multiShot_ = 5;		//発射タイミング(連発モード)
const int victory_ = 50;		//勝利条件(発生障害物数)
const float defeat_ = 0.5f;		//敗北条件(残体力ゲージ)
const int changeSplash_ = 40;	//画像の切り替えタイミング
const int single_ = 0;			//単発モード
const int multi_ = 1;			//連発モード
const int diff_ = 2;			//拡散モード

//プレイシーンを管理するクラス
class PlayScene : public GameObject
{
	int hSplash_[splashNum_];	//画像番号
	bool drawFlg_;				//画像の切り替えを指定
	int judgeFlg_;				//勝利・敗北モードを指定
	int countDrawFlg_;			//画像切り替えタイミングをカウント
	int countObstacle_;			//障害物数をカウント
	int shotTypeFlg_;			//攻撃モードを選択
	int shotTime_;				//発射タイミング(連発モード)
	int changeTime_;			//障害物発生タイミング

	XMVECTOR nowDirCam_;		//今カメラが向いている方向
	XMVECTOR prevDirCam_;		//前カメラが向いていた方向
	XMVECTOR rotatecam_;		//カメラの回転角度を求める

	Pointer* pPointer;

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	PlayScene(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};
