#pragma once

#include "ShotNumber7.h"
#include "Engine/Model.h"
#include "Engine/SphereCollider.h"

ShotNumber7::ShotNumber7(GameObject* parent)
	:Diffusion(parent, "ShotNumber7")
{
}

ShotNumber7::~ShotNumber7()
{
}

void ShotNumber7::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Assets/Attack.fbx");
	assert(hModel_ >= 0);

	//ポインターの位置に弾が描画されるようにする
	transform_.position_ = pPointer_->GetPos();
	transform_.scale_ *= data_.size_;

	//弾に当たり判定を追加
	collision_ = new SphereCollider(XMVectorSet(0, 0, 0, 0), data_.collisionSize_);
	AddCollider(collision_);
}

void ShotNumber7::Update()
{
	transform_.position_.vecX += diffusion_;
	transform_.position_.vecY -= diffusion_;
}

void ShotNumber7::OnCollision(GameObject* pTarget)
{
	//障害物に当たったとき
	if (pTarget->GetObjectName() == "Obstacle")
	{
		//耐久値を減少させる
		pObstacle_->HitAttack(data_.damage_);
		//ビームモデルを削除する
		KillMe();
	}
}
