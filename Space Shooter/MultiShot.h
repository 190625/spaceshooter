#pragma once

#include "Attack.h"

//連発を管理するクラス
class MultiShot : public Attack
{
public:
	//コンストラクタ
	MultiShot(GameObject* parent);

	//デストラクタ
	~MultiShot();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//何かに当たった
	//引数：pTarget 当たった相手
	void OnCollision(GameObject* pTarget) override;
};