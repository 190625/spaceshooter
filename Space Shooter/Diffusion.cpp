#pragma once

#include "Diffusion.h"
#include "Engine/Model.h"
#include "Engine/SphereCollider.h"
#include "ShotNumber1.h"
#include "ShotNumber2.h"
#include "ShotNumber3.h"
#include "ShotNumber4.h"
#include "ShotNumber5.h"
#include "ShotNumber6.h"
#include "ShotNumber7.h"
#include "ShotNumber8.h"
#include "ShotNumber9.h"
#include "Obstacle.h"

Diffusion::Diffusion(GameObject* parent, const std::string& name)
	:Attack(parent, name)
{
	hModel_ = 0;
	data_.damage_ = 0.34f;
	data_.size_ = 0.25f;
	data_.collisionSize_ = 0.3f;
	data_.speed_ = 1.65f;
}

Diffusion::~Diffusion()
{
}

void Diffusion::Initialize()
{
	Instantiate<ShotNumber1>(this);
	Instantiate<ShotNumber2>(this);
	Instantiate<ShotNumber3>(this);
	Instantiate<ShotNumber4>(this);
	Instantiate<ShotNumber5>(this);
	Instantiate<ShotNumber6>(this);
	Instantiate<ShotNumber7>(this);
	Instantiate<ShotNumber8>(this);
	Instantiate<ShotNumber9>(this);
}

void Diffusion::Update()
{
	for (auto itr = childList_.begin(); itr != childList_.end(); itr++)
	{
		Diffusion* pDiffusion = (Diffusion*)(*itr);
		pDiffusion->transform_.position_.vecZ += data_.speed_;


		if (pDiffusion->transform_.position_.vecZ > attackRangeZ_)
		{
			pDiffusion->KillMe();
		}
	}
}

void Diffusion::Draw()
{
	for (auto itr = childList_.begin(); itr != childList_.end(); itr++)
	{
		Diffusion* pDiffusion = (Diffusion*)(*itr);
		Model::SetTransform(pDiffusion->hModel_, pDiffusion->transform_);
		Model::Draw(pDiffusion->hModel_, SHADER_3D);
	}
}

void Diffusion::Release()
{
	for (auto itr = childList_.begin(); itr != childList_.end(); itr++)
	{
		Diffusion* pDiffusion = (Diffusion*)(*itr);
		pObstacle_ = nullptr;
		pDiffusion->pPointer_ = nullptr;
	}
}