#pragma once

#include "Engine/GameObject.h"
#include "Engine/Camera.h"

const float camPosZ_ = -30.0f;          //カメラのZ位置
const float camPosY_ = 2.0f;            //カメラのY位置
const float camTarZ_ = -24.0f;          //ポインターを設置する位置
const float stageSideUpper_ = 24.0f;    //ステージの上端
const float stageSideLow_ = -12.0f;     //ステージの下端
const float stageSideRight_ = 14.0f;    //ステージの右端
const float stageSideLeft_ = -14.0f;    //ステージの左端
const float mauseRate_ = 1.3f;          //マウスによる回転率変更


//プレイシーンのカメラを管理するクラス
class PlaySceneCamera : public GameObject
{

    XMVECTOR position_;     //カメラの位置
    XMVECTOR target_;       //カメラの焦点
    XMVECTOR rotate_;       //マウス操作による回転
    XMMATRIX mX_;           //X軸の回転行列
    XMMATRIX mY_;           //Y軸の回転行列

public:
    //コンストラクタ
    PlaySceneCamera(GameObject* parent);

    //デストラクタ
    ~PlaySceneCamera();

    //初期化
    void Initialize() override;

    //更新
    void Update() override;

    //描画
    void Draw() override;

    //開放
    void Release() override;

    //カメラの焦点位置を渡す
    XMVECTOR GetTarget() { return target_; };

    //カメラの位置を渡す
    XMVECTOR GetPos() { return position_; };

    //X軸の回転行列を渡す
    XMMATRIX GetRotX() { return mX_; };

    //Y軸の回転行列を渡す
    XMMATRIX GetRotY() { return mY_; };

    //回転量を渡す
    XMVECTOR GetRotate() { return rotate_; };

};