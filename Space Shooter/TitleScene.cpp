#pragma once

#include "Engine/Splash.h"
#include "Engine/Input.h"
#include "Engine/SceneManager.h"
#include "Engine/Model.h"
#include "TitleScene.h"

//コンストラクタ
TitleScene::TitleScene(GameObject * parent)
	: GameObject(parent, "TitleScene"),drawFlg_(true),countDrawFlg_(reset_)
{
	for (int i = 0; i < sceneNum_; i++)
	{
		hSplash_[i] = -1;
	}
}

//初期化
void TitleScene::Initialize()
{
	for (int i = 0; i < sceneNum_; i++)
	{
		//画像データのロード
		hSplash_[i] = Splash::Load("Assets/Title_" + std::to_string(i + 1) + ".png");
		assert(hSplash_[i] >= 0);
	}
}

//更新
void TitleScene::Update()
{
	//画像を切り替えるタイミングを更新
	countDrawFlg_++;

	//一定のタイミングになったとき
	if (countDrawFlg_ > changeTime_)
	{
		//フラグを交互に更新させる
		if (drawFlg_ == true)
		{
			//画像の描画フラグを切り替える
			drawFlg_ = false;
			//画像を切り替えるタイミングをリセット
			countDrawFlg_ = reset_;
		}
		else
		{
			//画像の描画フラグを切り替える
			drawFlg_ = true;
			//画像を切り替えるタイミングをリセット
			countDrawFlg_ = reset_;
		}
	}

	//左クリックした
	if (Input::IsMouseButtonDown(0))
	{
		//プレイ画面に遷移
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_PLAY);
	}

	//右クリックした
	if (Input::IsMouseButtonDown(1))
	{
		//プログラム終了
		PostQuitMessage(0);
	}
}

//描画
void TitleScene::Draw()
{
	//フラグによって描画する画像を切り替えて点滅しているように見せる//

	if (drawFlg_ == true)		//選択肢無し画像
	{
		Splash::SetTransform(hSplash_[0], transform_);
		Splash::Draw(hSplash_[0]);
	}
	
	if (drawFlg_ == false)		//選択肢有り画像
	{
		Splash::SetTransform(hSplash_[1], transform_);
		Splash::Draw(hSplash_[1]);
	}
}

//開放
void TitleScene::Release()
{
}