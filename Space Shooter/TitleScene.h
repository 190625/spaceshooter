#pragma once

#include "Engine/GameObject.h"

//定数
const int sceneNum_ = 2;			//シーン数
const int changeTime_ = 40;			//画像の切り替えタイミング

//タイトルシーンを管理するクラス
class TitleScene : public GameObject
{
	int hSplash_[sceneNum_];		//画像番号
	bool drawFlg_;					//画像の描画フラグ
	int countDrawFlg_;				//画像の描画フラグを更新するタイミングを管理するフラグ

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	TitleScene(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};